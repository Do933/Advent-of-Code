use std::collections::HashSet;
use std::fs;
use itertools::Itertools;

pub fn solve_day6() {
    let chars = fs::read_to_string("resources/day6.txt").unwrap().chars().collect_vec();

    let mut res = 0usize;
    for i in 14..chars.len() {
        let mut prev: HashSet<char> = HashSet::new();
        for j in i-14..i {
            prev.insert(chars[j]);
        }
        if prev.len() == 14 {
            res = i;
            break;
        }
    }

    println!("{}", res);

    // day6_oneline();
}

fn day6_oneline() {
    println!("{}", 0);
}