use std::cmp::max;
use std::collections::{HashMap, HashSet};
use std::fs;
use std::iter::Map;
use itertools::{Itertools, min};

struct Valve {
    name: String,
    rate: u32,
    neighbours: Vec<String>
}

pub fn solve_day16() {
    let re = regex::Regex::new(r" has flow rate=|; tunnels? leads? to valves? ").unwrap();
    let vs = fs::read_to_string("resources/day16.txt").unwrap()
        .lines().map(|l| re.split(l).map(String::from).collect_vec())
        .map(|ps| Valve {
            name: ps[0].chars().dropping(6).collect(),
            rate: ps[1].parse::<u32>().unwrap(),
            neighbours: ps[2].split(", ").map(String::from).collect_vec()
        })
        .collect_vec();
    let mut valves: HashMap<String, Valve> = HashMap::new();
    for v in vs {
        valves.insert(v.name.chars().collect(), v);
    }

    println!("{}", 0);
}
