use std::fs;
use itertools::Itertools;

pub fn solve_day1() {
    let elfs: Vec<String> = fs::read_to_string("resources/day1.txt").unwrap()
        .split("\n\n").map(|x| String::from(x)).collect();
    let mut amounts: Vec<i32> = elfs.iter()
        .map(|s| s.lines().map(|l| l.parse::<i32>().unwrap()).sum())
        .collect();
    amounts.sort_by(|a, b| b.cmp(a));
    println!("{}", amounts[0] + amounts[1] + amounts[2]);
    // day1_oneline();
}

fn day1_oneline() {
    println!("{}", fs::read_to_string("resources/day1.txt").unwrap()
        .split("\n\n").map(|x| String::from(x))
        .map(|x| x.lines().map(|l| l.parse::<i32>().unwrap()).sum::<i32>())
        .sorted().rev().take(3).sum::<i32>());
}