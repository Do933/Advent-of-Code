use std::cmp::max;
use std::cmp::min;
use std::collections::HashSet;
use std::fs;
use itertools::Itertools;

struct Line {
    points: HashSet<(i32, i32)>,
    max_y: i32,
}
impl Line {
    fn new(x1: i32, y1: i32, x2: i32, y2: i32) -> Line {
        let from = (min(x1, x2), min(y1, y2));
        let to = (max(x1, x2), max(y1, y2));
        let mut points: HashSet<(i32, i32)> = HashSet::new();
        for x in from.0..to.0 + 1 {
            for y in from.1..to.1 + 1 {
                points.insert((x, y));
            }
        }
        Line { points, max_y: to.1 + 1 }
    }
    fn covers(&self, p: (i32, i32)) -> bool {
        self.points.contains(&p)
    }
}

fn check_point(x: i32, y: i32, lines: &Vec<Line>, sand: &HashSet<(i32, i32)>) -> bool {
    !lines.iter().any(|l| l.covers((x, y)))
        && !sand.contains(&(x, y))
}

pub fn solve_day14() {
    let lines = fs::read_to_string("resources/day14.txt").unwrap()
        .lines().map(|l| l.split(" -> ").map(|s| String::from(s)).collect_vec())
        .map(|ps| ps.iter().map(|p| p.split(",").map(|cs| cs.parse::<i32>().unwrap()).collect_vec()).map(|ps| (ps[0], ps[1])).collect_vec())
        .flat_map(|ps| {
            let mut ls: Vec<Line> = vec![];
            for i in 0..ps.len()-1 {
                ls.push(Line::new(ps[i].0, ps[i].1, ps[i+1].0, ps[i+1].1))
            }
            ls
        })
        .collect_vec();
    let mut sand: HashSet<(i32, i32)> = HashSet::new();

    let mut x = 500;
    let mut y = 0;
    // let mut abyss = false;
    let floor = lines.iter().map(|l| l.max_y).max().unwrap() + 2;
    while !sand.contains(&(500, 0)) {
        if y + 1 == floor {
            sand.insert((x, y));
            x = 500;
            y = 0;
            continue;
        }
        if check_point(x, y+1, &lines, &sand) {
            y += 1;
            continue;
        }
        if check_point(x-1, y+1, &lines, &sand) {
            x -= 1;
            y += 1;
            continue;
        }
        if check_point(x+1, y+1, &lines, &sand) {
            x += 1;
            y += 1;
            continue;
        }
        sand.insert((x, y));
        x = 500;
        y = 0;
    }

    println!("{}", sand.len())
}

