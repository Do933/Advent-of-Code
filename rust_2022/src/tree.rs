#[derive(Clone, PartialEq, Debug)]
pub enum Tree<'a, T: 'a> {
    Node(T, Vec<Tree<'a, T>>, Option<*mut Tree<'a, T>>),
    Leaf(T),
}

impl<'a, T> Tree<'a, T> {

    pub fn is_node(self: &Self) -> bool {
        match self {
            Tree::Leaf(_) => {false}
            Tree::Node(_, _, _) => {true}
        }
    }

    pub fn is_leaf(self: &Self) -> bool {
        match self {
            Tree::Leaf(_) => {true}
            Tree::Node(_, _, _) => {false}
        }
    }

    pub fn children(self: &'a Self) -> &Vec<Tree<T>> {
        match self {
            Tree::Leaf(_) => { panic!() }
            Tree::Node(_, cs, _) => {cs}
        }
    }

    pub fn children_mut(self: &'a mut Self) -> &mut Vec<Tree<T>> {
        match self {
            Tree::Leaf(_) => { panic!() }
            Tree::Node(_, cs, _) => {cs}
        }
    }

    pub fn add_node(self: &'a mut Self, value: T) {
        let ptr = self as *mut Tree<T>;
        unsafe {
            (*ptr).children_mut().push(Tree::Node(value, vec![], Some(ptr)));
        }
    }

    pub fn add_leaf(self: &'a mut Self, value: T) {
        self.children_mut().push(Tree::Leaf(value));
    }

    pub fn value(self: &Self) -> &T {
        match self {
            Tree::Leaf(v) => {v}
            Tree::Node(v, _, _) => {v}
        }
    }

    pub fn parent(self: &'a Self) -> Option<& Tree<T>> {
        match self {
            Tree::Leaf(_) => { panic!() }
            Tree::Node(_, _, None) => {None}
            Tree::Node(_, _, Some(p)) => unsafe {p.as_ref()}
        }
    }

    pub fn parent_mut(self: &'a mut Self) -> Option<&mut Tree<T>> {
        match self {
            Tree::Leaf(_) => { panic!() }
            Tree::Node(_, _, None) => {None}
            Tree::Node(_, _, Some(p)) => unsafe {p.as_mut()}
        }
    }

}