use std::{fs, iter};
use std::borrow::{Borrow, BorrowMut};
use itertools::Itertools;
use regex;

#[derive(Clone)]
struct Tower {
    crates: Vec<char>
}
impl Tower {
    fn take(self: &mut Tower) -> char {
        self.crates.remove(0)
    }
    fn take_n(self: &mut Tower, n: usize) -> Vec<char> {
        let cs = self.crates.iter().take(n).map(|c| *c).collect_vec();
        for _ in 0..n {
            self.crates.remove(0);
        }
        cs
    }
    fn add(self: &mut Tower, c: char) {
        self.crates.insert(0, c);
    }
    fn add_all(self: &mut Tower, cs: Vec<char>) {
        for c in cs.iter().rev() {
            self.crates.insert(0, *c);
        }
    }
}

pub fn solve_day5() {
    let contents = fs::read_to_string("resources/day5.txt").unwrap()
        .split("\n\n").map(|p| String::from(p)).collect_vec();
    let n_stacks = (contents[0].lines().map(|l| l.len() + 1).max().unwrap()) / 4;
    let mut stacks = iter::repeat(Tower { crates: vec![] })
        .take(n_stacks).collect_vec();

    contents[0].lines().dropping_back(1)
        .for_each(|l| {
            for i in 0..n_stacks {
                let c = l.chars().nth(i * 4 + 1).or(Some(' ')).unwrap();
                if c.is_alphabetic() {
                    stacks[i].crates.push(c);
                }
            }
        });

    let re = regex::Regex::new(r"move | from | to ").unwrap();
    let moves = contents[1].lines()
        .map(|l| re.split(l).dropping(1).map(|s| s.parse::<usize>().unwrap()).collect_vec())
        .map(|v| (v[0], v[1], v[2]))
        .collect_vec();

    for (amt, from, to) in moves {
        // for _ in 0..amt {
        //     let c = stacks[from-1].take();
        //     stacks[to-1].add(c);
        // }
        let cs = stacks[from-1].take_n(amt);
        stacks[to-1].add_all(cs);
    }

    println!("{}", stacks.iter().map(|s| s.crates[0]).collect::<String>());

    // day5_oneline();
}

fn day5_oneline() {
    println!("{}", 0);
}