use std::borrow::Borrow;
use std::collections::HashMap;
use std::fs;
use std::num::IntErrorKind::Empty;
use std::ops::IndexMut;
use std::ptr::{null_mut, slice_from_raw_parts_mut};
use std::rc::{Rc, Weak};
use itertools::Itertools;
use crate::tree::Tree;
use crate::tree::Tree::{Leaf, Node};

#[derive(Clone, PartialEq, Debug)]
enum Command {
    Cd(String),
    Ls(Vec<String>),
}

#[derive(Clone, Debug)]
struct File {
    name: String,
    size: usize,
}

fn size(dir: &Tree<File>) -> usize {
    match dir {
        Node(v, cs, _) => {
            v.size + cs.iter().map(|t| size(t)).sum::<usize>()
        }
        Leaf(File { name: _, size: s }) => {*s}
    }
}

fn flatten<'a>(tree: &'a Tree<'a, File>) -> Vec<&'a Tree<'a, File>> {
    match tree {
        Leaf(_) => { vec![tree] }
        Node(_, cs, _) => {
            let mut res = vec![tree];
            for c in cs {
                for t in flatten(c) {
                    res.push(t);
                }
            }
            res
        }
    }
}

pub fn solve_day7() {
    let mut commands: Vec<Command> = Vec::new();
    let mut output: Vec<String> = Vec::new();
    let mut ls = false;
    for l in fs::read_to_string("resources/day7.txt").unwrap().lines() {
        if l.starts_with("$ ") && ls {
            commands.push(Command::Ls(output.clone()));
            output.clear();
        }

        if l.starts_with("$ cd") {
            commands.push(Command::Cd(l.chars().dropping(5).collect::<String>()));
            ls = false;
        } else if l == "$ ls" {
            ls = true
        } else {
            output.push(String::from(l));
        }
    }
    if ls {
        commands.push(Command::Ls(output.clone()));
        output.clear();
    }

    let mut root = Node(File { name: String::from("/"), size: 0 }, vec![], None);
    let mut current = &mut root as *mut Tree<File>;

    for cmd in commands.iter().dropping(1) {
        match cmd {
            Command::Cd(loc) if loc == ".." => unsafe {
                current = (*current).parent_mut().unwrap();
            }
            Command::Cd(loc) => unsafe {
                let mut found = None;
                for c in (*current).children_mut() {
                    if c.value().name == *loc {
                        found = Some(c);
                        break;
                    }
                }
                current = found.unwrap();
            }
            Command::Ls(output) => unsafe {
                for l in output {
                    if l.starts_with("dir ") {
                        (*current).add_node(File { name: l.chars().dropping(4).collect(), size: 0 });
                    } else {
                        let parts = l.split(" ").collect_vec();
                        (*current).add_leaf(File { name: String::from(parts[1]), size: parts[0].parse().unwrap() });
                    }
                }
            }
        }
    }

    let all_files = flatten(&root);
    let total = all_files.iter().filter(|t| t.is_node()).map(|t| {
        let s = size(t);
        if s <= 100000 {
            s
        } else {
            0
        }
    }).sum::<usize>();

    println!("{}", total);

    let total = 70000000usize;
    let needed = 30000000usize;
    let used = size(&root);
    let available = total - used;
    let to_free = needed - available;

    let deletable = all_files.iter().filter(|t| size(t) >= to_free)
        .min_by(|a, b| size(a).cmp(&size(b)));

    println!("{}", size(deletable.unwrap()));

    // day7_oneline();
}

fn day7_oneline() {
    println!("{}", 0);
}