use std::fs;
use std::str::FromStr;
use itertools::Itertools;
use crate::day11::Operation::{Add, AddSelf, Mul, MulSelf};

struct Monkey {
    items: Vec<usize>,
    operation: Operation,
    test: Test,
    total_inspections: usize,
}
impl Monkey {
    fn inspect_item(&mut self, m: usize) -> Option<(usize, usize)> {
        if self.items.is_empty() {
            return None;
        }
        let mut item = self.items.remove(0);
        item = self.operation.apply(item);
        // item /= 3;
        item %= m;
        self.total_inspections += 1;
        Some((item, self.test.apply(item)))
    }
}

enum Operation {
    AddSelf, MulSelf, Add(usize), Mul(usize),
}
impl Operation {
    fn apply(&self, x: usize) -> usize {
        match self {
            AddSelf => {x + x}
            MulSelf => {x * x}
            Add(y) => {x + y}
            Mul(y) => {x * y}
        }
    }
}
impl FromStr for Operation {
    type Err = ();
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let split = if s.contains(" + ") {" + "} else {" * "};
        let ps = s.split(split).collect_vec();
        let l = ps[0];
        let r = ps[1];
        if split == " + " { match (l, r) {
            ("old", "old") => { Ok(AddSelf) }
            ("old", _) => { Ok(Add( r.parse::<usize>().unwrap())) }
            (_, "old") => { Ok(Add(l.parse::<usize>().unwrap())) }
            _ => Err(())
        } } else { match (l, r) {
            ("old", "old") => { Ok(MulSelf) }
            ("old", _) => { Ok(Mul( r.parse::<usize>().unwrap())) }
            (_, "old") => { Ok(Mul(l.parse::<usize>().unwrap())) }
            _ => Err(())
        } }
    }
}

struct Test {
    divisible_by: usize,
    if_true: usize,
    if_false: usize,
}
impl Test {
    fn apply(&self, x: usize) -> usize {
        if x % self.divisible_by == 0 {
            self.if_true
        } else {
            self.if_false
        }
    }
}

pub fn solve_day11() {
    let mut monkeys = fs::read_to_string("resources/day11.txt").unwrap()
        .split("\n\n").map(|m| String::from(m))
        .map(|m| m.lines().map(|l| String::from(l)).collect_vec())
        .map(|m| {
            let items = m[1].split(": ").collect_vec()[1].split(", ").map(|i| i.parse::<usize>().unwrap()).collect_vec();
            let operation = m[2].split(" = ").collect_vec()[1].parse::<Operation>().unwrap();
            let divisible_by = m[3].split("divisible by ").collect_vec()[1].parse::<usize>().unwrap();
            let if_true = m[4].split("to monkey ").collect_vec()[1].parse::<usize>().unwrap();
            let if_false = m[5].split("to monkey ").collect_vec()[1].parse::<usize>().unwrap();
            Monkey {
                items, operation, test: Test {
                    divisible_by, if_true, if_false
                }, total_inspections: 0
            }
        })
        .collect_vec();

    let m = monkeys.iter().map(|m| m.test.divisible_by).product();
    for _ in 0..10000 {
        for i in 0..monkeys.len() {
            let mut res = monkeys[i].inspect_item(m);
            let mut results: Vec<(usize, usize)> = vec![];
            while res.is_some() {
                results.push(res.unwrap());
                res = monkeys[i].inspect_item(m);
            }
            for (worry, rec) in results {
                monkeys[rec].items.push(worry);
            }
        }
    }

    let activity = monkeys.iter().map(|m| m.total_inspections as u64)
        .sorted().collect_vec();

    println!("{}", activity[monkeys.len()-1] * activity[monkeys.len()-2]);
}