use std::cmp::{max, min};
use std::collections::{HashMap, HashSet};
use std::fs;
use itertools::Itertools;

fn to_height(c: char) -> i32 {
    match c {
        'S' => {0}
        'E' => {27}
        _ => {c as i32 - 96}
    }
}

fn bfs(heights: &HashMap<(i32, i32), i32>, start: (i32, i32), end: (i32, i32), size: (i32, i32)) -> Vec<(i32, i32)> {
    let mut queue: Vec<(i32, i32)> = vec![start];
    let mut visited: HashSet<(i32, i32)> = HashSet::new();
    let mut prev: HashMap<(i32, i32), (i32, i32)> = HashMap::new();

    while !queue.is_empty() {
        let current = queue.remove(0);

        if visited.contains(&current) {
            continue;
        }
        if current == end {
            break;
        }

        visited.insert(current);

        let ns = vec![(current.0 + 1, current.1), (current.0 - 1, current.1),
                      (current.0, current.1 + 1), (current.0, current.1 - 1)];
        for n in &ns {
            if visited.contains(n) {
                continue;
            }
            if n.0 >= size.0 || n.1 >= size.1 || n.0 < 0 || n.1 < 0 {
                continue;
            }
            let diff = heights.get(&current).unwrap() - heights.get(n).unwrap();
            if diff < -1 {
                continue;
            }
            prev.insert(*n, current);
            queue.push(*n);
        }
    }

    let mut path = vec![end];
    while prev.contains_key(path.last().unwrap()) {
        path.push(*prev.get(path.last().unwrap()).unwrap())
    }
    path
}

pub fn solve_day12() {
    let mut heights: HashMap<(i32, i32), i32> = HashMap::new();
    let contents = fs::read_to_string("resources/day12.txt").unwrap();
    let lines = contents.lines().map(|s| String::from(s)).collect_vec();
    let mut start = (0, 0);
    let mut end = (0, 0);
    let mut size = (0, 0);
    for y in 0..lines.len() {
        let chars = lines[y].chars().collect_vec();
        for x in 0..chars.len() {
            heights.insert((x as i32, y as i32), to_height(chars[x]));
            if chars[x] == 'S' { start = (x as i32, y as i32); }
            if chars[x] == 'E' { end = (x as i32, y as i32); }
            size = (max(size.0, x as i32 + 1), size.1);
        }
        size = (size.0, max(size.1, y as i32 + 1));
    }

    let from_start = bfs(&heights, start, end, size).len() - 1;
    println!("{}", from_start);

    let mut shortest = from_start;
    for s in heights.keys().filter(|k| *heights.get(k).unwrap() == 1) {
        let length = bfs(&heights, *s, end, size).len() - 1;
        if length != 0 {
            shortest = min(shortest, length);
        }
    }
    println!("{}", shortest);
}

