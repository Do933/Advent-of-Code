use std::cmp::Ordering;
use std::fs;
use std::str::FromStr;
use itertools::Itertools;
use crate::day13::Data::{List, Number};

#[derive(Clone, Debug, PartialEq, Eq)]
enum Data {
    Number(i32), List(Vec<Data>)
}
impl Data {
    fn copy(&self) -> Data {
        match self {
            Number(x) => {Number(*x)}
            List(xs) => {List(xs.iter().map(|x| x.copy()).collect_vec())}
        }
    }
}

impl FromStr for Data {
    type Err = ();
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s.starts_with("[") {
            let mut buffer: Vec<char> = vec![];
            let mut items: Vec<Data> = vec![];
            let chars = s.chars().collect_vec();
            let mut depth = 0;
            for i in 1..s.len()-1 {
                let c = chars[i];
                if c == '[' {
                    depth += 1;
                }
                if c == ']' {
                    depth -= 1;
                }
                if c == ',' && depth == 0 {
                    items.push(buffer.iter().collect::<String>().parse().unwrap());
                    buffer.clear();
                } else {
                    buffer.push(c);
                }
            }
            if !buffer.is_empty() {
                items.push(buffer.iter().collect::<String>().parse().unwrap());
            }
            Ok(List(items))
        } else {
            Ok(Number(s.parse::<i32>().unwrap()))
        }
    }
}

fn compare(left: &Data, right: &Data) -> Ordering {
    match (left, right) {
        (Number(l), Number(r)) => { l.cmp(r) }
        (List(l), List(r)) => { compare_lists(l, r) }
        (Number(l), List(r)) => { compare_lists(&vec![Number(*l)], r) }
        (List(l), Number(r)) => { compare_lists(l, &vec![Number(*r)]) }
    }
}

fn compare_lists(left: &Vec<Data>, right: &Vec<Data>) -> Ordering {
    let mut i = 0;
    while i < left.len() && i < right.len() {
        let res = compare(&left[i], &right[i]);
        if res != Ordering::Equal {
            return res;
        }
        i += 1;
    }
    return left.len().cmp(&right.len());
}

pub fn solve_day13() {
    let pairs = fs::read_to_string("resources/day13.txt").unwrap()
        .split("\n\n").map(|s| String::from(s))
        .map(|ps| ps.lines().map(|l| l.parse::<Data>().unwrap()).collect_vec())
        .map(|ps| (ps[0].copy(), ps[1].copy()))
        .collect_vec();

    let mut indices: Vec<usize> = vec![];
    for i in 0..pairs.len() {
        let (l, r) = &pairs[i];
        if compare(l, r) == Ordering::Less {
            indices.push(i + 1);
        }
    }

    println!("{}", indices.iter().sum::<usize>());

    let mut packets = pairs.iter().flat_map(|(l, r)| vec![l.copy(), r.copy()]).collect_vec();
    let div1 = List(vec![List(vec![Number(2)])]);
    let div2 = List(vec![List(vec![Number(6)])]);
    packets.push(div1.copy());
    packets.push(div2.copy());
    packets.sort_by(|l, r| compare(l, r));

    let mut i1 = 0;
    let mut i2 = 0;
    for i in 0..packets.len() {
        if packets[i] == div1 {
            i1 = i + 1;
        }
        if packets[i] == div2 {
            i2 = i + 1;
        }
    }

    println!("{}", i1 * i2)
}

