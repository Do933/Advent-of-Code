use std::borrow::Borrow;
use std::cmp::max;
use std::collections::{HashMap, HashSet};
use std::fs;
use std::num::IntErrorKind::Empty;
use std::ops::IndexMut;
use std::ptr::{null_mut, slice_from_raw_parts_mut};
use std::rc::{Rc, Weak};
use std::str::FromStr;
use itertools::Itertools;
use crate::day10::Instruction::{AddX, Noop};
use crate::tree::Tree;
use crate::tree::Tree::{Leaf, Node};

enum Instruction {
    AddX(i32), Noop
}
impl Instruction {
    fn amt_cycles(&self) -> i32 {
        match self {
            Noop => {1}
            AddX(_) => {2}
        }
    }
}

fn abs(x: i32) -> i32 {
    if x < 0 {-x} else {x}
}

pub fn solve_day10() {
    let instructions = fs::read_to_string("resources/day10.txt").unwrap()
        .lines().map(|l| String::from(l))
        .map(|i| if i == "noop" { Noop } else {
            let ps = i.split(" ").collect_vec();
            AddX(ps[1].parse().unwrap())
        }).collect_vec();

    let mut cycle = 1;
    let mut x = 1;
    let mut total = 0;
    let mut screen = [[false; 40]; 6];
    for i in &instructions {
        for j in 1..=i.amt_cycles() {
            let x_pos = (cycle - 1) % 40;
            let y_pos = (cycle - 1) / 40;
            screen[y_pos as usize][x_pos as usize] = abs(x_pos - x) <= 1;
            cycle += 1;
            if j == i.amt_cycles() {
                match i {
                    AddX(amt) => {x += amt;}
                    _ => {}
                }
            }
            if (cycle - 20) % 40 == 0 {
                total += x * cycle;
            }
        }
    }

    println!("{}", total);
    for l in &screen {
        for b in l {
            print!("{}", if *b {'█'} else {' '});
        }
        println!();
    }

    // day10_oneline();
}

fn day10_oneline() {
    println!("{}", 0);
}