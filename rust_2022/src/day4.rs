use std::fs;
use itertools::Itertools;

pub fn solve_day4() {
    let pairs = fs::read_to_string("resources/day4.txt")
        .unwrap().lines()
        .map(|l| String::from(l))
        .map(|l| {
            let ps = l.split(",")
                .map(|p| String::from(p))
                .map(|r| {
                    let is = r.split("-")
                        .map(|i| i.parse::<i32>().unwrap())
                        .collect_vec();
                    (is[0], is[1])
                })
                .collect_vec();
            (ps[0], ps[1])
        })
        .collect_vec();

    println!("{}", pairs.iter()
        .filter(|((l1, l2), (r1, r2))| l1 >= r1 && l1 <= r2 || l2 >= r1 && l2 <= r2 || r1 >= l1 && r1 <= l2 || r2 >= l1 && r2 <= l2)
        .count());

    // day4_oneline();
}

fn day4_oneline() {
    println!("{}", 0);
}