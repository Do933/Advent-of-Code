use std::fs;
use itertools::Itertools;

#[derive(PartialEq, Copy, Clone)]
enum RPS {
    Rock, Paper, Scissors
}

#[derive(PartialEq)]
enum Outcome {
    Win, Draw, Loss
}

fn score(yours: &RPS, theirs: &RPS) -> i32 {
    move_score(yours) + match yours {
        RPS::Rock => if *theirs == RPS::Scissors {6}
            else if *theirs == RPS::Paper {0}
            else {3}
        RPS::Scissors => if *theirs == RPS::Paper {6}
            else if *theirs == RPS::Rock {0}
            else {3}
        RPS::Paper => if *theirs == RPS::Rock {6}
            else if *theirs == RPS::Scissors {0}
            else {3}
    }
}

fn what_to_choose(theirs: &RPS, outcome: &Outcome) -> RPS {
    match outcome {
        Outcome::Win => match theirs {
            RPS::Rock => { RPS::Paper }
            RPS::Paper => { RPS::Scissors }
            RPS::Scissors => { RPS::Rock }
        }
        Outcome::Loss => match theirs {
            RPS::Rock => { RPS::Scissors }
            RPS::Paper => { RPS::Rock }
            RPS::Scissors => { RPS::Paper }
        }
        Outcome::Draw => {*theirs}
    }
}

fn move_score(mov: &RPS) -> i32 {
    match mov {
        RPS::Rock => {1}
        RPS::Paper => {2}
        RPS::Scissors => {3}
    }
}

fn to_rps(c: char) -> RPS {
    match c {
        'A'|'X' => {RPS::Rock}
        'B'|'Y' => {RPS::Paper}
        'C'|'Z' => {RPS::Scissors}
        _ => panic!("No")
    }
}

fn to_outcome(c: char) -> Outcome {
    match c {
        'X' => {Outcome::Loss }
        'Y' => {Outcome::Draw}
        'Z' => {Outcome::Win }
        _ => panic!("No")
    }
}

pub fn solve_day2() {
    let games = fs::read_to_string("resources/day2.txt").unwrap().lines().map(|l| String::from(l)).collect_vec();
    let score: i32 = games.iter()
        .map(|g| {
            let theirs = to_rps(g.chars().nth(0).unwrap());
            let outcome = to_outcome(g.chars().nth(2).unwrap());
            score(&what_to_choose(&theirs, &outcome), &theirs)
        })
        .sum();

    println!("{}", score);

    // day2_oneline();
}

fn day2_oneline() {
    println!("{}", fs::read_to_string("resources/day2.txt").unwrap().lines()
        .map(|l| String::from(l))
        .map(|g| (g.chars().nth(0).unwrap(), g.chars().nth(2).unwrap()))
        .map(|(t, o)| if o == 'X' { if t == 'A' {3} else if t == 'B' {1} else {2} }
            else if o == 'Y' { if t == 'A' {4} else if t == 'B' {5} else {6} }
            else { if t == 'A' {8} else if t == 'B' {9} else {7} })
        .sum::<i32>())
}