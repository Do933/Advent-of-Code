use std::borrow::Borrow;
use std::cmp::max;
use std::collections::{HashMap, HashSet};
use std::fs;
use std::num::IntErrorKind::Empty;
use std::ops::IndexMut;
use std::ptr::{null_mut, slice_from_raw_parts_mut};
use std::rc::{Rc, Weak};
use std::str::FromStr;
use itertools::Itertools;
use crate::tree::Tree;
use crate::tree::Tree::{Leaf, Node};

enum Dir {
    Right, Left, Up, Down
}
impl FromStr for Dir {
    type Err = ();
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "R" => {Ok(Dir::Right)}
            "L" => {Ok(Dir::Left)}
            "U" => {Ok(Dir::Up)}
            "D" => {Ok(Dir::Down)}
            _ => {Err(())}
        }
    }
}
impl Dir {
    fn diff(self: &Self) -> (i32, i32) {
        match self {
            Dir::Right => {(1, 0)}
            Dir::Left => {(-1, 0)}
            Dir::Up => {(0, -1)}
            Dir::Down => {(0, 1)}
        }
    }
}

fn add(left: (i32, i32), right: (i32, i32)) -> (i32, i32) {
    (left.0 + right.0, left.1 + right.1)
}

fn abs(n: i32) -> i32 {
    if n == 0 {0} else if n > 0 {n} else {-n}
}

fn sgn(n: i32) -> i32 {
    if n == 0 {0} else if n > 0 {1} else {-1}
}

fn follow(head: (i32, i32), tail: (i32, i32)) -> (i32, i32) {
    let dx = head.0 - tail.0;
    let dy = head.1 - tail.1;
    if abs(dx) <= 1 && abs(dy) <= 1 {tail}
    else {add(tail, (sgn(dx), sgn(dy)))}
}

pub fn solve_day9() {
    let mut head = (0, 0);
    let mut tail = (0, 0);
    let mut pos: HashSet<(i32, i32)> = HashSet::new();

    let moves = fs::read_to_string("resources/day9.txt").unwrap()
        .lines().map(|l| l.split(" ").map(|s| String::from(s)).collect_vec())
        .map(|ps| (ps[0].parse::<Dir>().unwrap(), ps[1].parse::<i32>().unwrap()))
        .collect_vec();

    for (d, amt) in &moves {
        for _ in 0..*amt {
            pos.insert(tail);
            head = add(head, d.diff());
            tail = follow(head, tail);
        }
    }
    pos.insert(tail);

    println!("{}", pos.len());

    pos.clear();

    let mut knots = [(0, 0); 10];

    for (d, amt) in &moves {
        for _ in 0..*amt {
            pos.insert(knots[9]);
            knots[0] = add(knots[0], d.diff());
            for i in 0..9 {
                knots[i+1] = follow(knots[i], knots[i+1])
            }
        }
    }

    println!("{}", pos.len());
    // day9_oneline();
}

fn day9_oneline() {
    println!("{}", 0);
}