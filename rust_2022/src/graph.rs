use std::collections::HashMap;
use itertools::Itertools;

#[derive(Copy, Clone, PartialEq, Eq, Hash)]
pub struct GraphNode {
    index: usize,
}

#[derive(Copy, Clone, PartialEq, Eq, Hash)]
pub struct GraphEdge {
    from: GraphNode,
    to: GraphNode,
}

pub struct Graph<N, E> {
    nodes: Vec<N>,
    edges: HashMap<(usize, usize), E>
}

impl<N, E> Graph<N, E> {

    pub fn new() -> Graph<N, E> {
        Graph {
            nodes: vec![],
            edges: HashMap::new(),
        }
    }

    pub fn nodes(&self) -> Vec<GraphNode> {
        (0..self.nodes.len()).map(|i| GraphNode { index: i }).collect_vec()
    }

    pub fn add_node(&mut self, value: N) -> GraphNode {
        self.nodes.push(value);
        GraphNode { index: self.nodes.len() - 1 }
    }

    pub fn node_value(&self, node: &GraphNode) -> &N {
        &self.nodes[node.index]
    }

    pub fn edges(&self) -> Vec<GraphEdge> {
        self.edges.keys().map(|(f, t)| GraphEdge { from: GraphNode { index: *f }, to: GraphNode { index: *t } }).collect_vec()
    }

    pub fn add_edge(&mut self, from: &GraphNode, to: &GraphNode, value: E) {
        self.edges.insert((from.index, to.index), value);
    }

    pub fn edge(&self, from: &GraphNode, to:& GraphNode) -> Option<&E> {
        self.edges.get(&(from.index, to.index))
    }

    pub fn edge_value(&self, node: &GraphEdge) -> &E {
        self.edges.get(&(node.from.index, node.to.index)).unwrap()
    }

    pub fn neighbours(&self, node: &GraphNode) -> Vec<GraphNode> {
        self.edges.keys().filter(|e| e.0 == node.index)
            .map(|e| e.1)
            .map(|i| GraphNode { index: i })
            .collect_vec()
    }

    pub fn outgoing(&self, node: &GraphNode) -> Vec<GraphEdge> {
        self.neighbours(node).iter().map(|n| GraphEdge { from: node.clone(), to: *n }).collect_vec()
    }

}
