use std::collections::HashSet;
use std::fs;
use std::io::BufRead;
use std::thread::scope;
use itertools::Itertools;

fn priority(c: char) -> i32 {
    if c.is_ascii_lowercase() {(c as i32) - 96}
    else {(c as i32) - 64 + 26}
}

pub fn solve_day3() {
    // let prio = fs::read_to_string("resources/day3.txt").unwrap()
    //     .lines().map(|l| String::from(l))
    //     .map(|l| {
    //         let (l, r) = l.split_at(l.len() / 2);
    //         r.chars().find(|c| l.contains(*c)).unwrap()
    //     })
    //     .map(|c| priority(c)))
    //     .sum::<i32>();

    let backpacks = fs::read_to_string("resources/day3.txt").unwrap()
        .lines().map(|s| String::from(s)).collect_vec();
    let mut total = 0;
    for i in 0..backpacks.len()/3 {
        let c1: HashSet<char> = backpacks[i*3+1].chars().collect();
        let c2: HashSet<char> = backpacks[i*3+2].chars().collect();
        let c = backpacks[i*3].chars().find(|c| c1.contains(c) && c2.contains(c)).unwrap();
        total += priority(c);
    }

    println!("{}", total);

    // day3_oneline();
}

fn day3_oneline() {
    println!("{}", fs::read_to_string("resources/day3.txt").unwrap()
        .lines()
        .map(|l| String::from(l))
        .zip(0i32..10000i32)
        .group_by(|(s, i)| i / 3)
        .into_iter()
        .map(|(_, g)| g.map(|(bs, _)| bs).collect_vec())
        .map(|bs| bs[0].chars().find(|c| bs[1].chars().any(|c2| *c == c2) && bs[2].chars().any(|c2| *c == c2)).unwrap())
        .map(|c| priority(c))
        .sum::<i32>())
}