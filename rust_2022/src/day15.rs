use std::collections::HashSet;
use std::fs;
use itertools::Itertools;

struct Sensor {
    at: (i32, i32),
    closest: (i32, i32),
}
impl Sensor {
    fn radius(&self) -> i32 {
        (self.at.0.abs_diff(self.closest.0) + self.at.1.abs_diff(self.closest.1)) as i32
    }
}

fn parse_coord(s: &str) -> (i32, i32) {
    let ps = s.split(", ").collect_vec();
    (ps[0].chars().dropping(2).collect::<String>().parse::<i32>().unwrap(),
        ps[1].chars().dropping(2).collect::<String>().parse::<i32>().unwrap())
}

fn radius(c: (i32, i32), r: i32) -> Vec<(i32, i32)> {
    let mut points: Vec<(i32, i32)> = vec![];
    let mut curr = (c.0, c.1 - r);
    while curr != (c.0 + r, c.1) {
        points.push(curr);
        curr = (curr.0 + 1, curr.1 + 1);
    }
    while curr != (c.0, c.1 + r) {
        points.push(curr);
        curr = (curr.0 - 1, curr.1 + 1);
    }
    while curr != (c.0 - r, c.1) {
        points.push(curr);
        curr = (curr.0 - 1, curr.1 - 1);
    }
    while curr != (c.0, c.1 - r) {
        points.push(curr);
        curr = (curr.0 + 1, curr.1 - 1);
    }
    points
}

pub fn solve_day15() {
    let sensors = fs::read_to_string("resources/day15.txt").unwrap()
        .lines().map(|l| l.split(": closest beacon is at ").map(|s| String::from(s)).collect_vec())
        .map(|l| (l[0].chars().dropping(10).collect::<String>(), l[1].chars().collect::<String>()))
        .map(|(s, b)| Sensor { at: parse_coord(&s), closest: parse_coord(&b) })
        .collect_vec();

    let mut no_beacon: HashSet<(i32, i32)> = HashSet::new();

    for s in &sensors {
        let dist = (s.at.0.abs_diff(s.closest.0) + s.at.1.abs_diff(s.closest.1)) as i32;
        for x in s.at.0-dist..s.at.0+dist+1 {
            if !(s.at.1-dist..s.at.1+dist+1).contains(&2000000) {
                continue;
            }
            let y = 2000000i32;
            // for y in s.at.1-dist..s.at.1+dist+1 {
                let d = (x.abs_diff(s.at.0) + y.abs_diff(s.at.1)) as i32;
                if d <= dist && (x, y) != s.closest {
                    no_beacon.insert((x, y));
                }
            // }
        }
    }

    println!("{}", no_beacon.iter().filter(|(x, y)| *y == 2000000).collect_vec().len());

    'outer: for s in &sensors {
        for (x, y) in radius(s.at, s.radius() + 1) {
            let mut found = true;
            for t in &sensors {
                let d = (x.abs_diff(t.at.0) + y.abs_diff(t.at.1)) as i32;
                if d <= t.radius() {
                    found = false;
                    break;
                }
            }
            if found && x <= 4000000 && y <= 4000000 && x >= 0 && y >= 0 {
                println!("{}", (x as u64) * 4000000u64 + (y as u64));
                break 'outer;
            }
        }
    }
}

