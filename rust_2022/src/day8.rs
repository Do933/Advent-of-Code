use std::borrow::Borrow;
use std::cmp::max;
use std::collections::HashMap;
use std::fs;
use std::num::IntErrorKind::Empty;
use std::ops::IndexMut;
use std::ptr::{null_mut, slice_from_raw_parts_mut};
use std::rc::{Rc, Weak};
use itertools::Itertools;
use crate::tree::Tree;
use crate::tree::Tree::{Leaf, Node};

fn can_see(grid: &Vec<Vec<i32>>, pos: (usize, usize)) -> bool {
    let height = grid[pos.1][pos.0];
    let mut res = true;
    for x in 0..pos.0 {
        if grid[pos.1][x] >= height {
            res = false;
            break;
        }
    }
    if res { return true; }
    res = true;
    for x in pos.0+1..grid[pos.1].len() {
        if grid[pos.1][x] >= height {
            res = false;
            break;
        }
    }
    if res { return true; }
    res = true;
    for y in 0..pos.1 {
        if grid[y][pos.0] >= height {
            res = false;
            break;
        }
    }
    if res { return true; }
    res = true;
    for y in pos.1+1..grid.len() {
        if grid[y][pos.0] >= height {
            res = false;
            break;
        }
    }
    return res;
}

fn scenic_score(grid: &Vec<Vec<i32>>, pos: (usize, usize)) -> i32 {
    let height = grid[pos.1][pos.0];
    let mut res = 1;
    let mut c = 0;
    for x in (0..pos.0).rev() {
        c += 1;
        if grid[pos.1][x] >= height {
            break;
        }
    }
    res *= c;
    c = 0;
    for x in pos.0+1..grid[pos.1].len() {
        c += 1;
        if grid[pos.1][x] >= height {
            break;
        }
    }
    res *= c;
    c = 0;
    for y in (0..pos.1).rev() {
        c += 1;
        if grid[y][pos.0] >= height {
            break;
        }
    }
    res *= c;
    c = 0;
    for y in pos.1+1..grid.len() {
        c += 1;
        if grid[y][pos.0] >= height {
            break;
        }
    }
    res *= c;
    return res;
}

pub fn solve_day8() {
    let lines = fs::read_to_string("resources/day8.txt").unwrap().lines().map(|l| String::from(l)).collect_vec();
    let grid = lines.iter().map(|l| l.chars().map(|c| c as i32 - 48).collect_vec()).collect_vec();

    let mut count = 0;
    for y in 0..lines.len() {
        for x in 0..lines[y].len() {
            if can_see(&grid, (x, y)) {
                count += 1;
            }
        }
    }

    println!("{}", count);

    let mut max_score = 0;
    for y in 0..lines.len() {
        for x in 0..lines[y].len() {
            max_score = max(max_score, scenic_score(&grid, (x, y)));
        }
    }

    println!("{}", max_score);

    // day8_oneline();
}

fn day8_oneline() {
    println!("{}", 0);
}