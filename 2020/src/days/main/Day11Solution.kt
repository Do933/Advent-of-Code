import data.Matrix
import data.matrixRows
import data.tupleOf
import java.io.File
import java.io.InputStream

class Day11Solution : Solution<Matrix<Char>, Int>() {

    override fun read(input: InputStream): Matrix<Char> = input.lines.map { it.toCharArray().toList() }.toList().matrixRows()

    fun simulate(matrix: Matrix<Char>): Matrix<Char> {
        fun adjacent(i: Int, j: Int): Int {
            var amount = 0
            if (i > 0 && matrix[i-1, j] == '#') amount++
            if (i > 0 && j > 0 && matrix[i-1, j-1] == '#') amount++
            if (j > 0 && matrix[i, j-1] == '#') amount++
            if (i < matrix.width-1 && j > 0 && matrix[i+1, j-1] == '#') amount++
            if (i < matrix.width-1 && matrix[i+1, j] == '#') amount++
            if (i < matrix.width-1 && j < matrix.height-1 && matrix[i+1, j+1] == '#') amount++
            if (j < matrix.height-1 && matrix[i, j+1] == '#') amount++
            if (i > 0 && j < matrix.height-1 && matrix[i-1, j+1] == '#') amount++
            return amount
        }
        return Matrix(matrix.width, matrix.height) { i, j ->
            when (matrix[i, j]) {
            'L' -> if (adjacent(i, j) == 0) '#' else 'L'
            '#' -> if (adjacent(i, j) >= 4) 'L' else '#'
            else -> matrix[i, j]
        } }
    }

    override fun part1(input: Matrix<Char>): Int {
        var m1 = input
        var m2 = simulate(input)
        while (m2 != m1) {
            val sim = simulate(m2)
            m1 = m2
            m2 = sim
        }
        return m2.data.count { it == '#' }
    }

    fun simulate2(matrix: Matrix<Char>): Matrix<Char> {
        fun adjacent(i: Int, j: Int): Int {
            fun scan(dx: Int, dy: Int): Boolean {
                var x = i + dx
                var y = j + dy
                while (x >= 0 && x < matrix.width && y >= 0 && y < matrix.height) {
                    if (matrix[x, y] != '.') return matrix[x, y] == '#'
                    x += dx
                    y += dy
                }
                return false
            }
            var amount = 0
            if (scan(-1, 0)) amount++
            if (scan(-1, 1)) amount++
            if (scan(0, 1)) amount++
            if (scan(1, 1)) amount++
            if (scan(1, 0)) amount++
            if (scan(1, -1)) amount++
            if (scan(0, -1)) amount++
            if (scan(-1, -1)) amount++
            return amount
        }
        return Matrix(matrix.width, matrix.height) { i, j -> when (matrix[i, j]) {
            'L' -> if (adjacent(i, j) == 0) '#' else 'L'
            '#' -> if (adjacent(i, j) >= 5) 'L' else '#'
            else -> matrix[i, j]
        } }
    }

    override fun part2(input: Matrix<Char>): Int {
        var m1 = input
        var m2 = simulate2(input)
        while (m2 != m1) {
            val sim = simulate2(m2)
            m1 = m2
            m2 = sim
        }
        return m2.data.count { it == '#' }
    }

}