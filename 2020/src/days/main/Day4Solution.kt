import data.Matrix
import data.matrixRows
import java.io.InputStream

class Day4Solution : Solution<List<Map<String, String>>, Int>() {

    override fun read(input: InputStream): List<Map<String, String>> = input.string.split("\n\n").map { data ->
        data.split(Regex("\\s+")).associate { it.split(":")[0] to it.split(":")[1] }
    }

    override fun part1(input: List<Map<String, String>>): Int = input.count {
        it.keys.containsAll(listOf("byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"))
    }

    override fun part2(input: List<Map<String, String>>): Int = input.filter {
        it.keys.containsAll(listOf("byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"))
    }.count {
        it["byr"]!!.toInt() in 1920..2002 &&
                it["iyr"]!!.toInt() in 2010..2020 &&
                it["eyr"]!!.toInt() in 2020..2030 &&
                ((it["hgt"]!!.endsWith("cm") && it["hgt"]!!.dropLast(2).toInt() in 150..193) ||
                        (it["hgt"]!!.endsWith("in") && it["hgt"]!!.dropLast(2).toInt() in 59..76)) &&
                it["hcl"]!!.startsWith("#") && it["hcl"]!!.drop(1).matches(Regex("[0-9a-f]{6}")) &&
                it["ecl"]!! in setOf("amb", "blu", "brn", "grn", "gry", "hzl", "oth") &&
                it["pid"]!!.length == 9
    }

}