import java.io.InputStream

data class Field(val name: String, val range1: IntRange, val range2: IntRange) {
    fun valid(value: Int) = value in range1 || value in range2
}
data class Ticket(val values: List<Int>)
data class Tickets(val fields: List<Field>, val ticket: Ticket, val nearby: List<Ticket>)

class Day16Solution : Solution<Tickets, Long>() {

    override fun read(input: InputStream): Tickets = input.string.split("\n\n").let { (fields, ticket, nearby) ->
        Tickets(
                fields.lines.map { field ->
                    field.split(":").let { (name, ranges) ->
                        ranges.drop(1).split(" or ").map { it.split("-")[0].toInt()..it.split("-")[1].toInt() }.let { (r1, r2) ->
                            Field(name, r1, r2)
                        }
                    }
                }.toList(),
                Ticket(ticket.lines.toList().last().csv().map { it.toInt() }.toList()),
                nearby.lines.skip(1).map { Ticket(it.csv().map { it.toInt() }.toList()) }.toList()
        )
    }

    override fun part1(input: Tickets): Long {
        return input.nearby.flatMap { t ->
            t.values.filter { v -> input.fields.all { !it.valid(v) } }
        }.sum().toLong()
    }

    override fun part2(input: Tickets): Long {
        val validNear = input.nearby.filter { t -> t.values.all { v -> input.fields.any { it.valid(v) } } }
        val valid = input.fields.map { field ->
            field to input.ticket.values.indices.filter { i ->
                validNear.all { field.valid(it.values[i]) }
            }
        }.sortedBy { it.second.size }
        val assigned = HashSet<Int>()
        val assignment = HashMap<Field, Int>()
        valid.forEach { (f, indices) ->
            assignment[f] = indices.find { it !in assigned }!!
            assigned.add(assignment[f]!!)
        }
        return assignment.filter { it.key.name.startsWith("departure") }.map { input.ticket.values[it.value].toLong() }
                .reduce { a, b -> a * b }
    }

}