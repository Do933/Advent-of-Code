import data.modInvP
import data.tupleOf
import java.io.InputStream
import kotlin.math.abs

data class BusSchedule(val depart: Long, val busses: List<Int?>)

class Day13Solution : Solution<BusSchedule, Long>() {

    override fun read(input: InputStream): BusSchedule = input.lines.toList().let { L ->
        BusSchedule(L[0].toLong(), L[1].csv().map { if (it == "x") null else it.toInt() }.toList() )
    }

    fun closestTime(depart: Long, interval: Int): Long =
            if (depart / interval * interval == depart) depart
            else depart / interval * interval + interval

    override fun part1(input: BusSchedule): Long = input.busses.filterNotNull()
            .map { it to closestTime(input.depart, it) }.minBy { it.second }!!
            .let { (it.second - input.depart) * it.first }

    /**
     * eqs = [(a1, m1), (a2, m2) ...] s.t.
     * x = a1 mod m1
     * x = a2 mod m2
     * ...
     */
    fun solveModSystem(eqs: List<Pair<Long, Long>>): Long {
        val M = eqs.map { it.second }.reduce { a, b -> a * b }
        return eqs.map { (a, m) -> (M / m).let { Xk -> tupleOf(a, Xk, modInvP(Xk, m)) } }
                .map { (a, Xk, XkInv) -> a * Xk * XkInv }.sum() % M
    }

    override fun part2(input: BusSchedule): Long {
        val times = input.busses
        val eqs = times.mapIndexed { i, v -> if (v == null) null else (v - i).toLong() to v.toLong() }.filterNotNull()
        return solveModSystem(eqs)
    }

}