import data.head
import data.tail
import java.io.InputStream

class Day10Solution : Solution<List<Long>, Long>() {

    override fun read(input: InputStream): List<Long> = input.lines.map { it.toLong() }.toList()

    override fun part1(input: List<Long>): Long = input.sorted().let { 0L head it tail it.last() + 3L }
            .zipWithNext { a, b -> b - a }.let { l -> l.count { it == 3L }.toLong() * l.count { it == 1L }.toLong() }

    override fun part2(input: List<Long>): Long {
        val list = input.sorted().let { 0L head it tail it.last()+3 }
        val M = LongArray(list.size)
        M[0] = 1
        for (i in list.indices.drop(1)) {
            if (i >= 4 && list[i] - list[i-4] == 4L) M[i] = M[i-1] * 7L / 4L
            else if (i >= 2 && list[i] - list[i-2] == 2L) M[i] = M[i-1] * 2L
            else M[i] = M[i-1]
        }
        return M[list.size-1]
    }

}