import java.io.InputStream

class Day8Solution : Solution<List<Pair<String, Int>>, Int>() {

    override fun read(input: InputStream): List<Pair<String, Int>> = input.lines.map { l ->
        l.split(" ").let { it[0] to it[1].toInt() }
    }.toList()

    override fun part1(input: List<Pair<String, Int>>): Int {
        var acc = 0
        var curr = 0
        val visited = HashSet<Int>()
        while (curr !in visited) {
            visited.add(curr)
            when (input[curr].first) {
                "acc" -> acc += input[curr].second
                "jmp" -> curr += input[curr].second - 1
            }
            curr++
        }
        return acc
    }

    fun run(input: List<Pair<String, Int>>): Int? {
        var acc = 0
        var curr = 0
        val visited = HashSet<Int>()
        while (curr !in visited) {
            if (curr == input.size) return acc
            visited.add(curr)
            when (input[curr].first) {
                "acc" -> acc += input[curr].second
                "jmp" -> curr += input[curr].second - 1
            }
            curr++
        }
        return null
    }

    fun swap(input: List<Pair<String, Int>>, instruction: Int): List<Pair<String, Int>> {
        val newInstruction = if (input[instruction].first == "jmp") "nop" to input[instruction].second else
            "jmp" to input[instruction].second
        return input.subList(0, instruction) + newInstruction + input.subList(instruction+1, input.size)
    }

    override fun part2(input: List<Pair<String, Int>>): Int {
        input.forEachIndexed { i, ins ->
            if (ins.first != "acc") {
                val instructions = swap(input, i)
                val result = run(instructions)
                if (result != null) return result
            }
        }
        return 0;
    }

}