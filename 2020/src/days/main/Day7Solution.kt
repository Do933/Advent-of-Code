import data.splitAt
import java.io.InputStream

class Day7Solution : Solution<Map<String, List<Pair<String, Int>>>, Int>() {

    override fun read(input: InputStream): Map<String, List<Pair<String, Int>>> = input.lines.toList().associate {
        val parts = it.split(" contain ")
        val bag = parts[0].dropLast(5)
        val content = if (parts[1] == "no other bags.") emptyList() else
            parts[1].dropLast(1).split(", ").map { s -> s.drop(2).dropLast(if (s.endsWith("s")) 5 else 4) to s.take(1).toInt() }
        bag to content
    }

    override fun part1(input: Map<String, List<Pair<String, Int>>>): Int {
        fun getBags(bag: String): Boolean {
            if (input[bag]!!.any { it.first == "shiny gold" }) return true
            if (input[bag]!!.any { getBags(it.first) }) return true
            return false
        }
        return input.keys.count { getBags(it) }
    }

    override fun part2(input: Map<String, List<Pair<String, Int>>>): Int {
        fun getBags(bag: String): Int {
            if (input[bag]!!.isEmpty()) return 1
            return input[bag]!!.map { getBags(it.first) * it.second }.sum() + 1
        }
        return getBags("shiny gold") - 1
    }

}