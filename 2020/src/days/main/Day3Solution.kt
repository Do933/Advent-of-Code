import data.Matrix
import data.matrixRows
import java.io.InputStream

class Day3Solution : Solution<Matrix<Boolean>, Long>() {

    override fun read(input: InputStream): Matrix<Boolean> = input.lines.map { it.chars().mapToObj { c -> c == '#'.toInt() }.toList() }.toList().matrixRows()

    fun getTrees(input: Matrix<Boolean>, slope: Pair<Int, Int>): Long {
        fun get(x: Int, y: Int) = input[x % input.width, y]
        var coords = slope
        var count = 0L
        while (coords.second < input.height) {
            if (get(coords.first, coords.second)) count++
            coords = Pair(coords.first + slope.first, coords.second + slope.second)
        }
        return count
    }

    override fun part1(input: Matrix<Boolean>): Long {
        return getTrees(input, Pair(3, 1))
    }

    override fun part2(input: Matrix<Boolean>): Long =
        listOf(1 to 1, 3 to 1, 5 to 1, 7 to 1, 1 to 2).map { getTrees(input, it) }.reduce(Long::times)

}