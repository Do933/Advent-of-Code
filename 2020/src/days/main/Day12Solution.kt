import java.io.InputStream
import kotlin.math.abs

class Day12Solution : Solution<List<Pair<Char, Int>>, Int>() {

    override fun read(input: InputStream): List<Pair<Char, Int>> = input.lines.map { it.first() to it.drop(1).toInt() }.toList()
    enum class Dir(val dx: Int, val dy: Int) {
        N(0, 1), E(1, 0), S(0, -1), W(-1, 0)
    }

    override fun part1(input: List<Pair<Char, Int>>): Int {
        var x = 0
        var y = 0
        var dir = Dir.E
        input.forEach { (c, n) ->
            when (c) {
                'N' -> y += 1 * n
                'E' -> x += 1 * n
                'S' -> y -= 1 * n
                'W' -> x -= 1 * n
                'L' -> {
                    dir = Dir.values()[(dir.ordinal - (n/90) + 4) % 4]
                }
                'R' -> {
                    dir = Dir.values()[(dir.ordinal + (n/90)) % 4]
                }
                'F' -> { x += dir.dx * n; y += dir.dy * n }
            }
        }
        return abs(x) + abs(y)
    }

    override fun part2(input: List<Pair<Char, Int>>): Int {
        var x = 0
        var y = 0
        var wx = 10
        var wy = 1
        input.forEach { (c, n) ->
            when (c) {
                'N' -> wy += 1 * n
                'E' -> wx += 1 * n
                'S' -> wy -= 1 * n
                'W' -> wx -= 1 * n
                'L' -> (1..(n/90)).forEach {
                    val temp = wx
                    wx = -wy
                    wy = temp
                }
                'R' -> (1..(n/90)).forEach {
                    val temp = wx
                    wx = wy
                    wy = -temp
                }
                'F' -> {
                    x += wx * n
                    y += wy * n
                }
            }
        }
        return abs(x) + abs(y)
    }

}