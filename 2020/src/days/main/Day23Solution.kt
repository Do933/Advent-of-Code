import java.io.InputStream
import java.util.*
import kotlin.collections.ArrayList

data class QNode<T>(val value: T, var prev: QNode<T>?, var next: QNode<T>?) {
    override fun toString(): String = "${prev!!.value} -> ($value) -> ${next!!.value}"
}

class CircularQueue<T>(values: Collection<T>) {
    lateinit var current: QNode<T>
    var size: Int = values.size
    val nodes: MutableMap<T, QNode<T>> = HashMap()

    init {
        var prev: QNode<T>? = null
        values.forEach {
            val node = QNode(it, prev, null)
            if (prev != null) {
                prev!!.next = node
            } else {
                current = node
            }
            prev = node
        }
        prev!!.next = current
        current.prev = prev

        var node = current.next!!
        while (node != current) {
            nodes[node.value] = node
            node = node.next!!
        }
        nodes[current.value] = current
    }

    fun addAfter(node: QNode<T>, value: T) {
        val newNode = QNode(value, node, node.next)
        node.next!!.prev = newNode
        node.next = newNode
        nodes[value] = newNode
        size++
    }
    fun remove(node: QNode<T>): T {
        nodes.remove(node.value)
        node.prev!!.next = node.next
        node.next!!.prev = node.prev
        size--
        return node.value
    }
    fun next(): QNode<T> {
        current = current.next!!
        return current
    }
    fun find(item: T): QNode<T>? {
        return nodes[item]
    }

    override fun toString(): String {
        var node = current.next!!
        val sb = StringBuilder()
        while (node != current) {
            sb.append(node.value)
            if (node.next != current) sb.append(" ")
            node = node.next!!
        }
        return "(${current.value}) ${sb}"
    }
}

class Day23Solution : Solution<List<Int>, String>() {

    override fun read(input: InputStream): List<Int> = input.string.map { it.toString().toInt() }

    fun move(Q: CircularQueue<Int>) {
        val removed = listOf(Q.remove(Q.current.next!!), Q.remove(Q.current.next!!), Q.remove(Q.current.next!!))
        var dest = if (Q.current.value == 1) Q.size + 3 else Q.current.value - 1
        while (dest in removed) {
            dest = if (dest == 1) Q.size + 3 else dest - 1
        }
        val insertAfter = Q.find(dest)!!
        removed.reversed().forEach { Q.addAfter(insertAfter, it) }
        Q.next()
    }

    override fun part1(input: List<Int>): String {
        val Q = CircularQueue(input)
        (1..100).forEach { move(Q) }
        return generateSequence(Q.find(1)!!.next!!) {
            it.next!!
        }.takeWhile { it.value != 1 }.map { it.value }.joinToString("")
    }

    override fun part2(input: List<Int>): String {
        val Q = CircularQueue(input + ((input.max()!! + 1)..1000000).toList())
        (1..10000000).forEach { move(Q) }
        val oneNode = Q.find(1)
        return (oneNode!!.next!!.value.toLong() * oneNode.next!!.next!!.value.toLong()).toString()
    }

}