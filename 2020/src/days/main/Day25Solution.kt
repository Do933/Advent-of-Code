import data.modPow
import java.io.InputStream

class Day25Solution : Solution<Pair<Long, Long>, Long>() {

    val MOD = 20201227L

    override fun read(input: InputStream): Pair<Long, Long> = input.lines.toList().let { (kc, kd) -> kc.toLong() to kd.toLong() }

    override fun part1(input: Pair<Long, Long>): Long {
        val x = (1L..MOD).find { modPow(7, it, MOD) == input.first }!!
        val y = (1L..MOD).find { modPow(7, it, MOD) == input.second }!!
        return modPow(7, x * y, MOD)
    }

    override fun part2(input: Pair<Long, Long>): Long {
        return 0
    }

}