import java.io.InputStream
import java.util.*

class Day22Solution : Solution<Pair<List<Int>, List<Int>>, Long>() {

    override fun read(input: InputStream): Pair<List<Int>, List<Int>> = input.string.split("\n\n").let { (p1, p2) ->
        Pair(p1.lines().drop(1).map { it.toInt() }, p2.lines().drop(1).map { it.toInt() })
    }

    override fun part1(input: Pair<List<Int>, List<Int>>): Long {
        val q1 = LinkedList(input.first)
        val q2 = LinkedList(input.second)
        while (q1.isNotEmpty() && q2.isNotEmpty()) {
            val c1 = q1.removeFirst()
            val c2 = q2.removeFirst()
            if (c1 > c2) {
                q1.addLast(c1)
                q1.addLast(c2)
            } else {
                q2.addLast(c2)
                q2.addLast(c1)
            }
        }
        return if (q1.isNotEmpty()) q1.reversed().mapIndexed { i, v -> (i+1) * v.toLong() }.sum()
            else q2.reversed().mapIndexed { i, v -> (i+1) * v.toLong() }.sum()
    }

    fun playGame(q1: LinkedList<Int>, q2: LinkedList<Int>): Pair<Int, List<Int>> {
        val prevRounds = HashSet<Int>()
        fun prevRoundExists() = prevRounds.contains(Objects.hash(q1.hashCode(), q2.hashCode()))
        fun addRound() = prevRounds.add(Objects.hash(q1.hashCode(), q2.hashCode()))

        while (q1.isNotEmpty() && q2.isNotEmpty()) {
            if (prevRoundExists()) return 1 to q1
            addRound()
            val c1 = q1.removeFirst()
            val c2 = q2.removeFirst()

            var winner = if (c1 > c2) 1 else 2
            if (c1 <= q1.size && c2 <= q2.size) {
                winner = playGame(LinkedList(q1.take(c1)), LinkedList(q2.take(c2))).first
            }

            if (winner == 1) {
                q1.addLast(c1)
                q1.addLast(c2)
            } else {
                q2.addLast(c2)
                q2.addLast(c1)
            }
        }

        return if (q1.isNotEmpty()) 1 to q1 else 2 to q2
    }

    override fun part2(input: Pair<List<Int>, List<Int>>): Long {
        return playGame(LinkedList(input.first), LinkedList(input.second)).second
                .reversed().mapIndexed { i, v -> (i+1) * v.toLong() }.sum()
    }

}