import java.io.InputStream
import java.lang.IllegalArgumentException

sealed class Rule {
    abstract fun regex(): String

    data class SingleChar(val char: Char) : Rule() {
        override fun regex(): String = char.toString()
    }
    data class Concat(val rules: List<Rule>): Rule() {
        override fun regex(): String = rules.joinToString("") { if (it is SingleChar) it.regex() else "(${it.regex()})" }
    }
    data class Disjunct(val rules: List<Rule>): Rule() {
        override fun regex(): String = rules.joinToString("|") { if (it is SingleChar) it.regex() else "(${it.regex()})" }
    }
}

data class Messages(val rules: Map<Int, Rule>, val words: List<String>)

class Day19Solution : Solution<Messages, Int>() {

    fun parseRule(rule: String, ruleMap: Map<Int, String>): Rule = when {
        rule.startsWith("\"") -> Rule.SingleChar(rule[1])
        rule.contains("|") -> Rule.Disjunct(rule.split(" | ").map { parseRule(it, ruleMap) })
        else -> Rule.Concat(rule.split(" ").map { parseRule(ruleMap[it.toInt()]!!, ruleMap) })
    }

    override fun read(input: InputStream): Messages = input.string.split("\n\n").let { (rs, ws) ->
        val ruleMap = rs.lines().associate { it.split(": ")[0].toInt() to it.split(": ")[1] }
        Messages(ruleMap.mapValues { parseRule(it.value, ruleMap) }, ws.lines())
    }

    override fun part1(input: Messages): Int {
        return input.words.count { it.matches(Regex(input.rules[0]!!.regex())) }
    }

    override fun part2(input: Messages): Int {
        val rule42 = input.rules[42]!!
        val rule31 = input.rules[31]!!
        return input.words.map { w ->
            var word = w
            val r42 = Regex(rule42.regex())
            var count = 0
            val words = HashMap<Int, String>()
            while (word.isNotEmpty()) {
                val match = r42.find(word)
                if (match != null && match.range.first == 0) {
                    word = word.substring(match.value.length)
                    count++
                    if (count >= 2) words[count] = word
                }
                else break
            }
            words
        }.map { words ->
            val r31 = Regex(rule31.regex())
            words.mapValues { (_, w) ->
                var word = w
                var count = 0
                while (word.isNotEmpty()) {
                    val match = r31.find(word)
                    if (match != null && match.range.first == 0) {
                        word = word.substring(match.value.length)
                        count++
                    }
                    else break
                }
                if (word.isNotEmpty()) count = 0
                count
            }
        }.count { it.any { (c42, c31) -> c42 > c31 && c31 > 0 } }
    }

}