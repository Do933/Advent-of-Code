import data.tail
import java.io.InputStream

class Day9Solution : Solution<List<Long>, Long>() {

    override fun read(input: InputStream): List<Long> = input.lines.map { it.toLong() }.toList()

    fun invalidSequence(input: List<Long>): List<Long> {
        val preamble = if (input.size > 25) 25 else 5
        var prev = input.take(preamble)
        var list = input.drop(preamble)
        val result = ArrayList(prev)
        while (list.isNotEmpty()) {
            val curr = list[0]
            result.add(curr)

            if (prev.map { curr - it }.none { it in prev }) return result

            list = list.drop(1)
            prev = prev.drop(1) tail curr
        }
        return emptyList()
    }

    override fun part1(input: List<Long>): Long = invalidSequence(input).last()

    override fun part2(input: List<Long>): Long {
        val (list, num) = invalidSequence(input).let { it.dropLast(1) to it.last() }
        return (list.indices).flatMap { i ->
            (i..list.size).map { j -> list.subList(i, j) }.filter { it.sum() == num }
        }.first().let { it.min()!! + it.max()!! }
    }

}