import data.modInvP
import data.splitAt
import data.tupleOf
import io.mockk.InternalPlatformDsl.toStr
import java.io.InputStream
import java.util.*
import kotlin.collections.HashMap
import kotlin.math.abs

class Day15Solution : Solution<List<Int>, Long>() {

    override fun read(input: InputStream): List<Int> = input.lines.toList().first().csv().map { it.toInt() }.toList()

    override fun part1(input: List<Int>): Long {
        var turn = input.size + 1
        val lastSpoken = mutableMapOf(*input.withIndex().map { it.value.toLong() to Pair<Int?, Int>(null, it.index+1) }.toTypedArray())
        var prev = input.last()
        while (turn <= 2020) {
            val last: Pair<Int?, Int> = lastSpoken[prev.toLong()]!!
            prev = if (last.first == null) 0 else last.second - last.first!!
            lastSpoken[prev.toLong()] = lastSpoken[prev.toLong()]?.second to turn
            turn++
        }
        return prev.toLong()
    }

    override fun part2(input: List<Int>): Long {
        var turn = input.size + 1
        val lastSpoken = mutableMapOf(*input.withIndex().map { it.value.toLong() to Pair<Int?, Int>(null, it.index+1) }.toTypedArray())
        var prev = input.last()
        while (turn <= 30000000) {
            val last: Pair<Int?, Int> = lastSpoken[prev.toLong()]!!
            prev = if (last.first == null) 0 else last.second - last.first!!
            lastSpoken[prev.toLong()] = lastSpoken[prev.toLong()]?.second to turn
            turn++
        }
        return prev.toLong()
    }

}