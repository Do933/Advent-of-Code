import data.*
import java.io.File
import java.util.*
import java.util.stream.Collectors
import kotlin.math.abs
import kotlin.time.measureTime

object Oneliners {

    fun day1() =
        println({ l: List<Int>, s: Int -> l.toSet().let { numbers ->
            l.filter { numbers.contains(s - it) }.map { it.toLong() * (s - it).toLong() }.firstOrNull()
        } }.let { find ->
            File("input.txt").readLines().map { it.toInt() }.let { input ->
                find(input, 2020)!! to input.toSet().let { numbers ->
                    input.map { it to find(input, 2020 - it) }.filter { it.second != null }.map { it.first.toLong() * it.second!! }[0]
                }
            }
        })

    fun day2() =
        println(File("input.txt").readLines()
                .map { line -> line.replace(":", "").replace("-", " ").split(" ").let {
                    tupleOf(it[2][0], it[0].toInt(), it[1].toInt(), it[3])
                } }.map { (c, a, b, w) -> (w.count { it == c } in a..b) to ((w[a-1] == c) != (w[b-1] == c))
                }.let { r -> r.count { it.first } to r.count { it.second } })

    fun day3() =
        println(File("input.txt").readLines().map { it.chars().mapToObj { c -> c == '#'.toInt() }.collect(Collectors.toList()) }.matrixRows().let { input ->
            { slope: Pair<Int, Int> -> generateSequence(slope) { Pair(it.first + slope.first, it.second + slope.second) }
                    .takeWhile { it.second < input.height }.filter { input[it.first % input.width, it.second] }.count() }.let { amount ->
                amount(3 to 1) to listOf(1 to 1, 3 to 1, 5 to 1, 7 to 1, 1 to 2).map { amount(it).toLong() }.reduce(Long::times)
            }
        })

    fun day4() =
        println(File("input.txt").readText().split("\n\n").map { data ->
            data.split(Regex("\\s+")).associate { it.split(":")[0] to it.split(":")[1] }
        }.filter { it.keys.containsAll(listOf("byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"))}.let { valid ->
            valid.size to valid.count {
                it["byr"]!!.toInt() in 1920..2002 && it["iyr"]!!.toInt() in 2010..2020 && it["eyr"]!!.toInt() in 2020..2030 &&
                ((it["hgt"]!!.endsWith("cm") && it["hgt"]!!.dropLast(2).toInt() in 150..193) ||
                        (it["hgt"]!!.endsWith("in") && it["hgt"]!!.dropLast(2).toInt() in 59..76)) &&
                it["hcl"]!!.startsWith("#") && it["hcl"]!!.drop(1).matches(Regex("[0-9a-f]{6}")) &&
                it["ecl"]!! in setOf("amb", "blu", "brn", "grn", "gry", "hzl", "oth") && it["pid"]!!.length == 9
        } })

    fun day5() =
        println(File("input.txt").readLines().map { it.replace(Regex("[RB]"), "1").replace(Regex("[FL]"), "0") }.map {
            (it.substring(0, 7) to it.substring(7)).let { (row, column) -> row.toInt(2) * 8 + column.toInt(2) }
        }.let { ids ->
            ids.max()!! to ids.toSet().let { idSet -> (0..1023).first { it !in idSet && it-1 in idSet && it+1 in idSet } }
        })

    fun day6() =
        println(File("input.txt").readText().split("\n\n").map { it.split("\n") }
                .map { group -> group.flatMap { it.toCharArray().toList() }.toSet().let { chars ->
            chars.size to chars.count { c -> group.all { it.contains(c) } }
        } }.reduce { (a1, a2), (b1, b2) -> (a1 + b1) to (a2 + b2) })

    fun day7() =
        println(Pair({ F: Fix<String, Boolean> -> F(F) }.let { fix ->
            { f: ((String) -> Boolean) -> (String) -> Boolean -> fix(Fix { rec -> f { x -> fix(rec)(x) } }) }
        }, { F: Fix<String, Int> -> F.call(F) }.let { fix ->
            { f: ((String) -> Int) -> (String) -> Int -> fix(Fix { rec -> f { x -> fix(rec)(x) } }) }
        }).let { (y1, y2) ->
            File("input.txt").readLines().associate { it.split(" contain ").let { parts ->
                parts[0].dropLast(5) to if (parts[1] == "no other bags.") emptyList() else
                    parts[1].dropLast(1).split(", ").map { s -> s.drop(2).dropLast(if (s.endsWith("s")) 5 else 4) to s.take(1).toInt() }
            } }.let { bags ->
                Pair({ f: () -> (String) -> Boolean -> { bag: String ->
                    bags[bag]!!.any { it.first == "shiny gold" } || bags[bag]!!.any { f()(it.first) }
                } }, { f: () -> (String) -> Int -> { bag: String ->
                    if (bags[bag]!!.isEmpty()) 1 else bags[bag]!!.map { f()(it.first) * it.second }.sum() + 1
                } }).let { (p1, p2) ->
                    bags.keys.count { b -> y1 {p1{it}}(b) } to y2 {p2{it}}("shiny gold") - 1
                }
        } })

    fun day7Iterative() =
        println(File("input.txt").readLines().associate { it.split(" contain ").let { parts ->
            parts[0].dropLast(5) to if (parts[1] == "no other bags.") emptyList() else
                parts[1].dropLast(1).split(", ").map { s -> s.drop(2).dropLast(if (s.endsWith("s")) 5 else 4) to s.take(1).toInt() } }
        }.let { bags ->
            Pair({ s: String ->
                generateSequence<Pair<List<String>, Boolean>>(LinkedList(listOf(s)) to false) { (S, found) ->
                    S.last().let { b -> S.dropLast(1) + bags[b]!!.map { it.first } to (b == "shiny gold" || found) }
                }.takeWhile { (S, _) -> S.isNotEmpty() }.last().second
            }, { s: String ->
                generateSequence<Pair<List<Pair<String, Int>>, Int>>(LinkedList(listOf(s to 1)) to 0) { (S, T) ->
                    S.last().let { (b, n) -> S.dropLast(1) + bags[b]!!.map { it.first to n * it.second } to T + n }
                }.takeWhile { (S, _) -> S.isNotEmpty() }.last().let { (l, T) -> T + l.last().second } - 1
            }).let { (p1, p2) -> bags.keys.count { p1(it) } - 1 to p2("shiny gold") }
        })

    fun day8() =
        println({ input: List<Pair<String, Int>> ->
            generateSequence(tupleOf(0, 0, emptySet<Int>())) { (acc, curr, visited) -> when (input[curr].first) {
                    "acc" -> tupleOf(acc + input[curr].second, curr + 1, visited + setOf(curr))
                    "jmp" -> tupleOf(acc, curr + input[curr].second, visited + setOf(curr))
                    else -> tupleOf(acc, curr + 1, visited + setOf(curr))
                } }.takeWhile { (_, curr, visited) -> curr !in visited && curr < input.size }
                    .last().let { (acc, curr, _) -> acc to (curr + 1 == input.size) }
        }.let { find ->
            File("input.txt").readLines().map { l -> l.split(" ").let { it[0] to it[1].toInt() } }.let { instructions ->
                { input: List<Pair<String, Int>>, instruction: Int ->
                    input.subList(0, instruction) + when (input[instruction].first) {
                        "jmp" -> "nop" to input[instruction].second
                        "nop" -> "jmp" to input[instruction].second
                        else -> input[instruction]
                    } + input.subList(instruction + 1, input.size)
                }.let { swap ->
                    find(instructions).first to instructions.mapIndexed { i, _ -> find(swap(instructions, i)) }.first { it.second }.first
                }
            }
        })

    fun day9() =
        println(File("input.txt").readLines().map { it.toLong() }.let { input ->
            generateSequence(tupleOf(input.take(25), input.drop(25), input.take(25))) { (prev, list, result) ->
                list[0].let { curr ->
                    tupleOf(prev.drop(1) tail curr,
                            if (prev.map { list[0] - it }.none { it in prev }) emptyList() else list.drop(1),
                            result tail curr)
                }
            }.takeWhile { (_, list, _) -> list.isNotEmpty()}
                    .last().let { (_, list, res) -> res tail list[0] }.let { res ->
                Pair(res.dropLast(1), res.last()).let { (list, num) ->
                    num to (list.indices)
                            .flatMap { i -> (i..list.size).map { j -> list.subList(i, j) }.filter { it.sum() == num } }
                            .first().let { it.min()!! + it.max()!! }
                }
            }
        })

    fun day10() =
        println(File("input.txt").readLines().map { it.toLong() }.let { input ->
            input.sorted().let { 0L head it tail it.last() + 3L }.let { list ->
                list.zipWithNext { a, b -> b - a }.let { l -> l.count { it == 3L }.toLong() * l.count { it == 1L }.toLong() } to
                        generateSequence(0 to 1L) { (i, x) -> when {
                            i >= 4 && list[i] - list[i - 4] == 4L -> i + 1 to x * 7L / 4L
                            i >= 2 && list[i] - list[i - 2] == 2L -> i + 1 to x * 2L
                            else -> i + 1 to x
                        } }.take(list.size).last().second
            }
        })

    fun day11() =
        println(File("input.txt").readLines().map { it.toCharArray().toList() }.let { input ->
            { M: List<List<Char>>, i: Int, j: Int, dx: Int, dy: Int ->
                if (i + dx >= 0 && i + dx < M[0].size && j + dy >= 0 && j + dy < M.size) M[j + dy][i + dx] == '#'
                else false }.let { check ->
                { M: List<List<Char>>, i: Int, j: Int, dx: Int, dy: Int ->
                    if (i == 0 && dx < 0 || j == 0 && dy < 0 || i == M[0].size-1 && dx > 0 || j == M.size-1 && dy > 0) false
                    else generateSequence(tupleOf(i+dx, j+dy, '.')) { (x, y, _) ->
                        tupleOf(x+dx, y+dy, M[y][x])
                    }.takeWhile{ (x, y, c) -> x >= 0 && x < M[0].size && y >= 0 && y < M.size && c == '.' }
                            .let { s -> M[s.last().second][s.last().first] == '#' } }.let { scan ->
                    { M: List<List<Char>>, i: Int, j: Int, f: (List<List<Char>>, Int, Int, Int, Int) -> Boolean ->
                        listOf(-1 to 0, -1 to 1, 0 to 1, 1 to 1, 1 to 0, 1 to -1, 0 to -1, -1 to -1)
                                .count { f(M, i, j, it.first, it.second) } }.let { adjacent ->
                        { M: List<List<Char>>, f: (List<List<Char>>, Int, Int, Int, Int) -> Boolean, t: Int ->
                            (M.indices).map { j -> M[j].indices.map { i -> when (M[j][i]) {
                                'L' -> if (adjacent(M, i, j,f) == 0) '#' else 'L'
                                '#' -> if (adjacent(M, i, j, f) >= t) 'L' else '#'
                                else -> M[j][i]
                            } } }
                        }.let { simulate ->
                            Pair(generateSequence(input to simulate(input, check, 4)) { (_, m2) ->
                                m2 to simulate(m2, check, 4)
                            }.takeWhile { it.first != it.second }.last().second.map { r -> r.count { it == '#' } }.sum(),
                            generateSequence(input to simulate(input, scan, 5)) { (_, m2) ->
                                m2 to simulate(m2, scan, 5)
                            }.takeWhile { it.first != it.second }.last().second.map { r -> r.count { it == '#' } }.sum())
                        }
                    }
                }
            }
        })

//    fun day12() {
//        println({ dir: Int -> when (dir % 4) {
//            0 -> 0 to 1
//            1 -> 1 to 0
//            2 -> 0 to -1
//            3 -> -1 to 0
//            else -> 0 to 0
//        } }.let { D ->
//        File("input.txt").readLines().map { it.first() to it.drop(1).toInt() }.let { input ->
//            generateSequence(tupleOf(0, 0, 0, 0, 0, 0, 1, input)) { (x1, y1, x2, y2, wx, wy, dir, ins) ->
//                when (ins[0].first) {
//                    'N' -> tupleOf(x1, y1 + ins[0].second, x2, y2, wx, wy + ins[0].second, dir, ins.drop(1))
//                    'E' -> tupleOf(x1 + ins[0].second, y1, x2, y2, wx + ins[0].second, wy, dir, ins.drop(1))
//                    'S' -> tupleOf(x1, y1 - ins[0].second, x2, y2, wx, wy - ins[0].second, dir, ins.drop(1))
//                    'W' -> tupleOf(x1 - ins[0].second, y1, x2, y2, wx - ins[0].second, wy, dir, ins.drop(1))
//                    'L' -> tupleOf(x1, y1, x2, y2, -wy, wx, (dir + 3) % 4, ins.drop(1))
//                    'R' -> tupleOf(x1, y1, x2, y2, -wy, wx, (dir + 1) % 4, ins.drop(1))
//                    'F' -> tupleOf(x1 + D(dir).first * ins[0].second, y1 + D(dir).second * ins[0].second, x2 + wx * ins[0].second, y2 + wy * ins[0].second, wx, wy, dir, ins.drop(1))
//                    else -> tupleOf(x1, y1, x2, y2, wx, wy, dir, ins)
//                }
//            }.takeWhile { (_, _, _, _, _, _, _, ins) -> ins.isNotEmpty() }.last().let { (x1, y1, x2, y2, _, _, _, _) ->
//                (abs(x1) + abs(y1)) to (abs(x2) to abs(y2))
//            }
//        }
//        })
//    }

    fun day15() =
        println(File("input.txt").readLines().first().split(",").map { it.toInt() }.let { input ->
            generateSequence(tupleOf(input.size + 1,
                    input.withIndex().associate { it.value to Pair<Int?, Int>(null, it.index+1) },
                    input.last())) { (turn, lastSpoken, prev) ->
                lastSpoken[prev]!!.let { last -> (if (last.first == null) 0 else last.second - last.first!!).let { newPrev ->
                    tupleOf(turn + 1, lastSpoken + (newPrev to (lastSpoken[newPrev]?.second to turn)), newPrev)
                } }
            }.let { s -> s.find { it.first > 2020 }!!.third to s.find { it.first > 30000000 }!!.third }
        })

    fun day21() {
        println(File("input.txt").readLines().map { l ->
            l.split(" (").let { (ingr, aler) -> Pair(ingr.split(" ").toList(), aler.dropLast(1).drop(9).split(", ").toList()) }
        }.let { input ->
            Pair(input.flatMap { it.first }.toSet(), input.flatMap { it.second }.toSet()).let { (allIngredients, allAllergens) ->
                allAllergens.associate { a -> a to input.map { (ingrs, alls) ->
                    if (a in alls) ingrs.toSet()
                    else allIngredients
                }.reduce(Set<String>::intersect) }.let { allergens ->
                    Pair(input.map { f -> f.first.count { it in (allIngredients - allergens.values.flatMap { it.toList() }.toSet()) } }.sum(),
                        generateSequence(Pair(allergens, emptyMap<String, String>())) { (aller, final) ->
                            (final + aller.filter { it.value.size == 1 }.map { it.key to it.value.first() }).let { newFinal ->
                                aller.filter { it.key !in newFinal }.mapValues { alls -> alls.value - newFinal.values.toSet() } to newFinal
                            }
                        }.takeWhile { (_, final) -> final.size < allAllergens.size }.last().let { (a, f) -> f + a.mapValues { it.value.first() } }
                            .toList().sortedBy { it.first }.map { it.second }.joinToString(","))
                }
            }
        })
    }

}

fun main() {
    Oneliners.day21()
}