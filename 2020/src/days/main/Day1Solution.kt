import java.io.InputStream

class Day1Solution : Solution<List<Int>, Long>() {

    override fun read(input: InputStream): List<Int> = input.lines.map { it.toInt() }.toList()

    fun findTwo(list: List<Int>, sum: Int): Long? = list.toSet().let { numbers ->
        list.filter { numbers.contains(sum - it) }.map { it.toLong() * (sum - it).toLong() }.firstOrNull()
    }

    override fun part1(input: List<Int>): Long = findTwo(input, 2020)!!

    override fun part2(input: List<Int>): Long = input.toSet().let { numbers ->
        input.map { it to findTwo(input, 2020 - it) }
                .filter { it.second != null }
                .map { it.first.toLong() * it.second!! }[0]
    }

}