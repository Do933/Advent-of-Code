import java.io.InputStream

data class FoodItem(val ingredients: List<String>, val allergens: List<String>)

class Day21Solution : Solution<List<FoodItem>, String>() {

    override fun read(input: InputStream): List<FoodItem> = input.lines.map { l ->
        l.split(" (").let { (ingr, aler) -> FoodItem(ingr.split(" ").toList(), aler.dropLast(1).drop(9).split(", ").toList()) }
    }.toList()

    fun mapAllergens(input: List<FoodItem>): Map<String, Set<String>> {
        val allergens = HashMap<String, Set<String>>()
        val allAllergens = input.flatMap { it.allergens }.toSet()
        val allIngredients = input.flatMap { it.ingredients }.toSet()

        allAllergens.forEach { a -> allergens[a] = allIngredients }

        input.forEach { item ->
            item.allergens.forEach { a ->
                allergens[a] = allergens[a]!!.intersect(item.ingredients)
            }
        }
        return allergens
    }

    override fun part1(input: List<FoodItem>): String {
        val allIngredients = input.flatMap { it.ingredients }.toSet()

        val allergens = mapAllergens(input)

        val noAllergens = allIngredients - allergens.values.flatMap { it.toList() }.toSet()

        return input.map { f -> f.ingredients.count { it in noAllergens } }.sum().toString()
    }

    override fun part2(input: List<FoodItem>): String {
        val allAllergens = input.flatMap { it.allergens }.toSet()

        val allergens = HashMap(mapAllergens(input))
        val finalAllergens = HashMap<String, String>()

        while (finalAllergens.size < allAllergens.size) {
            allergens.forEach { (al, ingrs) ->
                if (ingrs.size == 1) finalAllergens[al] = ingrs.first()
            }
            finalAllergens.forEach { (al, ingr) ->
                allergens.remove(al)
                allergens.forEach { (k, v) -> allergens[k] = v - setOf(ingr) }
            }
        }

        return finalAllergens.toList().sortedBy { it.first }.map { it.second }.joinToString(",")
    }

}