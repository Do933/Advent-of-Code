import data.splitAt
import java.io.InputStream

class Day6Solution : Solution<List<List<String>>, Int>() {

    override fun read(input: InputStream): List<List<String>> = input.string.split("\n\n").map { it.split("\n") }

    override fun part1(input: List<List<String>>): Int = input.map { group -> group.flatMap { it.toCharArray().toList() }.distinct().count() }.sum()

    override fun part2(input: List<List<String>>): Int = input.map { group -> group.flatMap { it.toCharArray().toList() }.toSet().let { chars ->
        chars.count { c -> group.all { it.contains(c) } }
    } }.sum()

}