import java.io.InputStream

data class Password(val character: Char, val min: Int, val max: Int, val password: String) {
    val isValid: Boolean get() = password.count { it == character }.let { it in min..max }
    val isValidNew: Boolean get() = (password[min-1] == character) != (password[max-1] == character)
}

class Day2Solution : Solution<List<Password>, Int>() {

    override fun read(input: InputStream): List<Password> = input.lines.map { it.split(" ").let { parts ->
        Password(parts[1][0], parts[0].split("-")[0].toInt(), parts[0].split("-")[1].toInt(), parts[2])
    } }.toList()

    override fun part1(input: List<Password>): Int = input.count { it.isValid }

    override fun part2(input: List<Password>): Int = input.count { it.isValidNew }

}