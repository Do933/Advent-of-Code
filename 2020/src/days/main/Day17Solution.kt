import java.io.InputStream

data class Coords(val x: Int, val y: Int, val z: Int)
data class Coords4(val x: Int, val y: Int, val z: Int, val w: Int)

class Day17Solution : Solution<Set<Coords>, Int>() {

    override fun read(input: InputStream): Set<Coords> = input.lines.toList().withIndex().flatMap { (y, l) ->
        l.withIndex().filter { (_, c) -> c == '#' }.map { (x, _) -> Coords(x, y, 0) }
    }.toSet()

    override fun part1(input: Set<Coords>): Int {
        var cells = input
        val minX = input.map { it.x }.min()!! - 7
        val minY = input.map { it.y }.min()!! - 7
        val minZ = input.map { it.z }.min()!! - 7
        val maxX = input.map { it.x }.max()!! + 7
        val maxY = input.map { it.y }.max()!! + 7
        val maxZ = input.map { it.z }.max()!! + 7

        fun neighbours(pos: Coords): Int {
            var count = 0
            (-1..1).forEach { dx ->
                (-1..1).forEach { dy ->
                    (-1..1).forEach { dz ->
                        if ((dx != 0 || dy != 0 || dz != 0) && Coords(pos.x + dx, pos.y + dy, pos.z + dz) in cells) count++
                    }
                }
            }
            return count
        }

        (1..6).forEach { _ ->
            val newCells = HashSet<Coords>()
            (minX..maxX).forEach { x ->
                (minY..maxY).forEach { y ->
                    (minZ..maxZ).forEach { z ->
                        val c = Coords(x, y, z)
                        val n = neighbours(c)
                        if (c in cells && n in 2..3 || c !in cells && n == 3) newCells.add(c)
                    }
                }
            }
            cells = newCells
        }

        return cells.size
    }

    override fun part2(input: Set<Coords>): Int {
        var cells = input.map { Coords4(it.x, it.y, it.z, 0) }.toSet()
        val minX = cells.map { it.x }.min()!! - 7
        val minY = cells.map { it.y }.min()!! - 7
        val minZ = cells.map { it.z }.min()!! - 7
        val minW = cells.map { it.w }.min()!! - 7
        val maxX = cells.map { it.x }.max()!! + 7
        val maxY = cells.map { it.y }.max()!! + 7
        val maxZ = cells.map { it.z }.max()!! + 7
        val maxW = cells.map { it.w }.max()!! + 7

        fun neighbours(pos: Coords4): Int {
            var count = 0
            (-1..1).forEach { dx ->
                (-1..1).forEach { dy ->
                    (-1..1).forEach { dz ->
                        (-1..1).forEach { dw ->
                            if ((dx != 0 || dy != 0 || dz != 0 || dw != 0)
                                    && Coords4(pos.x + dx, pos.y + dy, pos.z + dz, pos.w + dw) in cells) count++
                        }
                    }
                }
            }
            return count
        }

        (1..6).forEach { _ ->
            val newCells = HashSet<Coords4>()
            (minX..maxX).forEach { x ->
                (minY..maxY).forEach { y ->
                    (minZ..maxZ).forEach { z ->
                        (minW..maxW).forEach { w ->
                            val c = Coords4(x, y, z, w)
                            val n = neighbours(c)
                            if (c in cells && n in 2..3 || c !in cells && n == 3) newCells.add(c)
                        }
                    }
                }
            }
            cells = newCells
        }

        return cells.size
    }

}