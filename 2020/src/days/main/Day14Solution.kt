import data.modInvP
import data.splitAt
import data.tupleOf
import io.mockk.InternalPlatformDsl.toStr
import java.io.InputStream
import java.util.*
import kotlin.collections.HashMap
import kotlin.math.abs

sealed class MemInstruction
data class Mask(val mask: String) : MemInstruction() {
    val zeroMask: Long = mask.replace("X", "1").toLong(2)
    val oneMask: Long = mask.replace("X", "0").toLong(2)
}
data class MemSet(val addr: Int, val value: Long) : MemInstruction()

class Day14Solution : Solution<List<MemInstruction>, Long>() {

    override fun read(input: InputStream): List<MemInstruction> = input.lines.map { line ->
        if (line.startsWith("mask = ")) Mask(line.drop(7))
        else line.split(" = ").let { (mem, value) -> MemSet(mem.drop(4).dropLast(1).toInt() ,value.toLong()) }
    }.toList()

    override fun part1(input: List<MemInstruction>): Long {
        val M = LongArray(100000)
        var mask = Mask("X")
        input.forEach { when (it) {
            is Mask -> mask = Mask(it.mask)
            is MemSet -> M[it.addr] = (it.value and mask.zeroMask) or mask.oneMask
        } }
        return M.sum()
    }

    override fun part2(input: List<MemInstruction>): Long {
        val M = HashMap<Long, Long>()
        var mask = Mask("X")
        fun addresses(a: Long): List<Long> {
            val indices = mask.mask.indices.filter { mask.mask[it] == 'X' }
            val addr = a or mask.oneMask
            if (indices.isEmpty()) return listOf(addr)
            val bits = 1 shl (indices.size)
            val addresses = LinkedList<Long>()
            (0 until bits).forEach { bit ->
                var m = addr.toString(2)
                m = "0".repeat(mask.mask.length-m.length) + m
                val s = ("0".repeat(34) + bit.toString(2)).reversed()
                indices.forEachIndexed { i, idx ->
                    m = m.splitAt(idx).let { (h, t) -> h + s[i] + t.drop(1) }
                }
                addresses.add((addr and m.replace("X", "1").toLong(2)) or m.replace("X", "0").toLong(2))
            }
            return addresses
        }
        input.forEach { when (it) {
            is Mask -> mask = Mask(it.mask)
            is MemSet -> addresses(it.addr.toLong()).forEach { a ->
                M[a] = it.value
            }
        } }
        return M.values.sum()
    }

}