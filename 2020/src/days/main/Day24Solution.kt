import data.Matrix
import java.io.InputStream
import java.util.*
import kotlin.collections.ArrayList

data class Hexagon(val id: Long, val x: Int, val y: Int, var flipped: Boolean = false) {
    val northEast: Pair<Int, Int> = x+1 to y-1
    val northWest: Pair<Int, Int> = x to y-1
    val southEast: Pair<Int, Int> = x to y+1
    val southWest: Pair<Int, Int> = x-1 to y+1
    val east: Pair<Int, Int> = x+1 to y
    val west: Pair<Int, Int> = x-1 to y

    val neighbours: Set<Pair<Int, Int>> = setOf(northEast, northWest, southEast, southWest, east, west)

    override fun equals(other: Any?): Boolean = other is Hexagon && this.id == other.id
    override fun hashCode(): Int = Objects.hash(id)
}

enum class HexInstruction {
    NE, SE, NW, SW, E, W
}

class Day24Solution : Solution<List<List<HexInstruction>>, Int>() {

    override fun read(input: InputStream): List<List<HexInstruction>> = input.lines.map { line ->
        var l = line
        val ins = ArrayList<HexInstruction>()
        while (l.isNotEmpty()) {
            HexInstruction.values().forEach { if (l.startsWith(it.name.toLowerCase())) ins.add(it) }
            l = l.substring(ins.last().name.length)
        }
        ins
    }.toList()

    fun makeFloor(input: List<List<HexInstruction>>, w: Int, h: Int): Matrix<Hexagon> {
        val floor = Matrix(w, h) { x, y ->
            Hexagon(y * w.toLong() + x, x, y)
        }
        input.forEach { instructions ->
            var curr = floor[w / 2, h / 2]
            instructions.forEach { ins ->
                val next = when (ins) {
                    HexInstruction.NE -> curr.northEast
                    HexInstruction.SE -> curr.southEast
                    HexInstruction.NW -> curr.northWest
                    HexInstruction.SW -> curr.southWest
                    HexInstruction.E -> curr.east
                    HexInstruction.W -> curr.west
                }
                curr = floor[next.first, next.second]
            }
            curr.flipped = !curr.flipped
        }
        return floor
    }

    override fun part1(input: List<List<HexInstruction>>): Int {
        return makeFloor(input, 100, 100).data.count { it.flipped }
    }

    override fun part2(input: List<List<HexInstruction>>): Int {
        fun simulate(floor: Matrix<Hexagon>): Matrix<Hexagon> {
            return Matrix(floor.width, floor.height) { x, y ->
                if (x == 0 || y == 0 || x == floor.width-1 || y == floor.height-1) return@Matrix floor[x, y]
                val neighbours = floor[x, y].neighbours.count { (nx, ny) -> floor[nx, ny].flipped }
                if (floor[x, y].flipped && (neighbours == 0 || neighbours > 2))
                    Hexagon(floor[x, y].id, x, y, false)
                else if (!floor[x, y].flipped && neighbours == 2)
                    Hexagon(floor[x, y].id, x, y, true)
                else
                    floor[x, y]
            }
        }

        var floor = makeFloor(input, 200, 200)
        (1..100).forEach { _ ->
            floor = simulate(floor)
        }
        return floor.data.count { it.flipped }
    }

}