import data.Matrix
import data.MutableMatrix
import data.matrixColumns
import data.matrixRows
import java.io.InputStream
import java.lang.IllegalArgumentException
import kotlin.math.sqrt

fun Matrix<Boolean>.rotated(): Matrix<Boolean> = this.columns.map { it.reversed() }.matrixRows()
fun Matrix<Boolean>.repr(): String = this.rows.map { r -> r.map { if (it) "#" else "." }.joinToString("") }.joinToString("\n")
fun Matrix<Boolean>.flipped(): Matrix<Boolean> = this.columns.map { it.reversed() }.matrixColumns()

enum class TileAction {
    NO_ACTION, ROTATE_1, ROTATE_2, ROTATE_3,
    NO_ROTATE_FX, ROTATE_1_FX, ROTATE_2_FX, ROTATE_3_FX
}

fun Matrix<Boolean>.applyAction(action: TileAction): Matrix<Boolean> = when (action) {
    TileAction.NO_ACTION -> this
    TileAction.ROTATE_1 -> this.rotated()
    TileAction.ROTATE_2 -> this.rotated().rotated()
    TileAction.ROTATE_3 -> this.rotated().rotated().rotated()
    TileAction.NO_ROTATE_FX -> this.flipped()
    TileAction.ROTATE_1_FX -> this.rotated().flipped()
    TileAction.ROTATE_2_FX -> this.rotated().rotated().flipped()
    TileAction.ROTATE_3_FX -> this.rotated().rotated().rotated().flipped()
}

data class Tile(val id: Long, val matrix: Matrix<Boolean>) {
    fun topEdge(): List<Boolean> = matrix.rows.first()
    fun bottomEdge(): List<Boolean> = matrix.rows.last()
    fun leftEdge(): List<Boolean> = matrix.columns.first()
    fun rightEdge(): List<Boolean> = matrix.columns.last()

    fun edges(): Set<List<Boolean>> = setOf(matrix.rows.first(), matrix.rows.last(), matrix.columns.first(), matrix.columns.last())
    fun edgesFlipped(): Set<List<Boolean>> = setOf(matrix.rows.first().reversed(), matrix.rows.last().reversed(), matrix.columns.first().reversed(), matrix.columns.last().reversed())

    fun matchRight(edge: List<Boolean>): Set<TileAction> {
        val actions = HashSet<TileAction>()
        if (edge == leftEdge()) actions.add(TileAction.NO_ACTION)
        if (edge == leftEdge().reversed()) actions.add(TileAction.NO_ROTATE_FX)
        if (edge == topEdge()) actions.add(TileAction.ROTATE_3_FX)
        if (edge == topEdge().reversed()) actions.add(TileAction.ROTATE_3)
        if (edge == rightEdge()) actions.add(TileAction.ROTATE_2_FX)
        if (edge == rightEdge().reversed()) actions.add(TileAction.ROTATE_2)
        if (edge == bottomEdge()) actions.add(TileAction.ROTATE_1)
        if (edge == bottomEdge().reversed()) actions.add(TileAction.ROTATE_1_FX)
        return actions
    }

    fun matchBottom(edge: List<Boolean>): Set<TileAction> {
        val actions = HashSet<TileAction>()
        if (edge == topEdge()) actions.add(TileAction.NO_ACTION)
        if (edge == topEdge().reversed()) actions.add(TileAction.ROTATE_2_FX)
        if (edge == rightEdge()) actions.add(TileAction.ROTATE_3)
        if (edge == rightEdge().reversed()) actions.add(TileAction.ROTATE_1_FX)
        if (edge == bottomEdge()) actions.add(TileAction.NO_ROTATE_FX)
        if (edge == bottomEdge().reversed()) actions.add(TileAction.ROTATE_2)
        if (edge == leftEdge()) actions.add(TileAction.ROTATE_3_FX)
        if (edge == leftEdge().reversed()) actions.add(TileAction.ROTATE_1)
        return actions
    }

    override fun equals(other: Any?): Boolean = other is Tile && this.id == other.id
    override fun hashCode(): Int = id.hashCode()
}

class Day20Solution : Solution<List<Tile>, Long>() {

    override fun read(input: InputStream): List<Tile> = input.string.split("\n\n").map { t ->
        val lines = t.lines()
        Tile(lines.first().drop(5).dropLast(1).toLong(), lines.drop(1).map { it.toCharArray().map { it == '#' } }.matrixRows())
    }

    override fun part1(input: List<Tile>): Long {
        return input.filter { t ->
            input.filter { it != t }.count { it.edges()
                    .any { e -> t.edges().contains(e) || t.edgesFlipped().contains(e) } } == 2
        }.map { it.id }.reduce(Long::times)
    }

    fun findSeaMonsters(sea: Matrix<Boolean>): Long {
        var count = 0L
        (0 until sea.width - 20).forEach { x ->
            (0 until sea.height - 3).forEach { y ->
                if (sea[x+18, y] &&
                        sea[x, y+1] && sea[x+5, y+1] && sea[x+6, y+1] && sea[x+11, y+1] &&
                        sea[x+12, y+1] && sea[x+17, y+1] && sea[x+18, y+1] && sea[x+19, y+1] &&
                        sea[x+1, y+2] && sea[x+4, y+2] && sea[x+7, y+2] && sea[x+10, y+2] && sea[x+13, y+2] && sea[x+16, y+2])
                            count++
            }
        }
        return count
    }

    override fun part2(input: List<Tile>): Long {
        val N = sqrt(input.size.toDouble()).toInt()
        val corners = input.filter { t ->
            input.filter { it != t }.count { it.edges()
                    .any { e -> t.edges().contains(e) || t.edgesFlipped().contains(e) } } == 2 }
        val image = MutableMatrix<Tile?>(N, N) { _, _ -> null }

        val tiles = HashSet<Tile>(input)

        fun fillRow(y: Int, x: Int = 0) {
            if (x == image.width - 1) return
            val tile = image[x, y]!!
            val next = tiles.filter { it != tile }.map { it to it.matchRight(tile.rightEdge()) }.filter { (_, m) -> m.isNotEmpty() }
            if (next.size == 1) {
                val res = next.first()
                tiles.remove(res.first)
                image[x+1, y] = res.let { (t, a) -> Tile(t.id, t.matrix.applyAction(a.first())) }
                fillRow(y, x+1)
            }
        }

        fun fillColumn(x: Int, y: Int = 0) {
            if (y == image.height - 1) return
            val tile = image[x, y]!!
            val next = tiles.filter { it != tile }.map { it to it.matchBottom(tile.bottomEdge()) }.filter { (_, m) -> m.isNotEmpty() }
            if (next.size == 1) {
                val res = next.first()
                tiles.remove(res.first)
                image[x, y+1] = res.let { (t, a) -> Tile(t.id, t.matrix.applyAction(a.first())) }
                fillColumn(x, y+1)
            }
        }

        val topLeft = corners.filter { c ->
            val other = input.filter { it != c }.flatMap { it.edges() }.toSet()
            c.topEdge() !in other && c.leftEdge() !in other
        }[1]
        image[0, 0] = topLeft
        tiles.remove(topLeft)
        fillColumn(0)
        fillRow(0)
        (0 until image.width).forEach { fillColumn(it) }

        println(image.map { it?.id })

        require(image.data.all { it != null })

        val w = image[0, 0]!!.matrix.width - 2
        val h = image[0, 0]!!.matrix.height - 2
        val finalImage = Matrix(N * w, N * h) { x, y ->
            val M = image[x / w, y / h]!!.matrix
            M[(x % w) + 1, (y % h) + 1]
        }

        return finalImage.data.count { it } -
                listOf(finalImage, finalImage.rotated(), finalImage.rotated().rotated(), finalImage.rotated().rotated().rotated(),
                finalImage.flipped(), finalImage.rotated().flipped(), finalImage.rotated().rotated().flipped(),
                finalImage.rotated().rotated().rotated().flipped()).map {
                    it to findSeaMonsters(it)
        }.find { (_, cnt) -> cnt > 0 }!!.second * 15
    }

}