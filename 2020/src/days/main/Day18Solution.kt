import java.io.InputStream
import java.lang.IllegalArgumentException

class Day18Solution : Solution<List<String>, Long>() {

    override fun read(input: InputStream): List<String> = input.lines.map { it.replace(" ", "") }.toList()

    fun findPart(eq: String): String {
        if (eq.startsWith("(")) {
            var d = 1
            var i = 1
            while (d > 0) {
                if (eq[i] == '(') d++
                else if (eq[i] == ')') d--
                i++
            }
            return eq.substring(0, i)
        }
        if (eq.startsWith("+") || eq.startsWith("*")) return eq.substring(0, 1)
        return Regex("[0-9]+").find(eq)!!.value
    }

    override fun part1(input: List<String>): Long {
        fun eval(eq: String): Long {
            val leftEq = findPart(eq)
            val left = if (leftEq.startsWith("(")) eval(leftEq.drop(1).dropLast(1)) else leftEq.toLong()
            val rightEq = findPart(eq.drop(leftEq.length+1))
            val right = if (rightEq.startsWith("(")) eval(rightEq.drop(1).dropLast(1)) else rightEq.toLong()
            val op = eq[leftEq.length]
            val value = when (op) {
                '*' -> left * right
                '+' -> left + right
                else -> throw IllegalArgumentException()
            }
            if (leftEq.length + rightEq.length + 1 < eq.length) {
                return eval(value.toString() + eq.substring(leftEq.length + rightEq.length + 1))
            }
            return value
        }
        return input.map { eval(it) }.sum()
    }

    sealed class Expr {
        abstract fun eval(): Long
        data class Plus(val left: Expr, val right: Expr) : Expr() {
            override fun eval(): Long = left.eval() + right.eval()
            override fun toString(): String = "($left) + ($right)"
        }
        data class Mult(val left: Expr, val right: Expr) : Expr() {
            override fun eval(): Long = left.eval() * right.eval()
            override fun toString(): String = "($left) * ($right)"
        }
        data class Value(val value: Long) : Expr() {
            override fun eval(): Long = value
            override fun toString(): String = value.toString()
        }
    }

    override fun part2(input: List<String>): Long {
        fun parse(eq: String): List<String> {
            if (eq.length == 0) return emptyList()
            val part = findPart(eq)
            return listOf(part) + parse(eq.substring(part.length))
        }
        fun eval(eq: List<String>): Expr {
            fun eval(part: String): Expr = if (part.startsWith("(")) eval(parse(part.drop(1).dropLast(1)))
                                           else Expr.Value(part.toLong())
            if (eq.size == 1) return eval(eq.first())
            val firstMult = eq.indexOf("*")
            if (firstMult != -1) {
                return Expr.Mult(eval(eq.subList(0, firstMult)), eval(eq.subList(firstMult+1, eq.size)))
            }
            val firstPlus = eq.indexOf("+")
            return Expr.Plus(eval(eq.subList(0, firstPlus)), eval(eq.subList(firstPlus+1, eq.size)))
        }
        return input.map { eval(parse(it)).eval() }.sum()
    }

}