import data.splitAt
import java.io.InputStream

class Day5Solution : Solution<List<String>, Int>() {

    override fun read(input: InputStream): List<String> = input.lines.toList()

    fun ids(input: List<String>): List<Int> = input.map { it.replace(Regex("[RB]"), "1").replace(Regex("[FL]"), "0") }.map {
        it.splitAt(7).let { (row, column) ->
            row.toInt(2) * 8 + column.toInt(2)
        }
    }

    override fun part1(input: List<String>): Int = ids(input).max()!!

    override fun part2(input: List<String>): Int = ids(input).toSet().let { ids ->
        (0..1023).filter { it !in ids && (it-1) in ids && (it+1) in ids }.first()
    }

}