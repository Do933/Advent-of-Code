import data.Matrix

class Day12Test : SolutionTest<List<Pair<Char, Int>>, Int>() {
    override val solution: Day12Solution = Day12Solution()
    override val expected: Map<Input, Output<Int>> = mapOf(
            Input("Example", raw("""
                F10
                N3
                F7
                R90
                F11
            """.trimIndent())) to Pair(25, 286).output()
    )
}