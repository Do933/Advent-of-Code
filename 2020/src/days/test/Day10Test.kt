class Day10Test : SolutionTest<List<Long>, Long>() {
    override val solution: Day10Solution = Day10Solution()
    override val expected: Map<Input, Output<Long>> = mapOf(
            Input("Example", raw("""
                16
                10
                15
                5
                1
                11
                7
                19
                6
                12
                4
            """.trimIndent())) to Pair(35L, 8L).output(),
            Input("Example 2", raw("""
                28
                33
                18
                42
                31
                14
                46
                20
                48
                47
                24
                23
                49
                45
                19
                38
                39
                11
                1
                32
                25
                35
                8
                17
                7
                9
                4
                2
                34
                10
                3
            """.trimIndent())) to Pair(220L, 19208L).output()
    )
}