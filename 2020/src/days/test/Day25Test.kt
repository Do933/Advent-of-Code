class Day25Test : SolutionTest<Pair<Long, Long>, Long>() {
    override val solution: Day25Solution = Day25Solution()
    override val expected: Map<Input, Output<Long>> = mapOf(
            Input("Example 0", raw("""
                5764801
                17807724
            """.trimIndent())) to Pair(14897079L, 0L).output()
    )
}