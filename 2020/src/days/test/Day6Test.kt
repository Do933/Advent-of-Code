class Day6Test : SolutionTest<List<List<String>>, Int>() {
    override val solution: Day6Solution = Day6Solution()
    override val expected: Map<Input, Output<Int>> = mapOf(
            Input("Example", raw("""
                abc

                a
                b
                c

                ab
                ac

                a
                a
                a
                a

                b
            """.trimIndent())) to Pair(11, 6).output()
    )
}