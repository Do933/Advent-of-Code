class Day21Test : SolutionTest<List<FoodItem>, String>() {
    override val solution: Day21Solution = Day21Solution()
    override val expected: Map<Input, Output<String>> = mapOf(
            Input("Example 0", raw("""
                mxmxvkd kfcds sqjhc nhms (contains dairy, fish)
                trh fvjkl sbzzf mxmxvkd (contains dairy)
                sqjhc fvjkl (contains soy)
                sqjhc mxmxvkd sbzzf (contains fish)
            """.trimIndent())) to Pair("5", "mxmxvkd,sqjhc,fvjkl").output()
    )
}