class Day15Test : SolutionTest<List<Int>, Long>() {
    override val solution: Day15Solution = Day15Solution()
    override val expected: Map<Input, Output<Long>> = mapOf(
            Input("Example 0", raw("""
                0,3,6
            """.trimIndent())) to 436L.output(),
            Input("Example 1", raw("""
                1,3,2
            """.trimIndent())) to 1L.output(),
            Input("Example 2", raw("""
                2,1,3
            """.trimIndent())) to 10L.output(),
            Input("Example 3", raw("""
                1,2,3
            """.trimIndent())) to 27L.output(),
            Input("Example 4", raw("""
                2,3,1
            """.trimIndent())) to 78L.output(),
            Input("Example 5", raw("""
                3,2,1
            """.trimIndent())) to 438L.output(),
            Input("Example 6", raw("""
                3,1,2
            """.trimIndent())) to 1836L.output()
    )
}