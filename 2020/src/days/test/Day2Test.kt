class Day2Test : SolutionTest<List<Password>, Int>() {
    override val solution: Day2Solution = Day2Solution()
    override val expected: Map<Input, Output<Int>> = mapOf(
            Input("Example", raw("""
                1-3 a: abcde
                1-3 b: cdefg
                2-9 c: ccccccccc
            """.trimIndent())) to Pair(2, 1).output()
    )
}