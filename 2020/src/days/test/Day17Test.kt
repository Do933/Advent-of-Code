class Day17Test : SolutionTest<Set<Coords>, Int>() {
    override val solution: Day17Solution = Day17Solution()
    override val expected: Map<Input, Output<Int>> = mapOf(
            Input("Example 0", raw("""
                .#.
                ..#
                ###
            """.trimIndent())) to Pair(112, 848).output()
    )
}