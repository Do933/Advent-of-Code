class Day8Test : SolutionTest<List<Pair<String, Int>>, Int>() {
    override val solution: Day8Solution = Day8Solution()
    override val expected: Map<Input, Output<Int>> = mapOf(
            Input("Example", raw("""
                nop +0
                acc +1
                jmp +4
                acc +3
                jmp -3
                acc -99
                acc +1
                jmp -4
                acc +6
            """.trimIndent())) to Pair(5, 8).output()
    )
}