class Day16Test : SolutionTest<Tickets, Long>() {
    override val solution: Day16Solution = Day16Solution()
    override val expected: Map<Input, Output<Long>> = mapOf(
            Input("Example 0", raw("""
                class: 1-3 or 5-7
                row: 6-11 or 33-44
                seat: 13-40 or 45-50
                
                your ticket:
                7,1,14
                
                nearby tickets:
                7,3,47
                40,4,50
                55,2,20
                38,6,12
            """.trimIndent())) to 71L.output()
    )
}