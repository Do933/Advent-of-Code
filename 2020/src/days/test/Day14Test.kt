class Day14Test : SolutionTest<List<MemInstruction>, Long>() {
    override val solution: Day14Solution = Day14Solution()
    override val expected: Map<Input, Output<Long>> = mapOf(
            Input("Example", raw("""
                mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
                mem[8] = 11
                mem[7] = 101
                mem[8] = 0
            """.trimIndent())) to 165L.output(),
            Input("Example 2", raw("""
                mask = 000000000000000000000000000000X1001X
                mem[42] = 100
                mask = 00000000000000000000000000000000X0XX
                mem[26] = 1
            """.trimIndent())) to Pair(51L, 208L).output()
    )
}