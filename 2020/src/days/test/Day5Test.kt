class Day5Test : SolutionTest<List<String>, Int>() {
    override val solution: Day5Solution = Day5Solution()
    override val expected: Map<Input, Output<Int>> = mapOf(
            Input("Example", raw("""
                BFFFBBFRRR""".trimIndent())) to 567.output(),
            Input("Example", raw("""
                FFFBBBFRRR""".trimIndent())) to 119.output(),
    )
}