class Day1Test : SolutionTest<List<Int>, Long>() {
    override val solution: Day1Solution = Day1Solution()
    override val expected: Map<Input, Output<Long>> = mapOf(
        Input("Example", raw("""
            1721
            979
            366
            299
            675
            1456
        """.trimIndent())) to Pair(514579L, 241861950L).output()
    )
}