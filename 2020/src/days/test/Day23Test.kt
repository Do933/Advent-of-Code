class Day23Test : SolutionTest<List<Int>, String>() {
    override val solution: Day23Solution = Day23Solution()
    override val expected: Map<Input, Output<String>> = mapOf(
            Input("Example 0", raw("""
                389125467
            """.trimIndent())) to Pair("67384529", "149245887792").output()
    )
}