class Day18Test : SolutionTest<List<String>, Long>() {
    override val solution: Day18Solution = Day18Solution()
    override val expected: Map<Input, Output<Long>> = mapOf(
            Input("Example 0", raw("""
                1 + (2 * 3) + (4 * (5 + 6))
            """.trimIndent())) to Pair(51L, 51L).output(),
            Input("Example 1", raw("""
                2 * 3 + (4 * 5)
            """.trimIndent())) to Pair(26L, 46L).output(),
            Input("Example 2", raw("""
                5 + (8 * 3 + 9 + 3 * 4 * 3)
            """.trimIndent())) to Pair(437L, 1445L).output(),
            Input("Example 3", raw("""
                5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))
            """.trimIndent())) to Pair(12240L, 669060L).output(),
            Input("Example 4", raw("""
                ((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2
            """.trimIndent())) to Pair(13632L, 23340L).output()
    )
}