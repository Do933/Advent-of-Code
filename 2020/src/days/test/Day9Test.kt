class Day9Test : SolutionTest<List<Long>, Long>() {
    override val solution: Day9Solution = Day9Solution()
    override val expected: Map<Input, Output<Long>> = mapOf(
            Input("Example", raw("""
                35
                20
                15
                25
                47
                40
                62
                55
                65
                95
                102
                117
                150
                182
                127
                219
                299
                277
                309
                576
            """.trimIndent())) to Pair(127L, 62L).output()
    )
}