import data.Matrix

class Day3Test : SolutionTest<Matrix<Boolean>, Long>() {
    override val solution: Day3Solution = Day3Solution()
    override val expected: Map<Input, Output<Long>> = mapOf(
            Input("Example", raw("""
                ..##.......
                #...#...#..
                .#....#..#.
                ..#.#...#.#
                .#...##..#.
                ..#.##.....
                .#.#.#....#
                .#........#
                #.##...#...
                #...##....#
                .#..#...#.#""".trimIndent())) to Pair(7L, 336L).output()
    )
}