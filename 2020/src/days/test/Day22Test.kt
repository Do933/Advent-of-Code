class Day22Test : SolutionTest<Pair<List<Int>, List<Int>>, Long>() {
    override val solution: Day22Solution = Day22Solution()
    override val expected: Map<Input, Output<Long>> = mapOf(
            Input("Example 0", raw("""
                Player 1:
                9
                2
                6
                3
                1
                
                Player 2:
                5
                8
                4
                7
                10
            """.trimIndent())) to Pair(306L, 291L).output()
    )
}