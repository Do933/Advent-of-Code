class Day13Test : SolutionTest<BusSchedule, Long>() {
    override val solution: Day13Solution = Day13Solution()
    override val expected: Map<Input, Output<Long>> = mapOf(
            Input("Example", raw("""
                939
                7,13,x,x,59,x,31,19
            """.trimIndent())) to Pair(295L, 1068781L).output(),
            Input("Example 2", raw("""
                0
                17,x,13,19
            """.trimIndent())) to Pair(0L, 3417L).output(),
            Input("Example 4", raw("""
                0
                67,7,x,59,61
            """.trimIndent())) to Pair(0L, 1261476L).output(),
            Input("Example 5", raw("""
                0
                1789,37,47,1889
            """.trimIndent())) to Pair(0L, 1202161486L).output()
    )
}