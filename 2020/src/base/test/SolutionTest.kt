import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.*
import org.junit.jupiter.api.DynamicTest.dynamicTest
import java.io.ByteArrayInputStream
import java.io.FileInputStream
import java.io.InputStream

abstract class SolutionTest<I: Any, O: Any> {

    sealed class Output<T>(val part1: T, val part2: T?) {
        data class PairOutput<T>(val result: Pair<T, T>) : Output<T>(result.first, result.second)
        data class Part1Output<T>(val result: T) : Output<T>(result, null)
    }
    fun Pair<O, O>.output() = Output.PairOutput(this)
    fun O.output() = Output.Part1Output(this)

    protected data class Input(val name: String, val input: InputStream)

    protected fun file(name: String): InputStream = FileInputStream("resources/$name")
    protected fun raw(string: String): InputStream = ByteArrayInputStream(string.toByteArray())

    protected open val input: String get() = "input_${this::class.simpleName!!.replace("Test", "").toLowerCase()}.txt"

    protected abstract val solution: Solution<I, O>
    protected abstract val expected: Map<Input, Output<O>>

    @TestFactory
    fun `Tests`(): Iterable<DynamicTest> = expected.flatMap { (inputData, expected) ->
        val input = solution.read(inputData.input)
        listOf(
            dynamicTest("${inputData.name}: Part 1") { check(expected.part1, solution.part1(input)) }
        ) + if (expected.part2 == null) emptyList<DynamicTest>() else listOf(dynamicTest("${inputData.name}: Part 2") {
            try {
                val value = solution.part2(input)
                check(expected.part2, value)
            } catch (e: TodoException) { println("Not implemented yet") }
        })
    }

    open fun check(expected: O, value: O) {
        assertThat(value).isEqualTo(expected)
    }

    @Test
    fun `Final Results`(): Unit = solution.run(file(input)).let { (v1, v2) ->
        println("Part 1: $v1")
        println("Part 2: ${v2.map { it.toString() }.orElse("TODO")}")
    }

}