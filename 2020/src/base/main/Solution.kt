import java.io.BufferedReader
import java.io.InputStream
import java.io.StringReader
import java.util.*
import java.util.stream.Collectors
import java.util.stream.IntStream
import java.util.stream.Stream

abstract class Solution<I: Any, O: Any> {

    protected val InputStream.lines: Stream<String> get() = this.bufferedReader().lines()
    protected val InputStream.string: String get() = this.bufferedReader().readText()
    protected val InputStream.scanner: Scanner get() = Scanner(this)

    protected val String.lines: Stream<String> get() = BufferedReader(StringReader(this)).lines()
    protected val String.spaced: Stream<String> get() = Scanner(this).tokens()
    protected fun String.csv(space: Boolean = false): Stream<String> =
        Scanner(this).useDelimiter(if (space) ", " else ",").tokens()
    protected val String.scanner: Scanner get() = Scanner(this)

    protected val Stream<String>.ints: IntStream get() = this.mapToInt { it.toInt() }
    protected fun <T> Stream<T>.toList(): List<T> = this.collect(Collectors.toList())
    protected fun <T> Stream<T>.toSet(): Set<T> = this.collect(Collectors.toSet())

    protected fun <T: Any> T?.optional(): Optional<T> = Optional.ofNullable(this)

    protected fun todo(): Nothing = throw TodoException()

    abstract fun read(input: InputStream): I

    abstract fun part1(input: I): O
    abstract fun part2(input: I): O

    fun run(inputStream: InputStream): Pair<O, Optional<O>> {
        val input = read(inputStream)
        val v1 = part1(input)
        return try {
            val v2 = part2(input)
            v1 to Optional.of(v2)
        } catch (e: TodoException) {
            v1 to Optional.empty()
        }
    }

}