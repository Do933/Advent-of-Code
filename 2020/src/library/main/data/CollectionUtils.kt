package data

infix fun <T> T.head(list: List<T>) = listOf(this) + list
infix fun <T> List<T>.tail(value: T) = this + listOf(value)

fun <T> Collection<T>.permutations(): Sequence<List<T>> = sequence {
    if (this@permutations.isEmpty()) {
        yield(emptyList<T>())
    } else {
        this@permutations.drop(1).permutations().forEach { perm ->
            (this@permutations.indices).forEach { i ->
                yield(perm.subList(0, i) + this@permutations.take(1) + perm.subList(i, perm.size))
            }
        }
    }
}