package data

import java.util.*

fun <T: Comparable<T>> emptyPQ() = AdaptablePriorityQueue<T>()
fun <T> emptyPQ(comparator: Comparator<T>) = AdaptablePriorityQueue(comparator = comparator)

fun <T: Comparable<T>> pqOf(vararg items: T) = AdaptablePriorityQueue(items = items.toList())

class AdaptablePriorityQueue<T>(items: Collection<T> = emptyList(), private val comparator: Comparator<T>? = null) : Queue<T> {

    private val items: MutableList<T> = ArrayList(items)

    override val size: Int get() = items.size

    init {
        if (size >= 2) (parent(size-1) downTo 0).forEach(::downHeap)
    }

    constructor(orig: AdaptablePriorityQueue<T>) : this(orig.items, orig.comparator)

    fun update(element: T) = bubble(items.indexOf(element))

    private fun swap(i: Int, j: Int) {
        val temp = items[i]
        items[i] = items[j]
        items[j] = temp
    }

    private fun parent(i: Int) = (i-1) / 2
    private fun parentValue(i: Int) = items[parent(i)]
    private fun hasParent(i: Int) = i > 0
    private fun left(i: Int) = 2*i + 1
    private fun leftValue(i: Int) = items[left(i)]
    private fun hasLeft(i: Int) = left(i) < size
    private fun right(i: Int) = 2*i + 2
    private fun rightValue(i: Int) = items[right(i)]
    private fun hasRight(i: Int) = right(i) < size
    private fun isLeaf(i: Int) = !hasLeft(i)
    private fun isRoot(i: Int) = !hasParent(i)

    private operator fun T.compareTo(other: T): Int =
            comparator?.compare(this, other) ?: (this as Comparable<T>).compareTo(other)

    private tailrec fun downHeap(i: Int): Unit =
            if (isLeaf(i) || (leftValue(i) >= items[i] && (!hasRight(i) || rightValue(i) >= items[i]))) Unit
            else downHeap((if (hasRight(i) && rightValue(i) < leftValue(i)) right(i) else left(i)).also { swap(it, i) })

    private tailrec fun upHeap(i: Int): Unit =
            if (isRoot(i) || (parentValue(i) <= items[i])) Unit
            else upHeap(parent(i).also { swap(it, i) })

    private fun bubble(i: Int): Unit = if (hasParent(i) && parentValue(i) > items[i]) upHeap(i) else downHeap(i)

    override fun add(element: T): Boolean = items.add(element).also {
        upHeap(size-1)
    }

    override fun addAll(elements: Collection<T>): Boolean = elements.map(::add).any()

    override fun clear() = items.clear()

    override fun iterator(): MutableIterator<T> = object : MutableIterator<T> {
        val copy = AdaptablePriorityQueue(this@AdaptablePriorityQueue)
        var last: T? = null
        override fun hasNext(): Boolean = copy.isNotEmpty()
        override fun next(): T = copy.remove().also { last = it }
        override fun remove(): Unit { last?.let { this@AdaptablePriorityQueue.remove(it) } }
    }

    override fun remove(): T =
            if (isEmpty()) throw NoSuchElementException()
            else swap(0, size-1).run { items.removeAt(size-1) }.also { downHeap(0) }

    override fun contains(element: T): Boolean = items.contains(element)

    override fun containsAll(elements: Collection<T>): Boolean = items.containsAll(items)

    override fun isEmpty(): Boolean = items.isEmpty()

    private fun removeIndex(i: Int): Boolean =
            swap(i, size-1).run { items.removeAt(size-1); true }.also { downHeap(i) }

    override fun remove(element: T): Boolean = items.indexOf(element).let {  if (it == -1) false else removeIndex(it) }

    override fun removeAll(elements: Collection<T>): Boolean = elements.map(::remove).any()

    override fun retainAll(elements: Collection<T>): Boolean = items.filter { it !in elements }.map(::remove).any()

    override fun offer(e: T): Boolean = add(e)

    override fun poll(): T? = if (isEmpty()) null else remove()

    override fun element(): T = if (isEmpty()) throw NoSuchElementException() else items[0]

    override fun peek(): T? = if (isEmpty()) null else items[0]

    override fun toString(): String = items.toString()

}