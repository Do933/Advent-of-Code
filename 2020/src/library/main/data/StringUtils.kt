package data

operator fun String.get(range: IntRange): String = this.substring(range)

fun String.splitAt(vararg indices: Int): List<String> = (0 head indices.toList() tail this.length).zipWithNext { a, b -> this[a until b] }