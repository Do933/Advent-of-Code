package data

import java.util.*

fun <T> List<List<T>>.matrixRows() = Matrix(this[0].size, this.size, this.flatten())
fun <T> List<List<T>>.matrixColumns() = this.matrixRows().transposed()

fun <T: Comparable<T>> Matrix<T>.min() = this.data.minOrNull()!!
fun <T: Comparable<T>> Matrix<T>.max() = this.data.maxOrNull()!!

@JvmName("timesInt")
operator fun Matrix<Int>.times(rhs: Matrix<Int>): Matrix<Int> = this.multiply(rhs, Int::times, Int::plus)
@JvmName("timesLong")
operator fun Matrix<Long>.times(rhs: Matrix<Long>): Matrix<Long> = this.multiply(rhs, Long::times, Long::plus)
@JvmName("timesDouble")
operator fun Matrix<Double>.times(rhs: Matrix<Double>): Matrix<Double> = this.multiply(rhs, Double::times, Double::plus)
fun <A, B, R> Matrix<A>.multiply(rhs: Matrix<B>, combine: (A, B) -> R, sum: (R, R) -> R): Matrix<R> {
    require(this.width == rhs.height)
    return Matrix(rhs.width, this.height) { x, y ->
        this.rows[y].zip(rhs.columns[x]).map { (l, r) -> combine(l, r) }.reduce(sum)
    }
}

@JvmName("expInt")
fun Matrix<Int>.pow(e: Int): Matrix<Int> = when {
    e == 1 -> this
    e % 2 == 1 -> this * this.pow(e-1)
    else -> pow(e/2).let { it * it }
}
@JvmName("expLong")
fun Matrix<Long>.pow(e: Int): Matrix<Long> = when {
    e == 1 -> this
    e % 2 == 1 -> this * this.pow(e-1)
    else -> pow(e/2).let { it * it }
}
@JvmName("expDouble")
fun Matrix<Double>.pow(e: Int): Matrix<Double> = when {
    e == 1 -> this
    e % 2 == 1 -> this * this.pow(e-1)
    else -> pow(e/2).let { it * it }
}

open class Matrix<T>(val width: Int, val height: Int, val data: List<T>) {

    val columns: List<List<T>> get() = (0 until width).map { x -> (0 until height).map { y -> this[x, y] } }
    val rows: List<List<T>> get() = (0 until height).map { y -> (0 until width).map { x -> this[x, y] } }

    init {
        require(width * height > 0)
        require(width * height == data.size)
    }

    constructor(width: Int, height: Int, init: (Int, Int) -> T) : this(width, height, List(width * height) { i ->
        init(i % width, i / width)
    })

    fun transposed(): Matrix<T> = columns.matrixRows()

    fun <S> zip(other: Matrix<S>): Matrix<Pair<T, S>> = Matrix(width, height, data.zip(other.data))

    inline fun <S> map(f: (T) -> S): Matrix<S> = Matrix(width, height, data.map(f))
    inline fun <S> mapIndexed(f: (Int, Int, T) -> S): Matrix<S> =
            Matrix(width, height, data.mapIndexed { i, value -> f(i % width, i / width, value) })

    inline fun forEach(f: (T) -> Unit): Unit = data.forEach(f)
    inline fun forEachIndexed(f: (Int, Int, T) -> Unit): Unit =
            data.forEachIndexed { i, value -> f(i % width, i / width, value) }

    inline fun <C : Comparable<C>> minBy(f: (T) -> C) = this.data.minByOrNull(f)!!
    inline fun <C : Comparable<C>> maxBy(f: (T) -> C) = this.data.maxByOrNull(f)!!

    operator fun get(x: Int, y: Int): T = data[x + y * width]
    operator fun iterator(): Iterator<T> = data.iterator()

    override fun equals(other: Any?): Boolean = other is Matrix<*> && this.width == other.width &&
            this.height == other.height && this.data == other.data
    override fun hashCode(): Int = Objects.hash(width, height, data)

    override fun toString(): String = this.map { it.toString() }.let { strings ->
        strings.data.map { it.length }.maxOrNull()!!.let { l ->
            strings.map { if (it.length < l) " ".repeat(l - it.length) + it else it }.rows
                    .joinToString("\n") { it.toString() }
        }
    }

}

open class MutableMatrix<T>(width: Int, height: Int, data: List<T>) : Matrix<T>(width, height, data.toMutableList()) {

    private val mutableData: MutableList<T> get() = super.data as MutableList<T>

    constructor(width: Int, height: Int, init: (Int, Int) -> T) : this(width, height, List(width * height) { i ->
        init(i % width, i / width)
    })

    operator fun set(x: Int, y: Int, value: T) {
        this.mutableData[x + y * width] = value
    }

}