package data

import kotlin.math.roundToInt

fun gcd(a: Int, b: Int): Int = if (b == 0) a else gcd(b, a % b)

fun fastFib(n: Int): Long = if (n == 0) 0 else (Matrix(2, 2, listOf(1L, 1L, 1L, 0L)).pow(n) * Matrix(1, 2, listOf(0L, 1L)))[0, 1]

fun digitSum(n: Int, base: Int = 10) = generateSequence(n) { it / base }.takeWhile { it > 0 }.map { it % base }.sum()

fun primeFactors(n: Int): Sequence<Int> = sequence {
    var nPrime = n
    var x = 2
    while (x*x <= nPrime) {
        while (nPrime % x == 0) {
            yield(x)
            nPrime /= x
        }
        x++
    }
    if (nPrime > 1) yield(nPrime)
}

fun primesTo(n: Int): Sequence<Int> = IntArray(n+1).let { sieve ->
    generateSequence(2) { it+1 }.takeWhile { x -> x <= n }.map { x ->
        if (sieve[x] == 0) {
            generateSequence(2*x) { u -> u + x }.takeWhile { u -> u <= n }.forEach { u -> sieve[u] = x }
        }
        x
    }.filter { x -> sieve[x] == 0 }
}

fun modPow(x: Int, n: Int, m: Int): Int = modPow(x.toLong(), n.toLong(), m.toLong()).toInt()
fun modPow(x: Long, n: Long, m: Long): Long {
    if (n == 0L) return 1L % m;
    var u: Long = modPow(x, n/2, m);
    u = (u*u) % m;
    if (n % 2L == 1L) u = (u * x) % m;
    return u;
}

fun totient(n: Int): Int = (primeFactors(n).map { p -> 1.0 - (1.0 / p) }.distinct().reduce(Double::times) * n).roundToInt()

fun modInv(x: Int, m: Int): Int = modPow(x, totient(m)-1, m)
fun modInvP(x: Int, p: Int): Int = modInvP(x.toLong(), p.toLong()).toInt()
fun modInvP(x: Long, p: Long): Long = modPow(x, p - 2, p)