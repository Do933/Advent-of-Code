package data

class UnionFind<in V>(items: List<V>) {

    private val indices: Map<V, Int>
    private val parent: MutableList<Int>
    private val size: MutableList<Int>

    init {
        this.indices = items.withIndex().associate { (i, v) -> v to i }
        this.parent = items.mapIndexed { i, _ -> i }.toMutableList()
        this.size = items.map { 1 }.toMutableList()
    }

    private fun find(x: Int): Int = if (parent[x] == x) x else parent[x].also { parent[x] = find(parent[x]) }

    fun union(x: V, y: V) = union(indices[x]!!, indices[y]!!)

    private fun union(x: Int, y: Int) = Pair(find(x), find(y)).let{ (sx, sy) -> when {
        size[sx] > size[sy] -> {
            parent[sy] = sx
            size[sx] = size[sx] + 1
            true
        }
        size[sx] < size[sy] -> {
            parent[sx] = sy
            size[sy] = size[sy] + 1
            true
        }
        sx != sy -> {
            size[sx] += size[sy]
            parent[sy] = sx
            true
        }
        else -> false
    } }

}