package data

fun <A, B> tupleOf(first: A, second: B) =
        Pair(first, second)
fun <A, B, C> tupleOf(first: A, second: B, third: C) =
        Triple(first, second, third)
fun <A, B, C, D> tupleOf(first: A, second: B, third: C, fourth: D) =
        FourTuple(first, second, third, fourth)
fun <A, B, C, D, E> tupleOf(first: A, second: B, third: C, fourth: D, fifth: E) =
        FiveTuple(first, second, third, fourth, fifth)
fun <A, B, C, D, E, F> tupleOf(first: A, second: B, third: C, fourth: D, fifth: E, sixth: F) =
        SixTuple(first, second, third, fourth, fifth, sixth)
fun <A, B, C, D, E, F, G> tupleOf(first: A, second: B, third: C, fourth: D, fifth: E, sixth: F, seventh: G) =
        SevenTuple(first, second, third, fourth, fifth, sixth, seventh)
fun <A, B, C, D, E, F, G, H> tupleOf(first: A, second: B, third: C, fourth: D, fifth: E, sixth: F, seventh: G, eighth: H) =
        EightTuple(first, second, third, fourth, fifth, sixth, seventh, eighth)

fun <T> FourTuple<T, T, T, T>.toList() = listOf(first, second, third, fourth)
fun <T> FiveTuple<T, T, T, T, T>.toList() = listOf(first, second, third, fourth, fifth)
fun <T> SixTuple<T, T, T, T, T, T>.toList() = listOf(first, second, third, fourth, fifth, sixth)
fun <T> SevenTuple<T, T, T, T, T, T, T>.toList() = listOf(first, second, third, fourth, fifth, sixth, seventh)
fun <T> EightTuple<T, T, T, T, T, T, T, T>.toList() = listOf(first, second, third, fourth, fifth, sixth, seventh, eighth)

data class FourTuple<A, B, C, D>(val first: A, val second: B, val third: C, val fourth: D) {
    override fun toString(): String = "($first, $second, $third, $fourth)"
}
data class FiveTuple<A, B, C, D, E>(val first: A, val second: B, val third: C, val fourth: D, val fifth: E) {
    override fun toString(): String = "($first, $second, $third, $fourth, $fifth)"
}
data class SixTuple<A, B, C, D, E, F>(val first: A, val second: B, val third: C, val fourth: D, val fifth: E, val sixth: F) {
    override fun toString(): String = "($first, $second, $third, $fourth, $fifth, $sixth)"
}
data class SevenTuple<A, B, C, D, E, F, G>(val first: A, val second: B, val third: C, val fourth: D, val fifth: E, val sixth: F, val seventh: G) {
    override fun toString(): String = "($first, $second, $third, $fourth, $fifth, $sixth, $seventh)"
}
data class EightTuple<A, B, C, D, E, F, G, H>(val first: A, val second: B, val third: C, val fourth: D, val fifth: E, val sixth: F, val seventh: G, val eighth: H) {
    override fun toString(): String = "($first, $second, $third, $fourth, $fifth, $sixth, $seventh, $eighth)"
}