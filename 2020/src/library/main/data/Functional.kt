package data

data class Fix<A, R>(val call: (Fix<A, R>) -> (A) -> R) {
    operator fun invoke(F: Fix<A, R>) = call(F)
}
private fun <A, R> fix(F: Fix<A, R>): (A) -> R = F(F)
fun <A, R> Y(f: ((A) -> R) -> (A) -> R): (A) -> R = fix(Fix { rec -> f { x -> fix(rec)(x) } })
