package data

import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

@JvmName("mstComparable")
fun <V: Comparable<V>, N: Node<N>, E: WeightedEdge<V, N>> Graph<N, E>.mst() = mst(Comparator.naturalOrder())

fun <V, N: Node<N>, E: WeightedEdge<V, N>> Graph<N, E>.mst(comparator: Comparator<V>): Set<E>? {
    val edges = this.edges.sortedWith(kotlin.Comparator { e1, e2 -> comparator.compare(e1.weight, e2.weight) })
    val nodes = this.nodes.sortedBy { it.id }
    val uf = UnionFind(nodes)

    val tree someFunc= HashSet<E>()

    for (e in edges) {
        if (uf.union(e.src, e.dest)) tree.add(e)
        if (tree.size == n-1) break
    }

    return if (tree.size != n-1) null else tree
}

@JvmName("shortestPathDouble")
fun <N: Node<N>, E: WeightedEdge<Double, N>> Graph<N, E>.shortestPath(s: N, t: N, combine: (Double, Double) -> Double = Double::plus) =
        shortestPath(s, t, 0.0, kotlin.Double.MAX_VALUE, combine)
@JvmName("shortestPathLong")
fun <N: Node<N>, E: WeightedEdge<Long, N>> Graph<N, E>.shortestPath(s: N, t: N, combine: (Long, Long) -> Long = Long::plus) =
        shortestPath(s, t, 0L, kotlin.Long.MAX_VALUE, combine)
@JvmName("shortestPathInt")
fun <N: Node<N>, E: WeightedEdge<Int, N>> Graph<N, E>.shortestPath(s: N, t: N, combine: (Int, Int) -> Int = Int::plus) =
        shortestPath(s, t, 0, kotlin.Int.MAX_VALUE, combine)
@JvmName("shortestPathComparable")
fun <V: Comparable<V>, N: Node<N>, E: WeightedEdge<V, N>> Graph<N, E>.shortestPath(s: N, t: N, zero: V, infty: V, combine: (V, V) -> V) =
        shortestPath(s, t, zero, infty, Comparator.naturalOrder<V>(), combine)

fun <V, N: Node<N>, E: WeightedEdge<V, N>> Graph<N, E>.shortestPath(s: N, t: N, zero: V, infty: V, comparator: Comparator<V>, combine: (V, V) -> V): Pair<V, List<N>>? {
    val visited = HashSet<N>()
    val distances = HashMap<N, V>()
    val pred = HashMap<N, N>()
    this.nodes.forEach { distances[it] = infty }
    distances[s] = zero

    val Q = AdaptablePriorityQueue(items = this.nodes, comparator = kotlin.Comparator { n1, n2 -> comparator.compare(distances[n1]!!, distances[n2]!!) })
    while (Q.isNotEmpty()) {
        val current = Q.remove()
        visited.add(current)

        if (distances[current]!! == infty) return null
        if (current == t) break

        current.neighbours.filter { it !in visited }.forEach { n ->
            val newDistance = combine(distances[current]!!, this.edge(current, n)!!.weight)
            if (comparator.compare(newDistance, distances[n]!!) < 0) {
                distances[n] = newDistance
                pred[n] = current
            }
        }
    }

    val path = LinkedList<N>()
    var current = t
    while (pred[current] != null) {
        path.addFirst(current)
        current = pred[current]!!
    }
    path.addFirst(s)

    return distances[t]!! to path
}

fun <N: Node<N>, E: Edge<N>> Graph<N, E>.bfs(): Sequence<N> = sequence {
    val unvisited = HashSet(this@bfs.nodes)
    while (unvisited.isNotEmpty()) {
        unvisited.iterator().next().bfs().forEach {
            unvisited.remove(it)
            yield(it)
        }
    }
}

fun <N: Node<N>> N.bfs(t: N? = null): Sequence<N> = sequence {
    val Q = LinkedList(listOf(this@bfs))
    val visited = HashSet<N>()
    while (Q.isNotEmpty()) {
        val current = Q.removeFirst()
        if (visited.contains(current)) continue
        if (t != null && current == t) return@sequence
        visited.add(current)
        yield(current)
        current.neighbours.filterNot(visited::contains).forEach(Q::addLast)
    }
}

fun <N: Node<N>, E: Edge<N>> Graph<N, E>.dfs(): Sequence<N> = sequence {
    val unvisited = HashSet(this@dfs.nodes)
    while (unvisited.isNotEmpty()) {
        unvisited.iterator().next().dfs().forEach {
            unvisited.remove(it)
            yield(it)
        }
    }
}

fun <N: Node<N>> N.dfs(t: N? = null): Sequence<N> = sequence {
    val S = LinkedList(listOf(this@dfs))
    val visited = HashSet<N>()
    while (S.isNotEmpty()) {
        val current = S.removeFirst()
        if (visited.contains(current)) continue
        if (t != null && current == t) return@sequence
        visited.add(current)
        yield(current)
        current.neighbours.filterNot(visited::contains).forEach(S::addFirst)
    }
}

fun <N: Node<N>, E: Edge<N>> buildGraph(n: Int, m: Int, configure: GraphBuilder<N, E>.() -> Unit) =
        GraphBuilder<N, E>(n, m).also(configure).build()

class GraphBuilder<N: Node<N>, E: Edge<N>>(val n: Int, val m: Int) {
    private var _directed: Boolean = false
    private var _valued: Boolean = false
    private var _weighted: Boolean = false
    private var _representation: GraphData = AdjacencyMap

    private val nodes: MutableMap<Int, N> = HashMap(n)
    private val edges: MutableMap<Int, E> = HashMap(m)
    private val edgeMap: MutableMap<Pair<Int, Int>, Int> = HashMap(m)

    val undirected: Nothing? get() { _directed = false; return null }
    val directed: Nothing? get() { _directed = true; return null }

    val valued: Nothing? get() { _valued = true; return null }
    val weighted: Nothing? get() { _weighted = true; return null }

    val adjacencyMatrix: Nothing? get() { _representation = AdjacencyMatrix; return null }
    val adjacencyList: Nothing? get() { _representation = AdjacencyList; return null }
    val adjacencyMap: Nothing? get() { _representation = AdjacencyMap; return null }
    val edgeList: Nothing? get() { _representation = EdgeList; return null }

    fun add(id: Int = nodes.size, configure: NodeBuilder<N>.() -> Unit = {}) {
        nodes[id] = NodeBuilder<N>(id, _valued).also(configure).build()
    }

    fun addAll(ids: Iterable<Int>): Unit = ids.forEach { add(it) }
    fun addAll(vararg ids: Int): Unit = ids.forEach { add(it) }

    fun connect(id: Int = edges.size, src: Int, dest: Int, configure: EdgeBuilder<N, E>.() -> Unit = {}) {
        val from = nodes[src]!!
        val to = nodes[dest]!!
        edges[id] = EdgeBuilder<N, E>(id, from, to, _weighted).also(configure).build()
        edgeMap[src to dest] = id
        (from.neighbours as MutableList).add(to)
        if (!_directed) {
            (to.neighbours as MutableList).add(from)
            edgeMap[dest to src] = id
        }
    }
    fun connect(nodes: Pair<Int, Int>, configure: EdgeBuilder<N, E>.() -> Unit = {}) =
            connect(src = nodes.first, dest = nodes.second, configure = configure)

    fun connectAll(connections: Iterable<Pair<Int, Int>>): Unit = connections.forEach { connect(src = it.first, dest = it.second) }
    fun connectAll(vararg connections: Pair<Int, Int>): Unit = connections.forEach { connect(src = it.first, dest = it.second) }

    fun build(): Graph<N, E> {
        require(n == nodes.size)
        require(m == edges.size)
        return _representation.createGraph(n, m, nodes, edges, edgeMap, _directed)
    }
}

class NodeBuilder<N: Node<N>>(val id: Int, val valued: Boolean) {
    var value: Any? = null

    fun build(): N {
        require((value != null) == valued)
        return (if (valued) ValuedNode(id, value!!, ArrayList()) else DefaultNode(id, ArrayList())) as N
    }
}

class EdgeBuilder<N: Node<N>, E: Edge<N>>(val id: Int, val src: N, val dest: N, val weighted: Boolean) {
    var weight: Any? = null

    fun build(): E {
        require((weight != null) == weighted)
        return (if (weighted) WeightedEdge(id, weight!!, src, dest) else DefaultEdge(id, src, dest)) as E
    }
}

interface Graph<N: Node<N>, E: Edge<N>> {
    val n: Int
    val m: Int
    val nodes: Collection<N>
    val edges: Collection<E>

    fun node(id: Int): N
    fun edge(id: Int): E
    fun edge(src: Int, dest: Int): E?
    fun edge(src: N, dest: N): E? = edge(src.id, dest.id)
}

private sealed class GraphData {
    abstract fun <N: Node<N>, E: Edge<N>> createGraph(n: Int, m: Int, nodes: Map<Int, N>, edges: Map<Int, E>,
                                                      edgeMap: Map<Pair<Int, Int>, Int>,
                                                      directed: Boolean) : Graph<N, E>
}
private object AdjacencyMatrix : GraphData() {
    override fun <N : Node<N>, E : Edge<N>> createGraph(n: Int, m: Int, nodes: Map<Int, N>, edges: Map<Int, E>,
                                                        edgeMap: Map<Pair<Int, Int>, Int>,
                                                        directed: Boolean): Graph<N, E> = object : Graph<N, E> {
        override val n: Int = n
        override val m: Int = m
        override val nodes: Collection<N> = nodes.values
        override val edges: Collection<E> = edges.values

        val ids: IntArray = nodes.values.map { it.id }.sorted().toIntArray()
        val indices: Map<Int, Int> = ids.withIndex().associate { i -> i.value to i.index }

        val matrix: Matrix<E?> = Matrix(n, n) { i, j ->
            edgeMap[ids[i] to ids[j]]?.let { edges[it]!! }
        }

        override fun node(id: Int): N = nodes[id]!!
        override fun edge(id: Int): E = edges[id]!!
        override fun edge(src: Int, dest: Int): E? = matrix[indices[src]!!, indices[dest]!!]
    }
}
private object AdjacencyList : GraphData() {
    override fun <N : Node<N>, E : Edge<N>> createGraph(n: Int, m: Int, nodes: Map<Int, N>, edges: Map<Int, E>,
                                                        edgeMap: Map<Pair<Int, Int>, Int>,
                                                        directed: Boolean): Graph<N, E> = object : Graph<N, E> {
        override val n: Int = n
        override val m: Int = m
        override val nodes: Collection<N> = nodes.values
        override val edges: Collection<E> = edges.values

        override fun node(id: Int): N = nodes[id]!!
        override fun edge(id: Int): E = edges[id]!!
        override fun edge(src: Int, dest: Int): E? =
                node(src).neighbours.find { it.id == dest }?.let { edges[edgeMap[src to dest]!!]!! }
    }
}
private object AdjacencyMap : GraphData() {
    override fun <N : Node<N>, E : Edge<N>> createGraph(n: Int, m: Int, nodes: Map<Int, N>, edges: Map<Int, E>,
                                                        edgeMap: Map<Pair<Int, Int>, Int>,
                                                        directed: Boolean): Graph<N, E> = object : Graph<N, E> {
        override val n: Int = n
        override val m: Int = m
        override val nodes: Collection<N> = nodes.values
        override val edges: Collection<E> = edges.values

        override fun node(id: Int): N = nodes[id]!!
        override fun edge(id: Int): E = edges[id]!!
        override fun edge(src: Int, dest: Int): E? = edgeMap[src to dest]?.let { edges[it]!! }
    }
}
private object EdgeList : GraphData() {
    override fun <N : Node<N>, E : Edge<N>> createGraph(n: Int, m: Int, nodes: Map<Int, N>, edges: Map<Int, E>,
                                                        edgeMap: Map<Pair<Int, Int>, Int>,
                                                        directed: Boolean): Graph<N, E> = object : Graph<N, E> {
        override val n: Int = n
        override val m: Int = m
        override val nodes: Collection<N> = nodes.values
        override val edges: Collection<E> = edges.values

        override fun node(id: Int): N = nodes[id]!!
        override fun edge(id: Int): E = edges[id]!!
        override fun edge(src: Int, dest: Int): E? = this.edges
                .find { it.src.id == src && it.dest.id == dest || (!directed && it.src.id == dest && it.dest.id == src) }
    }
}

sealed class Node<out N: Node<N>>(val id: Int, val neighbours: List<N>) {
    override fun equals(other: Any?): Boolean = other is Node<*> && this.id == other.id
    override fun hashCode(): Int = id
}
class DefaultNode(id: Int, neighbours: List<DefaultNode>) : Node<DefaultNode>(id, neighbours) {
    override fun toString(): String = "Node(id=$id)"
}
class ValuedNode<out T>(id: Int, val value: T, neighbours: List<ValuedNode<T>>) : Node<ValuedNode<T>>(id, neighbours) {
    override fun toString(): String = "Node(id=$id, value=$value)"
}

sealed class Edge<out N: Node<N>>(val id: Int, val src: N, val dest: N) {
    override fun equals(other: Any?): Boolean = other is Edge<*> && this.id == other.id
    override fun hashCode(): Int = id
}
class DefaultEdge<out N: Node<N>>(id: Int, src: N, dest: N) : Edge<N>(id, src, dest) {
    override fun toString(): String = "Edge(id=$id, src=$src, dest=$dest)"
}
class WeightedEdge<out T, out N: Node<N>>(id: Int, val weight: T, src: N, dest: N) : Edge<N>(id, src, dest) {
    override fun toString(): String = "Edge(id=$id, weight=$weight, src=$src, dest=$dest)"
}