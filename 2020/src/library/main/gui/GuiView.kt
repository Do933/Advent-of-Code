package gui

import javafx.application.Application
import javafx.scene.Scene
import javafx.stage.Stage

class GuiView: Application() {

    companion object {
        fun launch(title: String, scene: () -> Scene) {
            GuiView.title = title
            GuiView.scene = scene
            launch(GuiView::class.java)
        }

        private lateinit var title: String
        private lateinit var scene: () -> Scene
    }

    override fun start(window: Stage) {
        window.title = title
        window.scene = scene()

        window.show()
    }

}