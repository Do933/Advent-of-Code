package gui

import data.Matrix
import data.max
import data.min
import javafx.animation.KeyFrame
import javafx.animation.Timeline
import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.scene.Scene
import javafx.scene.canvas.Canvas
import javafx.scene.canvas.GraphicsContext
import javafx.scene.layout.VBox
import javafx.scene.paint.Color
import javafx.util.Duration

class GridView(val nx: Int, val ny: Int, width: Int = 800, height: Int = 800, val draw: GridView.(GraphicsContext) -> Unit) :
        Scene(VBox(), width.toDouble(), height.toDouble()) {

    private val root: VBox = super.getRoot() as VBox
    private val canvas = Canvas(width.toDouble(), height.toDouble())

    init {
        root.children.add(canvas)

        val timer = Timeline(KeyFrame(Duration.millis(50.0), EventHandler<ActionEvent> { redraw() }))
        timer.cycleCount = Timeline.INDEFINITE
        timer.play()
    }

    fun redraw(): Unit = canvas.graphicsContext2D.let { g ->
        g.clearRect(0.0, 0.0, width, height)
        g.stroke = Color.GRAY
        for (i in 1 until nx) {
            val x = i * width / nx
            g.strokeLine(x, 0.0, x, height)
        }
        for (i in 1 until nx) {
            val y = i * height / ny
            g.strokeLine(0.0, y, width, y)
        }

        draw(g)
    }

    @JvmName("visualiseIntMatrix")
    fun visualiseMatrix(g: GraphicsContext, matrix: Matrix<Int>) =
            visualiseMatrix(g, matrix.map { it.toDouble() })

    @JvmName("visualiseDoubleMatrix")
    fun visualiseMatrix(g: GraphicsContext, matrix: Matrix<Double>) {
        require(nx == matrix.width && ny == matrix.height)

        val dx = width / nx
        val dy = height / ny
        val min = matrix.min()
        val max = matrix.max()
        matrix.forEachIndexed { i, j, value ->
            val x = i * dx
            val y = j * dy
            g.fill = Color.gray((value - min) / (max - min))
            g.fillRect(x, y, dx, dy)
        }
    }

}