{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
module Paths_haskell2021 (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/home/ruben/Dev/IntelliJ/Advent_of_Code/haskell2021/.stack-work/install/x86_64-linux-tinfo6/66ca6d043ebde02d0be6867f8ae02084fc702063212d1b381986c3c3db1f2316/8.10.7/bin"
libdir     = "/home/ruben/Dev/IntelliJ/Advent_of_Code/haskell2021/.stack-work/install/x86_64-linux-tinfo6/66ca6d043ebde02d0be6867f8ae02084fc702063212d1b381986c3c3db1f2316/8.10.7/lib/x86_64-linux-ghc-8.10.7/haskell2021-0.1.0.0-4YoDH1EuNh3IeEJjZ3jHB8"
dynlibdir  = "/home/ruben/Dev/IntelliJ/Advent_of_Code/haskell2021/.stack-work/install/x86_64-linux-tinfo6/66ca6d043ebde02d0be6867f8ae02084fc702063212d1b381986c3c3db1f2316/8.10.7/lib/x86_64-linux-ghc-8.10.7"
datadir    = "/home/ruben/Dev/IntelliJ/Advent_of_Code/haskell2021/.stack-work/install/x86_64-linux-tinfo6/66ca6d043ebde02d0be6867f8ae02084fc702063212d1b381986c3c3db1f2316/8.10.7/share/x86_64-linux-ghc-8.10.7/haskell2021-0.1.0.0"
libexecdir = "/home/ruben/Dev/IntelliJ/Advent_of_Code/haskell2021/.stack-work/install/x86_64-linux-tinfo6/66ca6d043ebde02d0be6867f8ae02084fc702063212d1b381986c3c3db1f2316/8.10.7/libexec/x86_64-linux-ghc-8.10.7/haskell2021-0.1.0.0"
sysconfdir = "/home/ruben/Dev/IntelliJ/Advent_of_Code/haskell2021/.stack-work/install/x86_64-linux-tinfo6/66ca6d043ebde02d0be6867f8ae02084fc702063212d1b381986c3c3db1f2316/8.10.7/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "haskell2021_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "haskell2021_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "haskell2021_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "haskell2021_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "haskell2021_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "haskell2021_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
