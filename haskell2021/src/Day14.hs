module Day14 (day14, day14_oneline) where

import Data.Char
import Data.List
import Data.Maybe
import Data.Map as M (Map, fromList, toList, lookup, keys, insert)
import Util

import Debug.Trace

day14 :: String -> (Int, Int)
day14 input = (part1 input, part2 input)

part1 :: String -> Int
part1 input = let (poly, rules) = (head (lines input), M.fromList ((\s -> (take 2 s, last s)) <$> drop 2 (lines input))) in
                score (runSteps poly rules 10)
  where score s = let cs = [length (filter (==c) s) | c <- nub s] in maximum cs - minimum cs
        runSteps s rs n = foldr (\_ s' -> runStep s' rs) s [1..n]
        runStep [] rs = []
        runStep [c] rs = [c]
        runStep s rs = applyFragment (take 2 s) rs ++ runStep (tail s) rs
        applyFragment s rs = maybe [head s] (\x -> [head s, x]) (M.lookup s rs)

part2 :: String -> Int
part2 input = let (poly, rules) = (head (lines input), M.fromList ((\s -> (take 2 s, last s)) <$> drop 2 (lines input)))
                  pairs = let ps = (\(a, b) -> [a, b]) <$> zip poly (tail poly) in M.fromList [(p, length (filter (==p) ps)) | p <- ps]
                  counts = M.fromList [(c, length (filter (==c) poly)) | c <- nub poly] in
                score (snd (runSteps pairs counts rules 40))
  where score cs = let xs = snd <$> M.toList cs in maximum xs - minimum xs
        runSteps pcs cs rs n = foldr (\_ (pcs', cs') -> runStep pcs' cs' rs (M.keys pcs') pcs') (pcs, cs) [1..n]
        runStep pcs cs rs [] newPcs = (newPcs, cs)
        runStep pcs cs rs (p:ps) newPcs = let i = fromJust (M.lookup p rs)
                                              c = fromJust (M.lookup p pcs)
                                              pcs' = M.insert [head p, i] (fromMaybe 0 (M.lookup [head p, i] newPcs) + c) newPcs
                                              pcs'' = M.insert [i, last p] (fromMaybe 0 (M.lookup [i, last p] pcs') + c) pcs'
                                              pcs''' = M.insert p (fromMaybe c (M.lookup p pcs'') - c) pcs''
                                              cs' = M.insert i (fromMaybe 0 (M.lookup i cs) + c) cs in
                                            runStep pcs cs' rs ps pcs'''

day14_oneline :: String -> (Int, Int)
day14_oneline input = let (poly, rs) = (head (lines input), M.fromList ((\s -> (take 2 s, last s)) <$> drop 2 (lines input))) in let (pcs, cs) = (let ps = (\(a, b) -> [a, b]) <$> zip poly (tail poly) in M.fromList [(p, length (filter (==p) ps)) | p <- ps], M.fromList [(c, length (filter (==c) poly)) | c <- nub poly]) in let score n = let xs = snd <$> M.toList (snd (foldr (\_ (pcs, cs) -> foldr (\p (newPcs, cs) -> let (i, c) = (fromJust (M.lookup p rs), fromJust (M.lookup p pcs)) in let (pcs', cs') = (M.insert [head p, i] (fromMaybe 0 (M.lookup [head p, i] newPcs) + c) newPcs, M.insert i (fromMaybe 0 (M.lookup i cs) + c) cs) in let pcs'' = M.insert [i, last p] (fromMaybe 0 (M.lookup [i, last p] pcs') + c) pcs' in let pcs''' = M.insert p (fromMaybe c (M.lookup p pcs'') - c) pcs'' in (pcs''', cs')) (pcs, cs) (M.keys pcs)) (pcs, cs) [1..n])) in maximum xs - minimum xs in (score 10, score 40)
