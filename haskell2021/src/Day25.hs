module Day25 (day25) where

import Control.Applicative
import Control.Monad
import Control.Monad.State.Lazy
import Data.Char
import Data.Either
import Data.List as L
import Data.Maybe
import Data.Set as S
import Data.Map as M
import Text.Regex
import Util

import Debug.Trace

day25 :: String -> Int
day25 = part1

type Point = (Int, Int)
type Size = (Int, Int)
data Direction = East | South deriving (Show, Eq)
type Cucumber = (Point, Direction)
type Cucumbers = M.Map Point Direction

nextPos :: Cucumber -> Cucumbers -> Size -> Cucumber
nextPos ((x, y), dir) cucumbers s =
  let (x', y') = (newX x dir s, newY y dir s) in
    if M.member (x', y') cucumbers then ((x, y), dir)
    else ((x', y'), dir)
  where newX x dir (w, _) = if dir == East then (x + 1) `mod` w else x
        newY y dir (_, h) = if dir == South then (y + 1) `mod` h else y

next :: Size -> State Cucumbers ()
next s = do cucumbers <- get

            let east = L.filter ((==East).snd) (M.toList cucumbers)
            let noEast = M.difference cucumbers (M.fromList east)
            put (M.union noEast (M.fromList ((\c -> nextPos c cucumbers s) <$> east)))

            cucumbers' <- get
            let south = L.filter ((==South).snd) (M.toList cucumbers')
            let noSouth = M.difference cucumbers' (M.fromList south)
            put (M.union noSouth (M.fromList ((\c -> nextPos c cucumbers' s) <$> south)))

            return ()

findSame :: Size -> State Cucumbers Int
findSame s = do before <- get
                next s
                after <- get
                if before == after then return 1
                else put after >>= \() -> (1+) <$> findSame s

showCucumbers :: Cucumbers -> Size -> String
showCucumbers cucumbers (w, h) = intercalate "\n" [intercalate "" [maybe "." (\d -> if d == East then ">" else "v") (cucumbers M.!? (x, y)) | x <- [0..w-1]] | y <- [0..h-1]]

part1 :: String -> Int
part1 input = let s@(w, h) = (length (head (lines input)), length (lines input))
                  cucumbers = M.fromList [if (lines input !! y) !! x == '>' then ((x, y), East) else ((x, y), South) | y <- [0..h-1], x <- [0..w-1], (lines input !! y) !! x /= '.'] in
                let (n, res) = runState (findSame s) cucumbers in n

part2 :: String -> Int
part2 input = 0