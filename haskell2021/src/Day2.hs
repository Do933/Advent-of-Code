module Day2 (day2, day2_oneline, day2_maths) where

day2 :: String -> (Int, Int)
day2 input = (part1 input, part2 input)

part1 :: String -> Int
part1 input = let (x, y) = posFrom (0, 0) ((\l -> let ins = words l in (head ins, read (ins !! 1))) <$> lines input) in x * y
                where posFrom (x, y) [] = (x, y)
                      posFrom (x, y) (("forward", amt) : is) = posFrom (x + amt, y) is
                      posFrom (x, y) (("down", amt) : is) = posFrom (x, y + amt) is
                      posFrom (x, y) (("up", amt) : is) = posFrom (x, y - amt) is

part2 :: String -> Int
part2 input = let (x, y, _) = posFrom (0, 0, 0) ((\l -> let ins = words l in (head ins, read (ins !! 1))) <$> lines input) in x * y
                where posFrom (x, y, a) [] = (x, y, a)
                      posFrom (x, y, a) (("forward", amt) : is) = posFrom (x + amt, y + amt * a, a) is
                      posFrom (x, y, a) (("down", amt) : is) = posFrom (x, y, a + amt) is
                      posFrom (x, y, a) (("up", amt) : is) = posFrom (x, y, a - amt) is

day2_oneline :: String -> (Int, Int)
day2_oneline input = let (x, y1, y2, _) = foldl (\(x, y1, y2, a) (i, amt) -> if i == "forward" then (x + amt, y1, y2 + amt * a, a) else if i == "down" then (x, y1 + amt, y2, a + amt) else (x, y1 - amt, y2, a - amt)) (0, 0, 0, 0) ((\l -> let ins = words l in (head ins, read (ins !! 1))) <$> lines input) in (x * y1, x * y2)

day2_maths :: String -> (Int, Int)
day2_maths input = let (dX, dY) = unzip ((\l -> let ((i:_):a:_) = words l in if i == 'f' then (read a, 0) else if i == 'd' then (0, read a) else (0, -(read a))) <$> lines input) in (sum dX * sum dY, sum dX * sum ((\i -> (dX!!i) * sum (take i dY)) <$> [1..(length (lines input) - 1)]))