module Heap where

import Data.Maybe
import Data.List

data Heap k v = Heap [(k, v)] deriving (Show, Eq)

empty :: Ord v => Heap k v
empty = Heap []

insert :: Ord v => k -> v -> Heap k v -> Heap k v
insert key value (Heap arr) = upHeap (Heap (arr ++ [(key, value)])) (length arr)

removeMin :: Ord v => Heap k v -> ((k, v), Heap k v)
removeMin (Heap arr) = let (Heap newArr) = swap (Heap arr) 0 (length arr - 1) in (last newArr, downHeap (Heap (take (length arr - 1) newArr)) 0)

update :: (Eq k, Ord v) => k -> v -> Heap k v -> Heap k v
update key newVal (Heap arr) = let i = fromJust (findIndex ((==key).fst) arr)
                                   oldVal = snd (arr !! i)
                                   newArr = take i arr ++ [(key, newVal)] ++ drop (i+1) arr in
                                if newVal > oldVal then downHeap (Heap newArr) i
                                else upHeap (Heap newArr) i

upHeap :: Ord v => Heap k v -> Int -> Heap k v
upHeap (Heap arr) i
  | i == 0    = Heap arr
  | otherwise = let (j, (k, v)) = ((i-1) `div` 2, arr !! ((i-1) `div` 2)) in
                  if v > snd (arr !! i) then upHeap (swap (Heap arr) i j) j else Heap arr

downHeap :: Ord v => Heap k v -> Int -> Heap k v
downHeap (Heap arr) i
  | 2 * i + 1 >= length arr     = Heap arr
  | 2 * i + 1 == length arr - 1 = if snd (last arr) > snd (arr !! i) then swap (Heap arr) (2*i+1) i else Heap arr
  | otherwise                   = let (j, (_, v)) = if snd (arr !! (2*i+1)) < snd (arr !! (2*i+2)) then (2*i+1, arr !! (2*i+1)) else (2*i+2, arr !! (2*i+2)) in
                                    if v < snd (arr !! i) then downHeap (swap (Heap arr) i j) j else Heap arr

swap :: Ord v => Heap k v -> Int -> Int -> Heap k v
swap (Heap arr) i j = let i' = min i j
                          j' = max i j in Heap (take i' arr ++ [arr !! j'] ++ take (j' - i' - 1) (drop (i'+1) arr) ++ [arr !! i'] ++ drop (j'+1) arr)
