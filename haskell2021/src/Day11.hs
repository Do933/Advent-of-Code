module Day11 (day11, day11_oneline) where

import Data.Char
import Data.List
import Data.Maybe
import Util

import Debug.Trace

day11 :: String -> (Int, Int)
day11 input = (part1 input, part2 input)

part1 :: String -> Int
part1 input = let grid = (digitToInt <$>) <$> lines input in
                snd (foldr (\_ (g, c) -> step g c) (grid, 0) [0..99])
  where neighbours x y = [(x+i, y+j) | i <- [-1..1], j <- [-1..1], i+x >= 0 && i+x < 10 && j+y >= 0 && j+y < 10 && (j /= 0 || i /= 0)]
        flash grid [] v c = (grid, c)
        flash grid ((x, y):ps) v c
          | (x, y) `elem` v        = flash grid ps v c
          | (grid !! y) !! x == 10 = flash (update grid x y 0) (neighbours x y ++ ps) ((x, y):v) (c+1)
          | (grid !! y) !! x == 9  = flash (increment grid x y) ((x, y):ps) v c
          | otherwise              = flash (increment grid x y) ps v c
        update grid x y val = [[if i == x && j == y then val else (grid !! j) !! i | i <- [0..9]] | j <- [0..9]]
        increment grid x y = update grid x y (((grid !! y) !! x) + 1)
        step grid c = let incGrid = [[((grid !! j) !! i) + 1 | i <- [0..9]] | j <- [0..9]] in
                        flash incGrid (filter (\(x, y) -> (grid !! y) !! x == 9) [(i, j) | i <- [0..9], j <- [0..9]]) [] c

part2 :: String -> Int
part2 input = let grid = (digitToInt <$>) <$> lines input in
                steps grid 0
  where neighbours x y = [(x+i, y+j) | i <- [-1..1], j <- [-1..1], i+x >= 0 && i+x < 10 && j+y >= 0 && j+y < 10 && (j /= 0 || i /= 0)]
        flash grid [] v c = (grid, c)
        flash grid ((x, y):ps) v c
          | (x, y) `elem` v        = flash grid ps v c
          | (grid !! y) !! x == 10 = flash (update grid x y 0) (neighbours x y ++ ps) ((x, y):v) (c+1)
          | (grid !! y) !! x == 9  = flash (increment grid x y) ((x, y):ps) v c
          | otherwise              = flash (increment grid x y) ps v c
        update grid x y val = [[if i == x && j == y then val else (grid !! j) !! i | i <- [0..9]] | j <- [0..9]]
        increment grid x y = update grid x y (((grid !! y) !! x) + 1)
        step grid = let incGrid = [[((grid !! j) !! i) + 1 | i <- [0..9]] | j <- [0..9]] in
                      flash incGrid (filter (\(x, y) -> (grid !! y) !! x == 9) [(i, j) | i <- [0..9], j <- [0..9]]) []
        steps grid n = if sum (sum <$> grid) == 0 then n else steps (fst (step grid 0)) (n+1)

day11_oneline :: String -> (Int, Int)
day11_oneline input = let run n = foldr (\_ (g, c) -> let flash g ps' v c = if null ps' then (g, c) else let ((x, y):ps) = ps' in if (x, y) `elem` v then flash g ps v c else if (g !! y) !! x == 10 then flash [[if i == x && j == y then 0 else (g !! j) !! i | i <- [0..9]] | j <- [0..9]] ([(x+i, y+j) | i <- [-1..1], j <- [-1..1], i+x >= 0 && i+x < 10 && j+y >= 0 && j+y < 10 && (j /= 0 || i /= 0)] ++ ps) ((x, y):v) (c+1) else flash [[((g !! j) !! i) + if i == x && j == y then 1 else 0 | i <- [0..9]] | j <- [0..9]] (if (g !! y) !! x == 9 then (x, y):ps else ps) v c in flash [[((g !! j) !! i) + 1 | i <- [0..9]] | j <- [0..9]] (filter (\(x, y) -> (g !! y) !! x == 9) [(i, j) | i <- [0..9], j <- [0..9]]) [] c) ((digitToInt <$>) <$> lines input, 0) [1..n] in (snd (run 100), length (takeWhile ((>0).sum.(sum <$>)) (fst.run <$> [0..])))
