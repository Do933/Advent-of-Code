module Day23 (day23) where

import Control.Applicative
import Control.Monad
import Control.Monad.State.Lazy
import Data.Char
import Data.Either
import Data.List as L
import Data.Maybe
import Data.Set as S
import Data.Map as M
import Text.Regex
import Util

import Debug.Trace

day23 :: String -> (Int, Int)
day23 input = (part1 input, part2 input)

type Pos = (Int, Int)
type Pod = Char
type Amphipods = M.Map Pos Pod

energy :: Pod -> Pos -> Pos -> Int
energy pod (x1, y1) (x2, y2)
  | y1 == 0 || y2 == 0 || x1 == x2 = (abs (x1-x2) + abs (y1-y2)) * case pod of
                                                                    'A' -> 1
                                                                    'B' -> 10
                                                                    'C' -> 100
                                                                    'D' -> 1000
  | otherwise = energy pod (x1, y1) (x2, 0) + energy pod (x2, 0) (x2, y2)

showPods :: Amphipods -> String
showPods pods = intercalate "\n" [intercalate "" [maybe (if y == 0 || x == 2 || x == 4 || x == 6 || x == 8 then "." else "#") (:[]) (pods M.!? (x, y)) | x <- [0..10]] | y <- [0..4]] ++ "\n"

heuristic :: Amphipods -> Amphipods -> Int
heuristic target pods = sum (cost <$> zip (positions target) (positions pods))
  where positions ps = [(c, tp) | (tp, c) <- M.toList ps, c == 'A'] ++
                       [(c, tp) | (tp, c) <- M.toList ps, c == 'B'] ++
                       [(c, tp) | (tp, c) <- M.toList ps, c == 'C'] ++
                       [(c, tp) | (tp, c) <- M.toList ps, c == 'D']
        cost ((c, p1), (_, p2)) = energy c p1 p2

data Dijkstra = Dijkstra { distances :: M.Map Amphipods Int, visited :: S.Set Amphipods, queue :: S.Set Amphipods, heuristics :: M.Map Amphipods Int } deriving Show
(#!) :: M.Map Amphipods Int -> Amphipods -> Int
ds #! a = fromMaybe ((maxBound :: Int) `div` 10) (ds M.!? a)

dijkstra :: Amphipods -> State Dijkstra ()
dijkstra t = do (Dijkstra ds vs q hs) <- get
                let (currDis, curr) = findNext t ds q hs

                if (if S.size vs `mod` 40 == 0 then trace (show (heuristic t curr + currDis)) curr else curr) == t then return ()

                else
                  do let hallway = M.filterWithKey (\(_, y) _ -> y == 0) curr
                     let rooms = M.filterWithKey (\p@(x, y) c -> y > 0 && M.notMember (x, y-1) curr && M.notMember (x, y-2) curr && M.notMember (x, y-3) curr) curr

                     let toRooms = [(M.insert p2 c (M.delete p curr), energy c p p2) | (p, c) <- M.toList hallway, p2 <- fromHallway p t hallway curr]
                     let neighs = if L.null toRooms then [(M.insert p2 c (M.delete p curr), energy c p p2) | (p, c) <- M.toList rooms, p2 <- fromRoom p rooms hallway] else toRooms

                     let neighDists = (\(n, e) -> (n, currDis + e)) <$> neighs
                     let ds' = M.unionWith min ds (M.fromList neighDists)
                     let hs' = M.union hs (M.fromList ((\(n, _) -> (n, heuristic t n)) <$> neighs))

                     put (Dijkstra ds' (S.insert curr vs) (S.difference (S.union (S.fromList (fst <$> neighs)) (S.delete curr q)) vs) hs')

                     dijkstra t
  where fromHallway (x, y) t hallway pods =
          let hallway' = M.delete (x, y) hallway
              c = hallway M.! (x, y)
              targets = fst <$> M.toList (M.filter (==c) t) in
            if all (\occ -> isNothing occ || occ == Just c) ((pods M.!?) <$> targets) then
              let ts = L.filter (\(x2, y2) -> M.notMember (x2, y2) pods && L.all (\(x', _) -> compare x x' == compare x2 x') (M.keys hallway')) targets in
                [maximumBy (\a b -> compare (snd a) (snd b)) ts | not (L.null ts)]
            else []
        fromRoom (x, y) rooms hallway =
          L.filter (\(x2, _) -> L.all (\(x', _) -> compare x x' == compare x2 x') (M.keys hallway)) [(x, 0) | x <- [0, 1, 3, 5, 7, 9, 10]]
        findNext t ds q hs = let (_, nd, n) = S.findMin (S.map (\v -> let h = fromMaybe (heuristic v t) (hs M.!? v); d = ds #! v in (d+h, d, v)) q) in (nd, n)


part1 :: String -> Int
part1 input = let pods = M.fromList (L.filter (isAlpha.snd) [((x, y), (lines input !! (y+1)) !! (x+1)) | y <- [0..2], x <- [0..10], x+1 < length (lines input !! (y+1))])
                  target = M.fromList [((2, 1), 'A'), ((2, 2), 'A'), ((4, 1), 'B'), ((4, 2), 'B'), ((6, 1), 'C'), ((6, 2), 'C'), ((8, 1), 'D'), ((8, 2), 'D')] in
                distances (execState (dijkstra target) (Dijkstra (M.singleton pods 0) S.empty (S.singleton pods) M.empty)) M.! target

part2 :: String -> Int
part2 input = let pods = M.union
                          (M.fromList (L.filter (isAlpha.snd) [((x, if y == 2 then 4 else y), (lines input !! (y+1)) !! (x+1)) | y <- [0..2], x <- [0..10], x+1 < length (lines input !! (y+1))]))
                          (M.fromList [((2, 2), 'D'), ((2, 3), 'D'), ((4, 2), 'C'), ((4, 3), 'B'), ((6, 2), 'B'), ((6, 3), 'A'), ((8, 2), 'A'), ((8, 3), 'C')])
                  target = M.fromList [((2*i, y), "ABCD" !! (i-1)) | i <- [1..4], y <- [1..4]] in
                error $ show $ distances (execState (dijkstra target) (Dijkstra (M.singleton pods 0) S.empty (S.singleton pods) M.empty)) M.! target
