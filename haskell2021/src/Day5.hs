module Day5 (day5, day5_oneline) where

import Data.List
import Util

day5 :: String -> (Int, Int)
day5 input = (part1 input, part2 input)

part1 :: String -> Int
part1 input = let ls = parseLine <$> lines input in length $ filter ((>=2).snd) $ getCounts $ filter (not . uncurry isDiagonal) ls >>= uncurry points
  where parseLine :: String -> ((Int, Int), (Int, Int))
        parseLine l = let (p1:p2:_) = split '-' l
                          (x1:y1:_) = read <$> split ',' (init p1)
                          (x2:y2:_) = read <$> split ',' (drop 2 p2) in ((x1, y1), (x2, y2))
        isDiagonal (x1, y1) (x2, y2) = x1 /= x2 && y1 /= y2
        points (x1, y1) (x2, y2) = let xs = if x1 == x2 then take (abs (y1 - y2) + 1) (repeat x1) else if x1 < x2 then [x1..x2] else [x2..x1]
                                       ys = if y1 == y2 then take (abs (x1 - x2) + 1) (repeat y1) else if y1 < y2 then [y1..y2] else [y2..y1] in zip xs ys
        getCounts xs = [(x, length (filter (==x) xs)) | x <- nub xs]

part2 :: String -> Int
part2 input = let ls = parseLine <$> lines input in length $ filter (>=2) $ getCounts $ ls >>= uncurry points
  where parseLine :: String -> ((Int, Int), (Int, Int))
        parseLine l = let (p1:p2:_) = split '-' l
                          (x1:y1:_) = read <$> split ',' (init p1)
                          (x2:y2:_) = read <$> split ',' (drop 2 p2) in ((x1, y1), (x2, y2))
        isDiagonal (x1, y1) (x2, y2) = x1 /= x2 && y1 /= y2
        points (x1, y1) (x2, y2) = let xs = if x1 == x2 then take (abs (y1 - y2) + 1) (repeat x1) else if x1 < x2 then [x1..x2] else reverse [x2..x1]
                                       ys = if y1 == y2 then take (abs (x1 - x2) + 1) (repeat y1) else if y1 < y2 then [y1..y2] else reverse [y2..y1] in zip xs ys
        getCounts xs = [length (filter (==x) xs) | x <- nub xs]

day5_oneline :: String -> (Int, Int)
day5_oneline input = let ls = ((\(p1:(_:_:p2):_) -> map read . split ',' <$> [init p1, p2]) . split '-' <$> lines input) in let [r1, r2] = map ((\ps -> length (filter (>1) [length (filter (==p) ps) | p <- nub ps])) . (>>= (\[p1, p2] -> let ps [x1, y1] [x2, y2] = if x1 == x2 then take (abs (y1 - y2) + 1) (repeat x1) else if x1 < x2 then [x1..x2] else reverse [x2..x1] in zip (ps p1 p2) (ps (reverse p1) (reverse p2))))) [filter (\[[x1, y1], [x2, y2]] -> x1 == x2 || y1 == y2) ls, ls] in (r1, r2)