module Day9 (day9, day9_oneline) where

import Data.Char
import Data.List
import Data.Maybe
import Util

day9 :: String -> (Int, Int)
day9 input = (part1 input, part2 input)

part1 :: String -> Int
part1 input = let grid = (digitToInt <$>) <$> lines input in
                sum [1 + (grid !! y) !! x | x <- [0..length (head grid)-1], y <- [0..length grid-1], lowPoint grid x y]
  where getSafe g x y = if x < 0 || x >= length (head g) || y < 0 || y >= length g then Nothing else Just ((g !! y) !! x)
        lowPoint g x y = all ((>((g !! y) !! x)).fromJust) (filter isJust (uncurry (getSafe g) <$> [(x-1, y), (x+1, y), (x, y-1), (x, y+1)]))

part2 :: String -> Int
part2 input = let grid = (digitToInt <$>) <$> lines input in
                product (take 3 (sortOn negate [length (dfs grid x y []) | x <- [0..length (head grid)-1], y <- [0..length grid-1], lowPoint grid x y]))
  where getSafe g x y = if x < 0 || x >= length (head g) || y < 0 || y >= length g then Nothing else Just ((g !! y) !! x)
        lowPoint g x y = all ((>((g !! y) !! x)).fromJust) (filter isJust (uncurry (getSafe g) <$> [(x-1, y), (x+1, y), (x, y-1), (x, y+1)]))
        dfs g x y v = let ns = filter (\(i, j) -> isJust (getSafe g i j) && ((g !! j) !! i) /= 9 && ((g !! j) !! i) > ((g !! y) !! x)) [(x-1, y), (x+1, y), (x, y-1), (x, y+1)] in
                        foldr (\(i, j) v' -> if (i, j) `elem` v' then v' else dfs g i j v') ((x,y):v) ns

day9_oneline :: String -> (Int, Int)
day9_oneline input = let (grid, test, ns) = ((digitToInt <$>) <$> lines input, \g x y -> x >= 0 && x < length (head g) && y >= 0 && y < length g, \x y -> [(x-1, y), (x+1, y), (x, y-1), (x, y+1)]) in let dfs g x y v = foldr (\(i, j) v' -> if not (test g i j) || ((g !! j) !! i) == 9 || (i, j) `elem` v' then v' else dfs g i j v') ((x,y):v) (ns x y) in let (p1, p2) = unzip [(1 + (grid !! y) !! x, length (dfs grid x y [])) | x <- [0..length (head grid)-1], y <- [0..length grid-1], all ((>((grid !! y) !! x)).(\(x, y) -> (grid !! y) !! x)) (filter (uncurry (test grid)) (ns x y))] in (sum p1, product (take 3 (sortOn negate p2)))