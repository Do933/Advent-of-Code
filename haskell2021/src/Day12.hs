module Day12 (day12, day12_oneline) where

import Data.Char
import Data.List
import Data.Maybe
import Data.Map as M hiding (split, foldr)
import Data.Set as S hiding (split, foldr)
import Util

import Debug.Trace

day12 :: String -> (Int, Int)
day12 input = (part1 input, part2 input)

part1 :: String -> Int
part1 input = let caves = foldr readCave M.empty (lines input) in
                length (search "start" caves [] [])
  where readCave line caves = let [from, to] = split '-' line in
                                do let c1 = M.insert from (to:fromMaybe [] (M.lookup from caves)) caves
                                   M.insert to (from:fromMaybe [] (M.lookup to c1)) c1
        search cave caves visited path
          | cave `elem` visited = []
          | cave == "end"       = [reverse (cave:path)]
          | otherwise           = (\c -> search c caves (if isLower (head cave) then cave:visited else visited) (cave:path)) =<< fromJust (M.lookup cave caves)

part2 :: String -> Int
part2 input = let caves = foldr readCave M.empty (lines input) in
                S.size (search "start" caves S.empty [] False)
  where readCave line caves = let [from, to] = split '-' line in
                                do let c1 = M.insert from (to:fromMaybe [] (M.lookup from caves)) caves
                                   M.insert to (from:fromMaybe [] (M.lookup to c1)) c1
        search cave caves visited path small
          | S.member cave visited = S.empty
          | cave == "end"         = S.singleton (path ++ cave)
          | otherwise             = let cs = fromJust (M.lookup cave caves)
                                        ps1 = S.unions ((\c -> search c caves (if isLower (head cave) then S.insert cave visited else visited) (path++cave) small) <$> cs)
                                        ps2 = S.unions ((\c -> if small || cave == "start" || isUpper (head cave) then S.empty else search c caves visited (path++cave) True) <$> cs) in
                                      S.union ps1 ps2

day12_oneline :: String -> (Int, Int)
day12_oneline input = let (caves, search) = (foldr (\l cs -> let [from, to] = split '-' l in let c1 = M.insert from (to:fromMaybe [] (M.lookup from cs)) cs in M.insert to (from:fromMaybe [] (M.lookup to c1)) c1) M.empty (lines input), \c cs v p s -> if S.member c v then S.empty else if c == "end" then S.singleton (p++c) else let ns = fromJust (M.lookup c cs) in let (ps1, ps2) = (S.unions ((\n -> search n cs (if isLower (head c) then S.insert c v else v) (p++c) s) <$> ns), S.unions ((\n -> if s || c == "start" || isUpper (head c) then S.empty else search n cs v (p++c) True) <$> ns)) in S.union ps1 ps2) in let [x, y] = length.search "start" caves S.empty [] <$> [True, False] in (x, y)
