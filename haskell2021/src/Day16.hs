module Day16 (day16) where

import Data.Char
import Data.List
import Data.Maybe
import Util

import Debug.Trace

day16 :: String -> Int
day16 = part2

hexToBin :: String -> String
hexToBin s = foldr (\c bin -> (["0000", "0001", "0010", "0011", "0100", "0101", "0110", "0111", "1000", "1001", "1010", "1011", "1100", "1101", "1110", "1111"] !! fromJust (elemIndex c ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'])) ++ bin) [] s

binToInt :: String -> Int
binToInt s = foldl (\n c -> 2 * n + if c == '0' then 0 else 1) 0 s

data Packet = ValuePacket Int Int | OperatorPacket Int Int [Packet] deriving Show

part1 :: String -> Int
part1 input = sumVersions $ fst $ fromJust $ decodePacket (hexToBin input)
  where decodePacket packet = do let version = binToInt (take 3 packet)
                                 let packet' = drop 3 packet

                                 let typeId = binToInt (take 3 packet')
                                 let packet'' = drop 3 packet'

                                 if typeId == 4 then let vs = decodeValue packet'' in Just (ValuePacket version (binToInt (concat vs)), 6 + 5 * length vs)
                                 else do let lengthId = if head packet'' == '0' then 0 else 1
                                         let packet''' = tail packet''

                                         if lengthId == 0 then let l = binToInt (take 15 packet''') in Just (OperatorPacket version typeId (fst (decodeOperator (drop 15 packet''') l (-1))), 22 + l)
                                         else let amt = binToInt (take 11 packet''') in let (ps, l) = decodeOperator (drop 11 packet''') (-1) amt in Just (OperatorPacket version typeId ps, 18 + l)

        decodeValue packet
          | null packet =        []
          | head packet == '0' = [tail (take 5 packet)]
          | otherwise =          tail (take 5 packet) : decodeValue (drop 5 packet)
        decodeOperator packet length amt = if length == 0 || amt == 0 || null packet then ([], 0)
                                           else let (Just (p, l)) = decodePacket packet
                                                    (ps, ls) = decodeOperator (drop l packet) (length - l) (amt - 1) in
                                                  (p : ps, l + ls)
        sumVersions (ValuePacket v _) = v
        sumVersions (OperatorPacket v _ ps) = v + sum (sumVersions <$> ps)

part2 :: String -> Int
part2 input = eval $ fst $ fromJust $ decodePacket (hexToBin input)
  where decodePacket packet = do let version = binToInt (take 3 packet)
                                 let packet' = drop 3 packet

                                 let typeId = binToInt (take 3 packet')
                                 let packet'' = drop 3 packet'

                                 if typeId == 4 then let vs = decodeValue packet'' in Just (ValuePacket version (binToInt (concat vs)), 6 + 5 * length vs)
                                 else do let lengthId = if head packet'' == '0' then 0 else 1
                                         let packet''' = tail packet''

                                         if lengthId == 0 then let l = binToInt (take 15 packet''') in Just (OperatorPacket version typeId (fst (decodeOperator (drop 15 packet''') l (-1))), 22 + l)
                                         else let amt = binToInt (take 11 packet''') in let (ps, l) = decodeOperator (drop 11 packet''') (-1) amt in Just (OperatorPacket version typeId ps, 18 + l)

        decodeValue packet
          | null packet =        []
          | head packet == '0' = [tail (take 5 packet)]
          | otherwise =          tail (take 5 packet) : decodeValue (drop 5 packet)
        decodeOperator packet length amt = if length == 0 || amt == 0 || null packet then ([], 0)
                                           else let (Just (p, l)) = decodePacket packet
                                                    (ps, ls) = decodeOperator (drop l packet) (length - l) (amt - 1) in
                                                  (p : ps, l + ls)
        eval (ValuePacket _ v) = v
        eval (OperatorPacket _ 0 ps) = sum (eval <$> ps)
        eval (OperatorPacket _ 1 ps) = product (eval <$> ps)
        eval (OperatorPacket _ 2 ps) = minimum (eval <$> ps)
        eval (OperatorPacket _ 3 ps) = maximum (eval <$> ps)
        eval (OperatorPacket _ 5 ps) = if eval (head ps) > eval (ps !! 1) then 1 else 0
        eval (OperatorPacket _ 6 ps) = if eval (head ps) < eval (ps !! 1) then 1 else 0
        eval (OperatorPacket _ 7 ps) = if eval (head ps) == eval (ps !! 1) then 1 else 0
