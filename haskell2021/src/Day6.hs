module Day6 (day6, day6_oneline) where

import Util

day6 :: String -> (Int, Int)
day6 input = (part1 input, part2 input)

part1 :: String -> Int
part1 input = let timers = read <$> split ',' (head (lines input)) in
                length (step timers 80)
  where step ts 0 = ts
        step ts n = step (((\i -> if i - 1 < 0 then 6 else i - 1) <$> ts) ++ replicate (length (filter (==0) ts)) 8) (n-1)

part2 :: String -> Int
part2 input = let timers = read <$> split ',' (head (lines input)) in
                sum (step [length (filter (==i) timers) | i <- [0..8]] 256)
  where step ts 0 = ts
        step ts n = step ([ts !! (i+1) | i <- [0..5]] ++ [(ts !! 7) + head ts, ts !! 8, head ts]) (n-1)

day6_oneline :: String -> (Int, Int)
day6_oneline input = let f d = sum (foldr (\_ [a,b,c,d,e,f,g,h,i] -> [b,c,d,e,f,g,h+a,i,a]) [length (filter (==i) (read <$> split ',' input)) | i <- [0..8]] [0..d-1]) in (f 80, f 256)