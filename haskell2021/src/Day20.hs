module Day20 (day20) where

import Control.Applicative
import Control.Monad
import Data.Char
import Data.Either
import Data.List as L
import Data.Maybe
import Data.Set as S
import Text.Regex
import Util

import Debug.Trace

type Point = (Int, Int)

binToInt :: String -> Int
binToInt = L.foldl (\n c -> 2 * n + if c == '0' then 0 else 1) 0

day20 :: String -> (Int, Int)
day20 input = (part1 input, part2 input)

nextPixel :: Point -> S.Set Point -> [Bool] -> Int -> Int -> Int -> Int -> Bool -> Bool
nextPixel (x, y) ps out minX minY maxX maxY inf1s =
  let isLit i j = if i < minX || i > maxX || j < minY || j > maxY then inf1s else S.member (i, j) ps
      idx = binToInt [if isLit (x+dx) (y+dy) then '1' else '0' | dy <- [-1..1], dx <- [-1..1]] in
    out !! idx

nextPoints :: S.Set Point -> [Bool] -> Bool -> S.Set Point
nextPoints ps out inf1s = let (cmpFst, cmpSnd) = (\(x1, _) (x2, _) -> compare x1 x2, \(_, y1) (_, y2) -> compare y1 y2)
                              minX = fst (minimumBy cmpFst ps)
                              maxX = fst (maximumBy cmpFst ps)
                              minY = snd (minimumBy cmpSnd ps)
                              maxY = snd (maximumBy cmpSnd ps) in
                            S.fromList [(x, y) | x <- [minX-1..maxX+1], y <- [minY-1..maxY+1], nextPixel (x, y) ps out minX minY maxX maxY inf1s]

step :: S.Set Point -> [Bool] -> Int -> S.Set Point
step ps out 0 = ps
step ps out n = step (nextPoints ps out (head out && odd n)) out (n-1)

part1 :: String -> Int
part1 input = let outputMap = (=='#') <$> head (lines input)
                  ps = ((=='#') <$>) <$> L.drop 2 (lines input) in
                S.size (step (S.fromList[(x, y) | y <- [0..length ps-1], x <- [0..length (head ps)-1], (ps !! y) !! x]) outputMap 2)

part2 :: String -> Int
part2 input = let outputMap = (=='#') <$> head (lines input)
                  ps = ((=='#') <$>) <$> L.drop 2 (lines input) in
                S.size (step (S.fromList[(x, y) | y <- [0..length ps-1], x <- [0..length (head ps)-1], (ps !! y) !! x]) outputMap 50)
