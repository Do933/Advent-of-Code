module Day18 (day18) where

import Control.Applicative hiding (many)
import Data.Char
import Data.Either
import Data.List
import Data.Maybe
import Text.Parsec hiding ((<|>))
import Text.Parsec.Char
import Text.Regex
import Util

import Debug.Trace

data Tree = Leaf { value :: Int } | Tree { left :: Tree, right :: Tree } deriving Eq

instance Show Tree where
  show (Leaf v) = show v
  show (Tree l r) = "[" ++ show l ++ "," ++ show r ++ "]"

parseTree :: String -> Tree
parseTree line = fromRight undefined (runParser treeParser () "Input" line)
  where pairParser = do _ <- char '['
                        l <- pairParser <|> leafParser
                        _ <- char ','
                        r <- pairParser <|> leafParser
                        _ <- char ']'
                        return (Tree l r)
        leafParser = Leaf . read <$> many digit
        treeParser = pairParser <|> leafParser

replaceSubTree :: Tree -> [Int] -> Tree -> Tree
replaceSubTree _ [] with = with
replaceSubTree (Tree l r) (0:path) with = Tree (replaceSubTree l path with) r
replaceSubTree (Tree l r) (1:path) with = Tree l (replaceSubTree r path with)

leftClosest :: [Int] -> Maybe [Int]
leftClosest ps = do i <- elemIndex 1 (reverse ps)
                    return (reverse (0 : drop (i+1) (reverse ps)))

rightClosest :: [Int] -> Maybe [Int]
rightClosest ps = do i <- elemIndex 0 (reverse ps)
                     return (reverse (1 : drop (i+1) (reverse ps)))

leftMost :: Tree -> (Tree, Int)
leftMost t@(Leaf _) = (t, 0)
leftMost (Tree l _) = let (t, d) = leftMost l in (t, d+1)

rightMost :: Tree -> (Tree, Int)
rightMost t@(Leaf _) = (t, 0)
rightMost (Tree _ r) = let (t, d) = rightMost r in (t, d+1)

findTree :: Tree -> [Int] -> Maybe Tree
findTree t [] = Just t
findTree (Leaf _) path = Nothing
findTree (Tree l _) (0:path) = findTree l path
findTree (Tree _ r) (1:path) = findTree r path

removeNested :: Tree -> [Int] -> Maybe (Tree, [Int], (Int, Int))
removeNested n@(Leaf _) _ = Nothing
removeNested (Tree (Leaf a) (Leaf b)) path
  | length path >= 4 = Just (Leaf 0, reverse path, (a, b))
  | otherwise        = Nothing
removeNested (Tree l r) path = (do (newChild, pathTo, replaced) <- removeNested l (0:path)
                                   return (Tree newChild r, pathTo, replaced))
                            <|> do (newChild, pathTo, replaced) <- removeNested r (1:path)
                                   return (Tree l newChild, pathTo, replaced)

explode :: Tree -> Maybe Tree
explode tree = do (newTree, path, (lVal, rVal)) <- removeNested tree []
                  leftReplaced <- addLeft newTree path lVal <|> Just newTree
                  addRight leftReplaced path rVal <|> Just leftReplaced
  where addLeft tree path val = do pathToParent <- leftClosest path
                                   parent <- findTree tree pathToParent
                                   let (Leaf lVal, addDepth) = rightMost parent
                                   return (replaceSubTree tree (pathToParent ++ replicate addDepth 1) (Leaf (val + lVal)))
        addRight tree path val = do pathToParent <- rightClosest path
                                    parent <- findTree tree pathToParent
                                    let (Leaf lVal, addDepth) = leftMost parent
                                    return (replaceSubTree tree (pathToParent ++ replicate addDepth 0) (Leaf (val + lVal)))

findSplit :: Tree -> [Int] -> Maybe [Int]
findSplit (Leaf n) path = if n >= 10 then Just (reverse path) else Nothing
findSplit (Tree l r) path = findSplit l (0:path) <|> findSplit r (1:path)

doSplit :: Tree -> Maybe Tree
doSplit tree = do path <- findSplit tree []
                  (Leaf val) <- findTree tree path
                  let lVal = val `div` 2
                  let rVal = (val + 1) `div` 2
                  return (replaceSubTree tree path (Tree (Leaf lVal) (Leaf rVal)))

balance :: Tree -> Tree
balance tree = fromMaybe tree ((balance <$> explode tree) <|> (balance <$> doSplit tree))

(+++) :: Tree -> Tree -> Tree
a +++ b = balance (Tree a b)

magnitude :: Tree -> Int
magnitude (Leaf v) = v
magnitude (Tree l r) = 3 * magnitude l + 2 * magnitude r

day18 :: String -> (Int, Int)
day18 input = (part1 input, part2 input)

part1 :: String -> Int
part1 input = magnitude (foldl1 (+++) (parseTree <$> lines input))

part2 :: String -> Int
part2 input = let trees = parseTree <$> lines input in
                maximum [magnitude (a +++ b) | a <- trees, b <- trees, a /= b]