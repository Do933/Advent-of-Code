module Day4 (day4, day4_oneline) where

import Data.Foldable
import Data.Maybe
import Data.List
import Data.Ord (comparing)
import Util (split)

data Board = Board { rows :: [[Int]]
                   , cols :: [[Int]] } deriving (Show, Eq)

day4 :: String -> (Int, Int)
day4 input = (part1 input, part2 input)

readInput :: String -> ([Int], [Board])
readInput input = let ls = lines input in (read <$> split ',' (head ls), readBoards (tail ls))
  where readBoards [] = []
        readBoards (_:r1:r2:r3:r4:r5:rest) = let rs = [read <$> r | r <- [words r1, words r2, words r3, words r4, words r5]] in
                                              Board rs ([[(rs !! r) !! c | r <- [0..4]] | c <- [0..4]]) : readBoards rest

finished :: Board -> [Int] -> Maybe [Int]
finished board nums = case filter (`allIn` nums) (rows board ++ cols board) of
                        [] -> Nothing
                        _ -> Just (nub [n | ns <- rows board ++ cols board, n <- ns, n `notElem` nums])
  where allIn xs ys = length (filter (`elem` ys) xs) == length xs

part1 :: String -> Int
part1 input = let (nums, boards) = readInput input
                  (unmarked, winningNum) = getFinish nums boards [] in sum unmarked * winningNum
  where getFinish :: [Int] -> [Board] -> [Int] -> ([Int], Int)
        getFinish [] _ _ = error "No finish"
        getFinish (n:ns) bs taken = case filter isJust [finished b (n:taken) | b <- bs] of
                                      [] -> getFinish ns bs (n:taken)
                                      (res:_) -> (fromJust res, n)

part2 :: String -> Int
part2 input = let (nums, boards) = readInput input
                  results = (\b -> getFinish nums b []) <$> boards
                  (unmarked, winningNum, _) = maximumBy (comparing (\(_, _, l) -> l)) results in sum unmarked * winningNum
  where getFinish :: [Int] -> Board -> [Int] -> ([Int], Int, Int)
        getFinish [] _ _ = error "No finish"
        getFinish (n:ns) b taken = case finished b (n:taken) of
                                    Nothing -> getFinish ns b (n:taken)
                                    Just res -> (res, n, length taken)

day4_oneline :: String -> (Int, Int)
day4_oneline input = let (ns, bs, getFinish) = (map read (split ',' (head (lines input))), [let rs = [map read ns | ns <- words <$> tail (take 6 (drop (i*6) (tail (lines input))))] in rs ++ [map (!!c) rs | c <- [0..4]] | i <- [0..(length (lines input) `div` 6)-1]], \ns b -> let (r, t) = foldl (\(res, taken) n -> if null res then let res = (if not (any ((`elem` (n:taken)) `all`) b) then [] else nub [n' | ns <- b, n' <- ns, n' `notElem` (n:taken)]) in (res, n:taken) else (res, taken)) ([], []) ns in (sum r, head t, length t)) in let (fs, cmp, rf) = (map (ns `getFinish`) bs, comparing (\(_, _, l) -> l), \(a, b, _) -> a * b) in (rf (minimumBy cmp fs), rf (maximumBy cmp fs))
