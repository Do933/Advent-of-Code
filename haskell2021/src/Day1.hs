module Day1 (day1, day1_oneline) where

day1 :: String -> (Int, Int)
day1 input = let xs = read <$> lines input
                 ys = (\(x, y, z) -> x + y + z) <$> zip3 xs (tail xs) (tail (tail xs))
                 amtInc ns = length $ filter (\(x, y) -> y > x) (zip ns (tail ns)) in
              (amtInc xs, amtInc ys)

day1_oneline :: String -> (Int, Int)
day1_oneline input = let xs = read <$> lines input in (length $ filter (\(x, y) -> y > x) (zip xs (tail xs)), let ys = (\(x, y, z) -> x + y + z) <$> zip3 xs (tail xs) (tail (tail xs)) in length $ filter (\(x, y) -> y > x) (zip ys (tail ys)))