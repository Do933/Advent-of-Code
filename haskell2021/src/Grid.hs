module Grid where

import Data.Maybe
import Data.List

data Grid a = Grid { values :: [a], width :: Int, height :: Int }

instance Show a => Show (Grid a) where
  show g = intercalate "\n" [show [(g !!! (x, y)) | x <- [0..width g-1]] | y <- [0..height g-1]]

instance Eq a => Eq (Grid a) where
  g1 == g2 = width g1 == width g2 && values g1 == values g2

instance Functor Grid where
  fmap f g = Grid (map f (values g)) (width g) (height g)

instance Foldable Grid where
  foldr f i g = foldr f i (values g)

instance Traversable Grid where
  traverse f g = (\vs -> fromList vs (width g) (height g)) <$> traverse f (values g)

fromMatrix :: [[a]] -> Grid a
fromMatrix matrix = Grid [(matrix !! y) !! x | y <- [0..length matrix-1], x <- [0..length (head matrix)-1]] (length matrix) (length (head matrix))

fromList :: [a] -> Int -> Int -> Grid a
fromList = Grid

withinBounds :: Grid a -> Int -> Int -> Bool
withinBounds g x y = x >= 0 && x < width g && y >= 0 && y < height g

getCell :: Grid a -> Int -> Int -> Maybe a
getCell g x y = if withinBounds g x y then Just (values g !! (x + y * width g)) else Nothing

(!!!) :: Grid a -> (Int, Int) -> a
g !!! (x, y) = fromMaybe (error (show x ++ "," ++ show y ++ " out of bounds of grid of size " ++ show (width g) ++ " x " ++ show (height g))) (getCell g x y)

setCell :: Grid a -> Int -> Int -> a -> Grid a
setCell g x y v = let i = x + y * width g in Grid (take i (values g) ++ (v : drop (i+1) (values g))) (width g) (height g)

around4 :: Grid a -> Int -> Int -> [(Int, Int)]
around4 g x y = filter (uncurry (withinBounds g)) [(x-1, y), (x+1, y), (x, y-1), (x, y+1)]

around8 :: Grid a -> Int -> Int -> [(Int, Int)]
around8 g x y = [(i, j) | i <- [x-1..x+1], j <- [y-1..y+1],  (i /= x || j /= y) && withinBounds g i j]

zipGrids :: Grid a -> Grid b -> Grid (a, b)
zipGrids g1 g2 = let w = min (width g1) (width g2)
                     h = min (height g1) (height g2) in
                  fromMatrix [[(g1 !!! (x, y), g2 !!! (x, y)) | x <- [0..w-1]] | y <- [0..h-1]]

unzipGrid :: Grid (a, b) -> (Grid a, Grid b)
unzipGrid g = (fst <$> g, snd <$> g)