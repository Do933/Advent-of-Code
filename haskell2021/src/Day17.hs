module Day17 (day17, day17_oneline) where

import Data.Char
import Data.List
import Data.Maybe
import Text.Regex
import Util

import Debug.Trace

day17 :: String -> (Int, Int)
day17 input = (part1 input, part2 input)

part1 :: String -> Int
part1 input = let Just [tx1, tx2, ty1, ty2] = ((\x -> read x :: Int) <$>) <$> matchRegex (mkRegex "target area: x=(-?[0-9]+)\\.\\.(-?[0-9]+), y=(-?[0-9]+)\\.\\.(-?[0-9]+)") input in
                let n = abs ty1 in (n*(n-1)) `div` 2
                --maximum (top.snd <$> search tx1 tx2 ty1 ty2)
  where search tx1 tx2 ty1 ty2 = filter (uncurry (isIn tx1 tx2 ty1 ty2)) [(vx, vy) | vx <- [0..tx2], vy <- [ty1..256]]
        isIn tx1 tx2 ty1 ty2 vx0 vy0 = any (\(x, y) -> x >= tx1 && y >= ty1 && x <= tx2 && y <= ty2) [(let d = min (abs vx0) i in vx0*d - signum vx0 * ((d*(d-1)) `div` 2), vy0*i - ((i*(i-1)) `div` 2)) | i <- [0..256]]
        top vy0 = let i = (2 * vy0 + 1) `div` 2 in vy0*i - ((i*(i-1)) `div` 2)

part2 :: String -> Int
part2 input = let Just [tx1, tx2, ty1, ty2] = ((\x -> read x :: Int) <$>) <$> matchRegex (mkRegex "target area: x=(-?[0-9]+)\\.\\.(-?[0-9]+), y=(-?[0-9]+)\\.\\.(-?[0-9]+)") input in
                length (filter (uncurry (isIn tx1 tx2 ty1 ty2)) [(vx, vy) | vx <- [ceiling ((-1 + sqrt (1 + 8 * fromIntegral tx1)) / 2)..tx2], vy <- [ty1..abs ty1]])
  where isIn tx1 tx2 ty1 ty2 vx0 vy0 = any (\(x, y) -> x >= tx1 && y >= ty1 && x <= tx2 && y <= ty2) [(let d = min (abs vx0) i in vx0*d - signum vx0 * ((d*(d-1)) `div` 2), vy0*i - ((i*(i-1)) `div` 2)) | i <- [0..256]]

day17_oneline :: String -> (Int, Int)
day17_oneline input = let Just [tx1, tx2, ty1, ty2] = (read <$>) <$> matchRegex (mkRegex "target area: x=(-?[0-9]+)\\.\\.(-?[0-9]+), y=(-?[0-9]+)\\.\\.(-?[0-9]+)") input in let vs = filter (\(vx0, vy0) -> any (\(x, y) -> x >= tx1 && y >= ty1 && x <= tx2 && y <= ty2) [(let d = min (abs vx0) i in vx0*d - signum vx0 * ((d*(d-1)) `div` 2), vy0*i - ((i*(i-1)) `div` 2)) | i <- [0..1000]]) [(vx, vy) | vx <- [0..tx2], vy <- [ty1..1000]] in (maximum ((\(_, vy0) -> let i = (2 * vy0 + 1) `div` 2 in vy0*i - ((i*(i-1)) `div` 2)) <$> vs), length vs)
