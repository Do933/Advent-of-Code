module Day3 (day3, day3_oneline) where

day3 :: String -> (Int, Int)
day3 input = (part1 input, part2 input)

toDec :: String -> Int
toDec [] = 0
toDec (x:xs) = (if x == '0' then 0 else 2 ^ length xs) + toDec xs

invert :: String -> String
invert = map (\x -> if x == '0' then '1' else '0')

part1 :: String -> Int
part1 input = let ns = lines input
                  b = [if 2 * length (filter ((=='1').(!!i)) ns) > length ns then '1' else '0' | i <- [0..length (head ns) - 1]] in
               toDec b * toDec (invert b)

part2 :: String -> Int
part2 input = let ns = lines input
                  co2 = filterMostCommon ns 0
                  ox = filterLeastCommon ns 0 in
               toDec co2 * toDec ox
  where filterMostCommon [] i = []
        filterMostCommon [x] i = x
        filterMostCommon xs i = if 2 * length (filter (=='1') (map (!!i) xs)) >= length xs
                                then filterMostCommon (filter (\b -> b !! i == '1') xs) (i+1)
                                else filterMostCommon (filter (\b -> b !! i == '0') xs) (i+1)
        filterLeastCommon [] i = []
        filterLeastCommon [x] i = x
        filterLeastCommon xs i = if 2 * length (filter (=='0') (map (!!i) xs)) <= length xs
                                 then filterLeastCommon (filter (\b -> b !! i == '0') xs) (i+1)
                                 else filterLeastCommon (filter (\b -> b !! i == '1') xs) (i+1)

day3_oneline :: String -> (Int, Int)
day3_oneline input = let (z, xs, is, dec) = (\xs i -> 2 * length (filter ((=='0').(!!i)) xs) <= length xs, lines input, [0..length (head (lines input))-1], foldl (\n b -> n * 2 + if b == '0' then 0 else 1) 0) in let (ox:_, co2:_) = foldl (\(ls, ms) i -> let cs c = filter ((==c).(!!i)) in (if length ls <= 1 then ls else cs (if z ls i then '0' else '1') ls, cs (if z ms i then '1' else '0') ms)) (xs, xs) is in (let inv c = if c then '1' else '0' in product (map dec (let b = map (inv.z xs) is in [b, map (inv.(=='0')) b])), dec ox * dec co2)
