module Day21 (day21) where

import Control.Applicative
import Control.Monad
import Data.Char
import Data.Either
import Data.List as L
import Data.Maybe
import Data.Set as S
import Text.Regex
import Util

import Debug.Trace

day21 :: String -> (Int, Int)
day21 input = (part1 input, part2 input)

score :: Int -> Int -> [Int] -> Int -> Int
score _ 0 _ _ = 0
score p t dice offset = score p (t-1) dice offset + 1 +
                          ((p + sum [(dice !! (6*i+offset)) + (dice !! (6*i+offset+1)) + (dice !! (6*i+offset+2)) | i <- [0..t-1]]) `mod` 10)

part1 :: String -> Int
part1 input = let p1 = read (last (words (head (lines input)))) - 1
                  p2 = read (last (words (lines input !! 1))) - 1
                  dice = cycle [1..100]
                  scores = [(t, score p1 t dice 0, score p2 t dice 3) | t <- [100..]] in
                let (t, s1, s2) = head $ L.filter (\(_, s1, s2) -> s1 >= 1000 || s2 >= 1000) scores in
                  if s1 > s2 then score p2 (t-1) dice 3 * (t * 6 - 3)
                  else s1 * t * 6

chance :: Int -> Int -> Int -> Int -> (Int, Int)
chance p1 p2 s1 s2
  | s1 >= 21  = (1, 0)
  | s2 >= 21  = (0, 1)
  | otherwise = let probMap = [(3, 1), (4, 3), (5, 6), (6, 7), (7, 6), (8, 3), (9, 1)] in
                  L.foldr (\(a, b) (c, d) -> (a+c, b+d)) (0, 0)
                          (do (inc1, n1) <- probMap
                              (inc2, n2) <- probMap
                              let p1' = (p1 + inc1) `mod` 10
                              let p2' = (p2 + inc2) `mod` 10
                              let (!c1, !c2) = chance p1' p2' (s1 + p1' + 1) (s2 + p2' + 1)
                              return (n1*n2*c1, n1*n2*c2))

part2 :: String -> Int
part2 input = let p1 = read (last (words (head (lines input)))) - 1
                  p2 = read (last (words (lines input !! 1))) - 1 in
                let (x, y) = chance p1 p2 0 0 in
                   if x > 27 * y then x `div` 27 else y
