module Day10 (day10, day10_oneline) where

import Data.Char
import Data.List
import Data.Maybe
import Util

import Debug.Trace

day10 :: String -> (Int, Int)
day10 input = (part1 input, part2 input)

part1 :: String -> Int
part1 input = sum $ (`checkCorrupt` []) <$> lines input
  where checkCorrupt [] _ = 0
        checkCorrupt (c:cs) stack = case c of
          '(' -> checkCorrupt cs (c:stack)
          '[' -> checkCorrupt cs (c:stack)
          '{' -> checkCorrupt cs (c:stack)
          '<' -> checkCorrupt cs (c:stack)
          ')' -> if head stack == '(' then checkCorrupt cs (tail stack) else 3
          ']' -> if head stack == '[' then checkCorrupt cs (tail stack) else 57
          '}' -> if head stack == '{' then checkCorrupt cs (tail stack) else 1197
          '>' -> if head stack == '<' then checkCorrupt cs (tail stack) else 25137

part2 :: String -> Int
part2 input = median ((`getScore` 0).(`toFill` []) <$> filter (\x -> checkCorrupt x [] == 0) (lines input))
  where checkCorrupt [] _ = 0
        checkCorrupt (c:cs) stack = case c of
          '(' -> checkCorrupt cs (c:stack)
          '[' -> checkCorrupt cs (c:stack)
          '{' -> checkCorrupt cs (c:stack)
          '<' -> checkCorrupt cs (c:stack)
          ')' -> if head stack == '(' then checkCorrupt cs (tail stack) else 3
          ']' -> if head stack == '[' then checkCorrupt cs (tail stack) else 57
          '}' -> if head stack == '{' then checkCorrupt cs (tail stack) else 1197
          '>' -> if head stack == '<' then checkCorrupt cs (tail stack) else 25137
        toFill [] stack = stack
        toFill (c:cs) stack = case c of
          '(' -> toFill cs (c:stack)
          '[' -> toFill cs (c:stack)
          '{' -> toFill cs (c:stack)
          '<' -> toFill cs (c:stack)
          ')' -> toFill cs (tail stack)
          ']' -> toFill cs (tail stack)
          '}' -> toFill cs (tail stack)
          '>' -> toFill cs (tail stack)
        getScore [] sc = sc
        getScore (x:xs) sc = getScore xs (5 * sc + fromJust (x `elemIndex` ['.', '(', '[', '{', '<']))
        median xs = sort xs !! (length xs `div` 2)

day10_oneline :: String -> (Int, Int)
day10_oneline input = let ps = foldl (\(w, s) c -> if c `elem` "([{<" then (w, c:s) else if abs (ord c - ord (head s)) > 2 then (c:w, s) else (w, tail s)) ([], []) <$> lines input in (sum (fromJust.(`lookup` [(')', 3), (']', 57), ('}', 1197), ('>', 25137)]).last.fst <$> filter (not.null.fst) ps), let xs = foldl (\t c -> 5 * t + fromJust (c `elemIndex` " ([{<")) 0.snd <$> filter (null.fst) ps in sort xs !! (length xs `div` 2))
