module Util where

split :: Char -> String -> [String]
split c s = case dropWhile (==c) s of
              "" -> []
              s' -> w : split c s''
                      where (w, s'') = break (==c) s'