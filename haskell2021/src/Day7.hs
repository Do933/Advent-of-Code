module Day7 (day7, day7_oneline) where

import Data.List
import Util

day7 :: String -> (Int, Int)
day7 input = (part1 input, part2 input)

part1 :: String -> Int
part1 input = let nums = (\x -> read x :: Int) <$> split ',' input
                  med = sort nums !! (length nums `div` 2) in sum (abs.(med-) <$> nums)

part2 :: String -> Int
part2 input = let nums = (\x -> read x :: Int) <$> split ',' input
                  mean = round (fromIntegral (sum nums) / fromIntegral (length nums)) in
                minimum ((\m -> sum (costTo m <$> nums)) <$> [mean-1..mean+1])
  where costTo n i = let dis = abs (i - n) in (dis * (dis+1)) `div` 2

day7_oneline :: String -> (Int, Int)
day7_oneline input = let nums = read <$> split ',' input in (sum (abs.((sort nums !! (length nums `div` 2))-) <$> nums), minimum ((\m -> sum ((\i -> let d = abs (i - m) in (d*(d+1)) `div` 2) <$> nums)) <$> [minimum nums..maximum nums]))