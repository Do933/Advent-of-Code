{-# LANGUAGE OverloadedStrings #-}

module Day13 (day13, day13_oneline, day13_graphics) where

import Data.Char
import Data.List
import Data.Maybe
import Data.Set as S hiding (drop, split, null, foldl)
import Data.Text (Text)

import Control.Concurrent
import Graphics.Blank

import System.IO

import Util

import Debug.Trace

day13 :: String -> (Int, String)
day13 input = (part1 input, part2 input)

part1 :: String -> Int
part1 input = let (pls, ils) = (takeWhile (\l -> not (null l) && isDigit (head l)) (lines input), dropWhile (\l -> null l || isDigit (head l)) (lines input))
                  ps = (\l -> let [x, y] = split ',' l in (read x :: Int, read y :: Int)) <$> pls
                  is = (\l -> let [d, v] = split '=' (drop 11 l) in (head d, read v :: Int)) <$> ils in
                length $ applyFold ps (head is)
  where applyFold ps ('x', x) = nub ((\(i, j) -> (if i < x then i else x - (i - x), j)) <$> ps)
        applyFold ps ('y', y) = nub ((\(i, j) -> (i, if j < y then j else y - (j - y))) <$> ps)

part2 :: String -> String
part2 input = let (pls, ils) = (takeWhile (\l -> not (null l) && isDigit (head l)) (lines input), dropWhile (\l -> null l || isDigit (head l)) (lines input))
                  ps = (\l -> let [x, y] = split ',' l in (read x :: Int, read y :: Int)) <$> pls
                  is = (\l -> let [d, v] = split '=' (drop 11 l) in (head d, read v :: Int)) <$> ils in
                "\n\n" ++ intercalate "\n" [[if (x, y) `elem` foldl applyFold ps is then '#' else ' ' | x <- [0..40]] | y <- [0..6]] ++ "\n\n"
  where applyFold ps ('x', x) = nub ((\(i, j) -> (if i < x then i else x - (i - x), j)) <$> ps)
        applyFold ps ('y', y) = nub ((\(i, j) -> (i, if j < y then j else y - (j - y))) <$> ps)

day13_oneline :: String -> (Int, String)
day13_oneline input = let pls = takeWhile (\l -> not (null l) && isDigit (head l)) (lines input) in let (ps, ils) = ((\l -> let [x, y] = split ',' l in (read x, read y)) <$> pls, drop (length pls + 1) (lines input)) in let (is, doFold) = ((\l -> let [d, v] = split '=' (drop 11 l) in (head d, read v)) <$> ils, \ps (c, d) -> nub ((\(i, j) -> (if i < d || c /= 'x' then i else d - (i - d), if j < d || c /= 'y' then j else d - (j - d))) <$> ps)) in (length (doFold ps (head is)), intercalate "\n" [[if (x, y) `elem` foldl doFold ps is then '#' else ' ' | x <- [0..40]] | y <- [0..6]])

day13_graphics :: String -> IO ()
day13_graphics input = let (pls, ils) = (takeWhile (\l -> not (null l) && isDigit (head l)) (lines input), dropWhile (\l -> null l || isDigit (head l)) (lines input))
                           !ps = S.fromList ((\l -> let [x, y] = split ',' l in (read x :: Int, read y :: Int)) <$> pls)
                           !is = (\l -> let [d, v] = split '=' (drop 11 l) in (head d, read v :: Int)) <$> ils in
                        do draw ps is
                           return ()
  where draw :: Set (Int, Int) -> [(Char, Int)] -> IO ()
        draw ps is = startCanvas $ \context -> do
--                           send context drawBg
--                           send context (head <$> traverse (\(x, y) -> drawSquare x y "white") (foldl applyFold ps is))
                           animate context ps is
        applyFold :: S.Set (Int, Int) -> (Char, Int) -> S.Set (Int, Int)
        applyFold ps ('x', x) = S.map (\(i, j) -> (if i < x then i else x - (i - x), j)) ps
        applyFold ps ('y', y) = S.map (\(i, j) -> (i, if j < y then j else y - (j - y))) ps
        animate :: DeviceContext -> S.Set (Int, Int) -> [(Char, Int)] -> IO ()
        animate context ps [] = do send context drawBg
                                   send context (() <$ traverse (\(x, y) -> drawSquare x y "white") (S.toList ps))
        animate context ps (i:is) = do send context drawBg
                                       send context (() <$ traverse (\(x, y) -> drawSquare x y "white") (S.toList ps))
                                       threadDelay 500000
                                       print $ length is
                                       hFlush stdout
                                       animate context (applyFold ps i) is

startCanvas :: (DeviceContext -> IO ()) -> IO ()
startCanvas = blankCanvas 3000

drawSquare :: Int -> Int -> Text -> Canvas ()
drawSquare x y colour = do beginPath ()
                           rect (fromIntegral x * 4, fromIntegral y * 4, 4, 4)
                           fillStyle colour
                           fill ()

drawBg :: Canvas ()
drawBg = do beginPath ()
            rect (0, 0, 1920, 1080)
            fillStyle "black"
            fill ()
