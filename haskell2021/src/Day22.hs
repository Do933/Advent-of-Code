module Day22 (day22) where

import Control.Applicative
import Control.Monad
import Data.Char
import Data.Either
import Data.List as L
import Data.Maybe
import Data.Set as S
import Text.Regex
import Util

import Debug.Trace

day22 :: String -> (Int, Int)
day22 input = (part1 input, part2 input)

data AABB = AABB { x1 :: Int, y1 :: Int, z1 :: Int, x2 :: Int, y2 :: Int, z2 :: Int } deriving (Show, Eq)

aabbContains :: Int -> Int -> Int -> AABB -> Bool
aabbContains x y z aabb = x >= x1 aabb && x <= x2 aabb && y >= y1 aabb && y <= y2 aabb && z >= z1 aabb && z <= z2 aabb

(&&&) :: AABB -> AABB -> Maybe AABB
(AABB ax1 ay1 az1 ax2 ay2 az2) &&& (AABB bx1 by1 bz1 bx2 by2 bz2) =
  let ix1 = max ax1 bx1; iy1 = max ay1 by1; iz1 = max az1 bz1
      ix2 = min ax2 bx2; iy2 = min ay2 by2; iz2 = min az2 bz2 in
    if ix1 > ix2 || iy1 > iy2 || iz1 > iz2 then Nothing
    else Just (AABB ix1 iy1 iz1 ix2 iy2 iz2)

(\\\) :: AABB -> AABB -> [AABB]
a@(AABB ax1 ay1 az1 ax2 ay2 az2) \\\ b =
  case a &&& b of
    Nothing -> [a]
    Just (AABB cx1 cy1 cz1 cx2 cy2 cz2) ->
      let xIntervals = L.filter nonZero [(ax1, cx1-1), (cx1, cx2), (cx2+1, ax2)]
          yIntervals = L.filter nonZero [(ay1, cy1-1), (cy1, cy2), (cy2+1, ay2)]
          zIntervals = L.filter nonZero [(az1, cz1-1), (cz1, cz2), (cz2+1, az2)] in
        [AABB x1 y1 z1 x2 y2 z2 | (x1, x2) <- xIntervals, (y1, y2) <- yIntervals, (z1, z2) <- zIntervals, (x1, y1, z1, x2, y2, z2) /= (cx1, cy1, cz1, cx2, cy2, cz2)]
  where nonZero (x, y) = x <= y

(\\\\) :: [AABB] -> AABB -> [AABB]
bbs \\\\ c = bbs >>= (\\\c)

(|||) :: AABB -> AABB -> [AABB]
a ||| b = a : (b \\\ a)

(||||) :: [AABB] -> AABB -> [AABB]
bbs |||| c = bbs ++ subtractAll bbs [c]
  where subtractAll [] c = c
        subtractAll (b:bs) c = subtractAll bs (c \\\\ b)

volume :: AABB -> Int
volume (AABB x1 y1 z1 x2 y2 z2) = (x2-x1+1)*(y2-y1+1)*(z2-z1+1)

part1 :: String -> Int
part1 input = let aabbs = (\[st, coords] -> (st == "on", let [x1, x2, y1, y2, z1, z2] = read <$> fromJust (matchRegex (mkRegex "x=(-?[0-9]+)\\.\\.(-?[0-9]+),y=(-?[0-9]+)\\.\\.(-?[0-9]+),z=(-?[0-9]+)\\.\\.(-?[0-9]+)") coords) in AABB x1 y1 z1 x2 y2 z2))
                          .words <$> lines input in
                length (L.filter id [maybe False fst (find (\(_, a) -> aabbContains x y z a) (reverse (L.filter isInit aabbs))) | x <- getXRange aabbs, y <- getYRange aabbs, z <- getZRange aabbs])
  where isInit (_, AABB x1 y1 z1 x2 y2 z2) = x1 >= -50 && y1 >= -50 && z1 >= -50 && x2 <= 50 && y2 <= 50 && z2 <= 50
        getXRange aabbs = [-50..50]
        getYRange aabbs = [-50..50]
        getZRange aabbs = [-50..50]

part2 :: String -> Int
part2 input = let aabbs = (\[st, coords] -> (st == "on", let [x1, x2, y1, y2, z1, z2] = read <$> fromJust (matchRegex (mkRegex "x=(-?[0-9]+)\\.\\.(-?[0-9]+),y=(-?[0-9]+)\\.\\.(-?[0-9]+),z=(-?[0-9]+)\\.\\.(-?[0-9]+)") coords) in AABB x1 y1 z1 x2 y2 z2))
                          .words <$> lines input in
                sum (volume <$> L.foldr (\(add, bb) curr -> if add then curr |||| bb else curr \\\\ bb) [] (reverse aabbs))