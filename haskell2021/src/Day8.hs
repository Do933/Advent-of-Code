module Day8 (day8, day8_oneline) where

import Data.Char
import Data.List
import Data.Maybe
import Util

day8 :: String -> (Int, Int)
day8 input = (part1 input, part2 input)

part1 :: String -> Int
part1 input = length $ lines input >>= filter ((`elem` [2, 3, 4, 7]).length).words.tail.(!!1).split '|'

part2 :: String -> Int
part2 input = let codes = words <$> ((\[l, r] -> l ++ r).split '|' <$> lines input) in
                sum ((\line -> (\x -> read x :: Int) (intToDigit.(\code -> getNum $ decode code $ [getSegment i line | i <- [0..6]]) <$> drop (length line - 4) line)) <$> codes)
  where getSegmentsIn n cs = nub (concat (filter ((==([6, 2, 5, 5, 4, 5, 6, 3, 7, 6]!!n)).length) cs))
        getNot069 cs = nub (head.("abcdefg" \\) <$> filter ((==6).length) cs)
        getSegment 0 cs = head (getSegmentsIn 7 cs \\ getSegmentsIn 1 cs)
        getSegment 2 cs = head (getNot069 cs `intersect` getSegmentsIn 7 cs)
        getSegment 3 cs = head ((getNot069 cs \\ getSegmentsIn 7 cs) `intersect` getSegmentsIn 4 cs)
        getSegment 5 cs = head (delete (getSegment 2 cs) (getSegmentsIn 1 cs))
        getSegment 1 cs = head (getSegmentsIn 4 cs \\ [getSegment 2 cs, getSegment 3 cs, getSegment 5 cs])
        getSegment 4 cs = head (getNot069 cs \\ [getSegment 2 cs, getSegment 3 cs])
        getSegment 6 cs = head ("abcdefg" \\ [getSegment i cs | i <- [0..5]])
        decode code segs = sort ((\x -> "abcdefg" !! fromJust (elemIndex x segs)) <$> code)
        getNum "abcefg" = 0
        getNum "cf" = 1
        getNum "acdeg" = 2
        getNum "acdfg" = 3
        getNum "bcdf" = 4
        getNum "abdfg" = 5
        getNum "abdefg" = 6
        getNum "acf" = 7
        getNum "abcdefg" = 8
        getNum "abcdfg" = 9

day8_oneline :: String -> (Int, Int)
day8_oneline input = let codes = words <$> ((\[l, r] -> l ++ r).split '|' <$> lines input) in (length [c | cs <- codes, c <- length <$> drop 10 cs, c `elem` [2,3,4,7]], sum [read (intToDigit.fromJust.(`elemIndex` [42, 17, 34, 39, 30, 37, 41, 25, 49, 45]) <$> (sum.((\x -> ((\c -> length (filter (==c) (concat (take 10 cs)))) <$> "abcdefg") !! (ord x - 97)) <$>) <$> drop 10 cs)) | cs <- codes])

--day8_oneline :: String -> (Int, Int)
--day8_oneline input = let (codes, decode, remap, l4) = (words <$> ((\[l, r] -> l ++ r).split '|' <$> lines input), \e -> elemIndex e ["abcefg", "cf", "acdeg", "acdfg", "bcdf", "abdfg", "abdefg", "acf", "abcdefg", "abcdfg"], \enc e -> sort ((\x -> "abcdefg" !! fromJust (elemIndex x enc)) <$> e), \xs -> drop (length xs - 4) xs) in (length [c | cs <- codes, c <- length <$> l4 cs, c `elem` [2,3,4,7]], sum (read.(\rs -> intToDigit <$> l4 (fromJust <$> rs)) <$> filter (all isJust) [decode.remap p <$> cs | p <- permutations "abcdefg", cs <- codes]))
