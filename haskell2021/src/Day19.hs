module Day19 (day19) where

import Control.Applicative
import Control.Monad
import Control.Monad.State.Lazy
import Data.Char
import Data.Either
import Data.List as L
import Data.Maybe
import Data.Set as S
import Data.Map as M
import Text.Regex
import Util

import Debug.Trace

day19 :: String -> (Int, Int)
day19 input = (part1 input, part2 input)

type Point = (Int, Int, Int)
type Scanner = [Point]

addV :: Point -> Point -> Point
addV (x1, y1, z1) (x2, y2, z2) = (x1+x2, y1+y2, z1+z2)
subV :: Point -> Point -> Point
subV (x1, y1, z1) (x2, y2, z2) = (x1-x2, y1-y2, z1-z2)
invV :: Point -> Point
invV (x, y, z) = (-x, -y, -z)

findS2 :: Scanner -> Scanner -> Maybe (Point, S.Set Point)
findS2 bs1 bs2 = checkPairs [(b1, b2) | b1 <- bs1, b2 <- bs2] bs1 bs2
  where checkPairs [] _ _ = Nothing
        checkPairs ((b1, b2):pairs) bs1 bs2 = let s2 = b1 `subV` b2 in
                                                ((s2,).S.insert b1 <$> findCommon [(b1', b2') | b1' <- L.delete b1 bs1, b2' <- L.delete b2 bs2] s2 S.empty) <|> checkPairs pairs bs1 bs2
        findCommon [] _ _ = Nothing
        findCommon ((b1, b2):pairs) s2 found
          | S.size found == 11 = Just found
          | b1 `subV` b2 == s2 = findCommon pairs s2 (S.insert b1 found)
          | otherwise          = findCommon pairs s2 found

class Transform t where
  apply :: t -> Point -> Point

data Rot = Rot0 | Rot90 | Rot180 | Rot270 deriving (Show, Eq)
instance Transform Rot where
  apply Rot0 (x, y, z) = (x, y, z)
  apply Rot90 (x, y, z) = (x, -z, y)
  apply Rot180 (x, y, z) = (x, -y, -z)
  apply Rot270 (x, y, z) = (x, z, -y)
data Side = Front | Top | Back | Bottom | LeftS | RightS deriving (Show, Eq)
instance Transform Side where
  apply Front (x, y, z) = (x, y, z)
  apply Top (x, y, z) = (-y, x, z)
  apply Back (x, y, z) = (-x, y, -z)
  apply Bottom (x, y, z) = (y, -x, z)
  apply LeftS (x, y, z) = (z, y, -x)
  apply RightS (x, y, z) = (-z, y, x)
instance (Transform t1, Transform t2) => Transform (t1, t2) where
  apply (t1, t2) = apply t1 . apply t2
instance (Transform t) => Transform [t] where
  apply [] p = p
  apply (t:ts) p = apply t . apply ts $ p

transformations :: [(Rot, Side)]
transformations = [(rot, side) | rot <- [Rot0, Rot90, Rot180, Rot270], side <- [Front, Top, Back, Bottom, LeftS, RightS]]

findCommon :: Scanner -> Scanner -> Maybe (Point, S.Set Point, (Rot, Side))
findCommon bs1 bs2 = searchTransformations bs1 bs2 transformations
  where searchTransformations _ _ [] = Nothing
        searchTransformations bs1 bs2 (t:ts) = ((\(p, f) -> (p, f, t)) <$> findS2 bs1 (apply t <$> bs2)) <|> searchTransformations bs1 bs2 ts

data ScannerData = ScannerData { queue :: [Int], visited :: S.Set Int, prev :: M.Map Int (Int, Point, (Rot, Side)) } deriving Show

findPoints :: [Scanner] -> State ScannerData ()
findPoints scanners = do (ScannerData queue visited prev) <- get

                         if L.null queue then return ()
                         else do let i = head queue
                                 let curr = scanners !! i

                                 if S.member i visited then return ()
                                 else do let neighs = neighbours i prev
                                         let prevEntries = M.fromList ((\(n, Just (p, _, t)) -> (n, (i, p, t))) <$> L.filter (isJust.snd) ((\n -> (n, findCommon curr (scanners !! n))) <$> neighs))
                                         put (ScannerData (tail queue ++ keys prevEntries) (S.insert i visited) (M.union prev prevEntries))
                                         findPoints scanners
  where neighbours i prev = [j | j <- [1..length scanners-1], i /= j, M.notMember j prev]


-- GOING FROM s TO t
-- CALC SCANNER RELATIVE TO PREVIOUS POINT BY APPLYING CUMULATIVE TRANSFORMATION
-- ADD PREVIOUS POINT TO SCANNER TO GET RELATIVE TO 0
-- APPLY SCANNER TRANSFORMATION TO POINTS
-- APPLY CUMULATIVE TRANSFORMATION TO POINTS
-- ADD SCANNER TO TRANSFORMED POINTS TO GET RELATIVE TO 0
-- EXAMPLE:
-- scannerAbs = cumulativeTransform.apply(scannerRel) + prevPoint
-- points.map(p -> scannerTransform.apply(p))
--       .map(p -> cumulativeTransform.apply(p))
--       .map(p -> scannerAbs + p)

getCumulativeTransform :: Int -> M.Map Int (Int, Point, (Rot, Side)) -> [(Rot, Side)]
getCumulativeTransform 0 _ = [(Rot0, Front)]
getCumulativeTransform i prev = let (prevI, _, scannerTransform) = prev M.! i
                                    prevTransform = getCumulativeTransform prevI prev in
                                  prevTransform ++ [scannerTransform]

getAbsolutePosition :: Int -> M.Map Int (Int, Point, (Rot, Side)) -> Point
getAbsolutePosition 0 _ = (0, 0, 0)
getAbsolutePosition i prev = let (prevI, scannerRel, scannerTransform) = prev M.! i
                                 prevPoint = getAbsolutePosition prevI prev
                                 cumulativeTransform = getCumulativeTransform prevI prev in
                              prevPoint `addV` apply cumulativeTransform scannerRel

getAbsolutePositions :: Int -> [Point] -> M.Map Int (Int, Point, (Rot, Side)) -> [Point]
getAbsolutePositions 0 ps _ = ps
getAbsolutePositions i ps prev = let (prevI, scannerRel, scannerTransform) = prev M.! i
                                     scannerAbs = getAbsolutePosition i prev
                                     cumulativeTransform = getCumulativeTransform prevI prev in
                                  addV scannerAbs . apply cumulativeTransform . apply scannerTransform <$> ps

part1 :: String -> Int
part1 input = let scanners = ((\p -> read ("(" ++ p ++ ")") :: (Int, Int, Int)) <$>).tail.lines <$> splitRegex (mkRegex "\n\n") input
                  (ScannerData _ _ prev) = execState (findPoints scanners) (ScannerData [0] S.empty M.empty) in
                S.size $ S.unions $ (\i -> S.fromList (getAbsolutePositions i (scanners !! i) prev)) <$> [0..length scanners-1]

part2 :: String -> Int
part2 input = let scanners = ((\p -> read ("(" ++ p ++ ")") :: (Int, Int, Int)) <$>).tail.lines <$> splitRegex (mkRegex "\n\n") input
                  (ScannerData _ _ prev) = execState (findPoints scanners) (ScannerData [0] S.empty M.empty) in
                let ps = (\i -> getAbsolutePosition i prev) <$> [0..length scanners-1] in maximum [abs (x1-x2) + abs (y1-y2) + abs (z1-z2) | (x1, y1, z1) <- ps, (x2, y2, z2) <- ps]
