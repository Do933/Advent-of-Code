module Day15 (day15) where

import Data.Char
import Data.List as L
import Data.Maybe
import Data.Map as M (Map, fromList, toList, lookup, keys, insert, empty, member, (!), unionWith, keysSet)
import Data.Set as S (Set, empty, insert, notMember, member, map, findMin, difference, delete, singleton, union, fromList)
import Control.Monad.State.Lazy
import Util

import Debug.Trace

day15 :: String -> Int
day15 = part2

part1 :: String -> Int
part1 input = let grid = ((digitToInt <$>) <$> lines input)
                  (w, h) = (length (head grid), length grid)
                  ps = M.fromList [((x, y), (grid !! y) !! x) | x <- [0..w-1], y <- [0..h-1]]
                  ds = M.fromList [((x, y), if (x, y) == (0, 0) then 0 else maxBound :: Int) | x <- [0..w-1], y <- [0..h-1]]
                  pq = M.keys ps in
                let (ds', _, _, prev) = dijkstra ps ds pq S.empty w h (w-1, h-1) M.empty in fromJust (M.lookup (w-1, h-1) ds')
  where dijkstra ps ds pq vs w h t prev
          | getMin pq ds == t = (ds, pq, vs, prev)
          | otherwise = let curr = getMin pq ds in
                          let (ds', pq', vs', prev') = loopNeighbours ps ds (L.delete curr pq) (S.insert curr vs) prev curr (filter (`S.notMember` vs) (ns curr w h)) in
                            dijkstra ps ds' pq' vs' w h t prev'
        path prev ps = if M.member (head ps) prev then path prev (fromJust (M.lookup (head ps) prev):ps) else ps
        loopNeighbours ps ds pq vs prev curr [] = (ds, pq, vs, prev)
        loopNeighbours ps ds pq vs prev curr (n:ns) = let newDist = fromJust (M.lookup curr ds) + fromJust (M.lookup n ps) in
                                                        if newDist < fromJust (M.lookup n ds) then
                                                          loopNeighbours ps (M.insert n newDist ds) pq vs (M.insert n curr prev) curr ns
                                                        else
                                                          loopNeighbours ps ds pq vs prev curr ns
        ns (x, y) w h = filter (\(i, j) -> i >= 0 && j >= 0 && i < w && j < h) [(x-1, y), (x+1, y), (x, y-1), (x, y+1)]
        getMin pq ds = minimumBy (\p q -> compare (fromJust (M.lookup p ds)) (fromJust (M.lookup q ds))) pq

data Dijkstra = Dijkstra { distances :: M.Map (Int, Int) Int, visited :: Set (Int, Int), queue :: Set (Int, Int) } deriving Show

part2 :: String -> Int
part2 input = let grid = ((digitToInt <$>) <$> lines input)
                  (w, h) = (length (head grid), length grid)
                  ps = M.fromList [((x, y), let v = (grid !! (y `mod` h)) !! (x `mod` h) + (x `div` w) + (y `div` h) in if v >= 10 then v - 9 else v) | x <- [0..w*5-1], y <- [0..h*5-1]]
                  ds = M.fromList [((x, y), if (x, y) == (0, 0) then 0 else maxBound :: Int) | x <- [0..w*5-1], y <- [0..h*5-1]] in
                let res = execState (dijkstra ps (w*5) (h*5) (w*5-1, h*5-1)) (Dijkstra ds S.empty (S.singleton (0, 0))) in distances res M.! (w*5-1, h*5-1)
  where dijkstra :: M.Map (Int, Int) Int -> Int -> Int -> (Int, Int) -> State Dijkstra ()
        dijkstra ps w h t = do (Dijkstra ds vs queue) <- get
                               let (currDis, curr) = S.findMin (S.map (\v -> (ds M.! v, v)) queue)

                               if curr == t then return ()
                               else do let neighs = ns curr w h
                                       let neighDists = (\n -> (n, currDis + ps M.! n)) <$> ns curr w h
                                       let ds' = M.unionWith min ds (M.fromList neighDists)
                                       put (Dijkstra ds' (S.insert curr vs) (S.difference (S.union (S.fromList neighs) (S.delete curr queue)) vs))
                                       dijkstra ps w h t
        ns (x, y) w h = filter (\(i, j) -> i >= 0 && j >= 0 && i < w && j < h) [(x-1, y), (x+1, y), (x, y-1), (x, y+1)]
