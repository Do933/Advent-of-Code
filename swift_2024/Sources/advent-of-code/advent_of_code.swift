import Foundation

func day1() throws {

    let file = URL(fileURLWithPath: "input.txt")

    let content = try String(contentsOf: file, encoding: .utf8)
    // let content = """
    //     two1nine
    //     eightwothree
    //     abcone2threexyz
    //     xtwone3four
    //     4nineeightseven2
    //     zoneight234
    //     7pqrstsixteen
    //     """

    func toNumeric(digit: String) -> Int? {
        return switch digit {
            case "0", "zero": 0
            case "1", "one": 1
            case "2", "two": 2
            case "3", "three": 3
            case "4", "four": 4
            case "5", "five": 5
            case "6", "six": 6
            case "7", "seven": 7
            case "8", "eight": 8
            case "9", "nine": 9
            default: nil
        }
    }
    
    var total = 0
    let regex = /(?=([0-9]|zero|one|two|three|four|five|six|seven|eight|nine))/
    content.split(separator: "\n").forEach { line in
        // let firstDigit = line.firstMatch(of: regex)
        // let lastDigit = line.lastMatch(of: regex)
        // total += (Int(firstDigit.asciiValue!) - 48) * 10 + (Int(lastDigit.asciiValue!) - 48)
        let matches = line.matches(of: regex)
        let firstMatch = toNumeric(digit: String(matches.first!.output.1))!
        let lastMatch = toNumeric(digit: String(matches.last!.output.1))!
        total += firstMatch * 10 + lastMatch
    }

    print(total)


}