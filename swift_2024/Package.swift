// swift-tools-version: 6.0
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "advent-of-code",
    products: [
        // Products define the executables and libraries a package produces, making them visible to other packages.
        .library(
            name: "advent-of-code",
            targets: ["advent-of-code"]),
    ],
    targets: [
        // Targets are the basic building blocks of a package, defining a module or a test suite.
        // Targets can depend on other targets in this package and products from dependencies.
        .target(
            name: "advent-of-code"),
        .testTarget(
            name: "advent-of-codeTests",
            dependencies: ["advent-of-code"]
        ),
    ]
)
