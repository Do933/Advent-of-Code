import scala.collection.mutable

object Day06:

    @main
    def day06(): Unit =
        println(AoCRunner.runFile("2023/06/input", solve))

    def solveOneLine(input: String): Result =
        ((times: List[String], distances: List[String], waysToWin: ((Long, Long)) => Long) =>
            Result(times.map(_.toLong).zip(distances.map(_.toLong)).map(waysToWin).product, waysToWin((times.mkString.toLong, distances.mkString.toLong))))
            (input.split("\n")(0).drop(5).trim.split("\\s+").toList, input.split("\n")(1).drop(9).trim.split("\\s+").toList,
                (r: (Long, Long)) => (0L to r._1).find(t => t * (r._1 - t) > r._2).map(t => r._1+1 - t*2).getOrElse(0L))

    def solve(input: String): Result =
        val races = input.split("\n")(0).drop(5).trim.split("\\s+").map(_.toLong)
            .zip(input.split("\n")(1).drop(9).trim.split("\\s+").map(_.toLong))
            .map { case (t, d) => Race(t, d) }
            .toList

        val bigRace = Race(input.split("\n")(0).drop(5).replaceAll("\\s+", "").toLong,
            input.split("\n")(1).drop(9).replaceAll("\\s+", "").toLong)

        Result(races.map(_.waysToWin).product, bigRace.waysToWin)

    case class Race(time: Long, distance: Long):
        def waysToWin: Long = (0L to time).find(t => t * (time - t) > distance).map(t => time+1 - t*2).getOrElse(0L)