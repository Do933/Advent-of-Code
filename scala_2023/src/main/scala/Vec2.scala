import math.Numeric.Implicits.infixNumericOps

type Vec2i = Vec2[Int]
type Vec2l = Vec2[Long]
type Vec2bi = Vec2[BigInt]
type Vec2d = Vec2[Double]
type Vec2bd = Vec2[BigDecimal]

case class Vec2[N](x: N, y: N)(using num: Numeric[N]):
    def +(other: Vec2[N]): Vec2[N] = Vec2(x + other.x, y + other.y)
    def -(other: Vec2[N]): Vec2[N] = Vec2(x - other.x, y - other.y)

    def *(s: N): Vec2[N] = Vec2(num.times(x, s), num.times(y, s))
    
    def dot(other: Vec2[N]): N = num.times(x, other.x) + num.times(y, other.y)

    override def toString: String = s"($x, $y)"

object Vec2:
    extension [N](s: N)(using num: Numeric[N])
        def *(v: Vec2[N]): Vec2[N] = Vec2(num.times(s, v.x), num.times(s, v.y))


given [N : Numeric]: Conversion[(N, N), Vec2[N]] with
    override def apply(t: (N, N)): Vec2[N] = Vec2(t._1, t._2)
