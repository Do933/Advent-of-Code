import Day07.Card.*
import Day07.HandType
import Day07.HandType.*

import scala.collection.mutable

object Day08:

    @main
    def day08(): Unit =
        println(AoCRunner.runFile("2023/08/input", solve))

    def solve(input: String): Result =
        val dirs = input.split("\n")(0).map(c => if c == 'L' then Dir.Left else Dir.Right).toList
        val tree = input.linesIterator.drop(2).map(l => l.split(" = "))
            .map(ps => ps(0) -> ps(1))
            .map { case (n, ns) => n -> ns.drop(1).dropRight(1).split(", ") }
            .map { case (n, ns) => n -> (ns(0), ns(1)) }
            .toMap

        var part1 = 0
        if tree.contains("AAA") then
            var current = "AAA"
            while current != "ZZZ" do
                if dirs(part1 % dirs.size) == Dir.Left then
                    current = tree(current)._1
                else current = tree(current)._2
                part1 += 1

        Result(part1, tree.keySet.filter(_.endsWith("A")).map(n => findCycle(n, dirs, tree)._2.toLong).foldRight(1L)((f, acc) => lcm(f, acc)))

    def findCycle(start: String, dirs: List[Dir], tree: Map[String, (String, String)]): (Int, Int) =
        case class State(node: String, dir: Int)
        val seen = mutable.Map[State, Int]()

        var current = State(start, 0)
        var steps = 0
        while !seen.contains(current) || !current.node.endsWith("Z") do
            seen.put(current, steps)
            if dirs(current.dir) == Dir.Left then
                current = State(tree(current.node)._1, (steps+1) % dirs.size)
            else current = State(tree(current.node)._2, (steps+1) % dirs.size)
            steps += 1

        val offset = seen(current)
        val freq = steps - offset

        (offset, freq)

    def lcm(a: Long, b: Long): Long = (a * b) / gcd(a, b)
    def gcd(a: Long, b: Long): Long = if b == 0 then a else gcd(b, a % b)

    enum Dir:
        case Left, Right