
import Day12.Spring.*

import scala.Iterator.iterate
import scala.annotation.targetName
import scala.collection.mutable

object Day15:

    @main
    def day15(): Unit =
        println(AoCRunner.runFile("2023/15/input", solve))

    def solve(input: String): Result =
        val sequence = input.split(",").toList

        val boxes = new Array[List[(String, Int)]](256)
        for i <- 0 until 256 do
            boxes(i) = Nil

        for step <- sequence do
            if step.contains("=") then
                val key = step.split("=")(0)
                val value = step.split("=")(1).toInt
                val i = hash(key)
                if boxes(i).exists(_._1 == key) then
                    boxes(i) = boxes(i).map { case e@(k, _) => if k == key then (key, value) else e }
                else
                    boxes(i) = (key, value) :: boxes(i)
            else if step.endsWith("-") then
                val key = step.dropRight(1)
                val i = hash(key)
                boxes(i) = boxes(i).filter(_._1 != key)

        Result(sequence.map(hash).sum, boxes.zipWithIndex.map { case (b, i) => b.reverse.zipWithIndex.map { case ((k, v), j) => (i+1) * (j+1) * v }.sum }.sum)

    def hash(s: String): Int =
        s.zipWithIndex.map { case (c, i) => (c * modExp(17, (s.length - i), 256)) % 256 }.sum % 256

    def modExp(b: Int, e: Int, m: Int): Int =
        if m == 1 then return 0
        var c = 1
        for _ <- 0 until e do
            c = (c * b) % m
        c

