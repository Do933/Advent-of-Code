import math.Numeric.Implicits.infixNumericOps

type Vec3i = Vec3[Int]
type Vec3l = Vec3[Long]
type Vec3d = Vec3[Double]
type Vec3bi = Vec3[BigInt]

case class Vec3[N](x: N, y: N, z: N)(using num: Numeric[N]):
    def +(other: Vec3[N]): Vec3[N] = Vec3(x + other.x, y + other.y, z + other.z)
    def -(other: Vec3[N]): Vec3[N] = Vec3(x - other.x, y - other.y, z - other.z)

    def xy: Vec2[N] = Vec2(x, y)
    def xz: Vec2[N] = Vec2(x, z)
    def yx: Vec2[N] = Vec2(y, x)
    def yz: Vec2[N] = Vec2(y, z)
    def zx: Vec2[N] = Vec2(z, x)
    def zy: Vec2[N] = Vec2(z, y)

    def lenSq: N = num.times(x, x) + num.times(y, y) + num.times(z, z)
    def len: Double = math.sqrt(num.toDouble(lenSq))

    def unit: Vec3d = Vec3(num.toDouble(x) / len, num.toDouble(y) / len, num.toDouble(z) / len)

    def *(s: N): Vec3[N] = Vec3(num.times(x, s), num.times(y, s), num.times(z, s))

    def cross(other: Vec3[N]): Vec3[N] = Vec3(
        num.times(this.y, other.z) - num.times(this.z, other.y),
        num.times(this.z, other.x) - num.times(this.x, other.z),
        num.times(this.x, other.y) - num.times(this.y, other.x)
    )

    def parallel(other: Vec3[N]): Boolean = (this cross other) == Vec3.zero[N]

    def toVec3d: Vec3d = Vec3(num.toDouble(x), num.toDouble(y), num.toDouble(z))

    override def toString: String = s"($x, $y, $z)"

object Vec3:
    def zero[N](using num: Numeric[N]) : Vec3[N] = Vec3(num.zero, num.zero, num.zero)
    extension [N](s: N)(using num: Numeric[N])
        def *(v: Vec3[N]): Vec3[N] = Vec3(num.times(s, v.x), num.times(s, v.y), num.times(s, v.z))

extension (v: Vec3l)
    def toVec3bi: Vec3bi = Vec3(BigInt(v.x), BigInt(v.y), BigInt(v.z))

given [N : Numeric]: Conversion[(N, N, N), Vec3[N]] with
    override def apply(t: (N, N, N)): Vec3[N] = Vec3(t._1, t._2, t._3)