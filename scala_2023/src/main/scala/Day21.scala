
import scala.Iterator.iterate
import scala.annotation.targetName
import scala.collection.mutable

object Day21:

    @main
    def day21(): Unit =
        println(AoCRunner.runFile("2023/21/input", solve))

    def solve(input: String): Result =
        val garden = input.linesIterator.zipWithIndex.flatMap:
            case (l, y) => l.zipWithIndex.flatMap:
                case (c, x) => if c == '.' || c == 'S' then Set(Vec2(x, y)) else Set()
        .toSet
        val start = input.linesIterator.zipWithIndex.flatMap:
            case (l, y) => l.zipWithIndex.flatMap:
                case (c, x) => if c == 'S' then List(Vec2(x, y)) else List()
        .toList.head

//        val size = input.linesIterator.size
//
//        val steps = 26501365
//        val reachable = bfs(start, v => garden.contains(Vec2(v.x mod size, v.y mod size)), s => s.steps == 1000 || math.abs(s.pos.x /! size) > 2 || math.abs(s.pos.y /! size) > 2, true)
//            .filter(s => math.abs(s.pos.x /! size) <= 2 && math.abs(s.pos.y /! size) <= 2)
//            .filter(s => s.steps % 2 == steps % 2)
//            .groupBy(s => s.pos)
//            .map(e => e._1 -> e._2.map(_.steps).min)
//
//        val reachableEven = reachable.filter:
//            case (v, _) => v.x /! size == 0 && v.y /! size == 0
//        val reachableOdd = reachable.filter:
//            case (v, _) => v.x /! size == 1 && v.y /! size == 0
//        println(reachableEven.size)
//        println(reachableOdd.size)
//
//        val r = 2
//        val nOdd = 1 + ((r/2)*(r/2+1)/2)*8
//        val nEven = 4 + (1+((r+1)/2)*((r+1)/2+1)/2)*8 // TODO fix this garbage
//        // 0 1 2 3  4  5
//        // 0 4 4 16 16 36
//
//        println(nEven)

        // 584211380134282 too low

        val part1 = bfs(start, garden.contains, _.steps == 64, false).count(_.steps == 64)

        /*
         * Part 2:
         */

        val steps = 26501365
        // Assumption: steps are odd
        require(steps % 2 == 1)

        // Assumption: grid is square
        require(input.linesIterator.size == input.lineList.head.length)
        val size = input.linesIterator.size

        // Assumption: steps is size/2 mod size
        require((steps mod size) == size/2)

        // Assumption: start is in the middle
        require(start == Vec2(size/2, size/2))

        // Assumption: all straight lines are free from the start
        require((0 until size).flatMap(i => Set(Vec2(start.x, i), Vec2(i, start.y))).forall(garden.contains))

        // Assumption: a diamond shape as big as the grid is free
        require((0 until size/2).flatMap(i => Set(Vec2(start.x-i, i), Vec2(start.x+i, i), Vec2(i, start.y+i), Vec2(size-i-1, start.y+i))).forall(garden.contains))

        val reach: Long = steps / size - 1
        println(s"Reach: $reach")

        val nOdd = 1 + (reach/2) * (reach/2 + 1) * 4
        println(s"Number of odd grids: $nOdd")
        val nEven = ((reach-1)/2 + 1) * ((reach-1)/2 + 1) * 4
        println(s"Number of even grids: $nEven")

        val distances = bfs(start, v => garden.contains(Vec2(v.x mod size, v.y mod size)), s => s.steps > size * 3 || math.abs(s.pos.x /! size) > 2 || math.abs(s.pos.y /! size) > 2, true)
            .filter(s => s.steps % 2 == 1)
            .groupBy(s => s.pos)
            .map(e => e._1 -> e._2.map(_.steps).min)
        val rOdd = distances.keySet.filter(v => Vec2(v.x /! size, v.y /! size) == Vec2(0, 0))
        println(s"Number of reachable points in odd grids: ${rOdd.size}")
        val rEven = distances.keySet.filter(v => Vec2(v.x /! size, v.y /! size) == Vec2(1, 0)).map(v => Vec2(v.x % size, v.y))
        println(s"Number of reachable points in even grids: ${rEven.size}")

        // Assumption: even points are disjoint from odd points
        require((rOdd intersect rEven).isEmpty)

        val fullyReached = rOdd.size * nOdd + rEven.size * nEven
        println(s"Number of points reached in fully reached grids: $fullyReached")

        val topGrid = bfs(Vec2(start.x, size-1), garden.contains, _.steps == size - 1, false).count(_.steps == size-1)
        println(s"Number of points reached in top grid: $topGrid")
        val leftGrid = bfs(Vec2(size-1, start.y), garden.contains, _.steps == size - 1, false).count(_.steps == size-1)
        println(s"Number of points reached in top grid: $leftGrid")
        val rightGrid = bfs(Vec2(0, start.y), garden.contains, _.steps == size - 1, false).count(_.steps == size-1)
        println(s"Number of points reached in top grid: $rightGrid")
        val bottomGrid = bfs(Vec2(start.x, 0), garden.contains, _.steps == size - 1, false).count(_.steps == size-1)
        println(s"Number of points reached in top grid: $bottomGrid")
        val reachedOuter = topGrid + leftGrid + rightGrid + bottomGrid
        println(s"Number of points reached in outermost grids: $reachedOuter")

        val numberSmall = reach + 1
        println(s"Number of small grids reached: $numberSmall")
        val topRightSmall = bfs(Vec2(0, size-1), garden.contains, _.steps == size/2 - 1, false).count(_.steps == size/2 - 1) * numberSmall
        println(s"Number of points reached in top right small grid: $topRightSmall")
        val bottomRightSmall = bfs(Vec2(0, 0), garden.contains, _.steps == size/2 - 1, false).count(_.steps == size/2 - 1) * numberSmall
        println(s"Number of points reached in top right small grid: $bottomRightSmall")
        val topLeftSmall = bfs(Vec2(size-1, size-1), garden.contains, _.steps == size/2 - 1, false).count(_.steps == size/2 - 1) * numberSmall
        println(s"Number of points reached in top right small grid: $topLeftSmall")
        val bottomLeftSmall = bfs(Vec2(size-1, 0), garden.contains, _.steps == size/2 - 1, false).count(_.steps == size/2 - 1) * numberSmall
        println(s"Number of points reached in top right small grid: $bottomLeftSmall")
        val reachedSmall = topRightSmall + bottomRightSmall + topLeftSmall + bottomLeftSmall
        println(s"Number of points reached in small grids: $reachedSmall")

        val numberLarge = reach
        println(s"Number of large grids reached: $numberLarge")
        val topRightLarge = bfs(Vec2(0, size - 1), garden.contains, _.steps == size * 3 / 2 - 1, false).count(_.steps == size * 3 / 2 - 1) * numberLarge
        println(s"Number of points reached in top right large grid: $topRightLarge")
        val bottomRightLarge = bfs(Vec2(0, 0), garden.contains, _.steps == size * 3 / 2 - 1, false).count(_.steps == size * 3 / 2 - 1) * numberLarge
        println(s"Number of points reached in top right large grid: $bottomRightLarge")
        val topLeftLarge = bfs(Vec2(size - 1, size - 1), garden.contains, _.steps == size * 3 / 2 - 1, false).count(_.steps == size * 3 / 2 - 1) * numberLarge
        println(s"Number of points reached in top right large grid: $topLeftLarge")
        val bottomLeftLarge = bfs(Vec2(size - 1, 0), garden.contains, _.steps == size * 3 / 2 - 1, false).count(_.steps == size * 3 / 2 - 1) * numberLarge
        println(s"Number of points reached in top right large grid: $bottomLeftLarge")
        val reachedLarge = topRightLarge + bottomRightLarge + topLeftLarge + bottomLeftLarge
        println(s"Number of points reached in large grids: $reachedLarge")

        val part2 = fullyReached + reachedOuter + reachedSmall + reachedLarge

        Result(part1, part2)

    extension (x: Int)
        def /!(y: Int): Int = if x < 0 then (x - y + 1) / y else x / y

    case class State(pos: Vec2i, steps: Int)
    def bfs(start: Vec2i, valid: Vec2i => Boolean, stop: State => Boolean, min: Boolean): Set[State] =
        val queue = mutable.Queue[State]()
        queue.enqueue(State(start, 0))

        val seen = mutable.Set[State]()
        val dist = mutable.Map[Vec2i, Int]()

        while queue.nonEmpty do
            val current = queue.dequeue()
            if current.pos.x % 131 == 83 && current.pos.y % 131 == 3 then
                println(current)

            if !seen.contains(current) then
                seen.add(current)
                if current.steps < dist.getOrElse(current.pos, Int.MaxValue) then
                    dist.put(current.pos, current.steps)
                if !stop(current) then
                    Set(Vec2(0, 1), Vec2(1, 0), Vec2(-1, 0), Vec2(0, -1))
                        .map(v => current.pos + v)
                        .filter(valid)
                        .filter(n => !min || current.steps + 1 < dist.getOrElse(n, Int.MaxValue))
                        .foreach(n => queue.enqueue(State(n, current.steps + 1)))

        seen.toSet
