import math.max

object Day02:

    @main
    def day02(): Unit =
        println(AoCRunner.runFile("2023/02/test", solve))

    def solveOneLine(input: String): Result =
        ((games: List[(Int, Int, Int)]) => Result(games.indices.filter(i => games(i)._1 <= 12 && games(i)._2 <= 13 && games(i)._3 <= 14).map(_ + 1).sum, games.map { case (r, g, b) => r * g * b }.sum))
            (input.linesIterator.map(l => l.split(": ")(1).split("; ")
                .map(s => s.split(", ").map(c => c.split(" ")).map(p => (p(0).toInt, p(1)))
                    .map { case (n, c) => Map("red" -> (n, 0, 0), "green" -> (0, n, 0), "blue" -> (0, 0, n))(c) }
                    .foldRight((0, 0, 0)) { case ((lr, lg, lb), (rr, rg, rb)) => (max(lr, rr), max(lg, rg), max(lb, rb)) })
                .foldRight((0, 0, 0)) { case ((r, g, b), (mr, mg, mb)) => (max(r, mr), max(g, mg), max(b, mb)) })
                .toList)

    def solve(input: String): Result =
        val games = input.linesIterator.map(read[Game]).toList
        val max = (12, 13, 14)
        val possible = games.filter(g => !g.subsets.exists(s => s.red > max._1 || s.green > max._2 || s.blue > max._3))
        val part1 = possible.map(_.number).sum

        val maxs = games.map(g => (g.subsets.map(_.red).max, g.subsets.map(_.green).max, g.subsets.map(_.blue).max))
        val powers = maxs.map { case (r, g, b) => r * g * b }
        val part2 = powers.sum

        Result(part1, part2)

    given Read[Game] with
        override def read(s: String): Game =
            val parts = s.split(": ")
            val number = parts(0).substring("Game ".length).toInt
            val subsets = parts(1).split("; ").map(parse[Subset]).toSet
            Game(number, subsets)

    given Read[Subset] with
        override def read(s: String): Subset =
            val parts = s.split(", ").map(c => c.split(" ")).map(p => (p(0).toInt, p(1)))
            val red = parts.find { case (_, c) => c == "red" }.map { case (n, _) => n }.getOrElse(0)
            val green = parts.find { case (_, c) => c == "green" }.map { case (n, _) => n }.getOrElse(0)
            val blue = parts.find { case (_, c) => c == "blue" }.map { case (n, _) => n }.getOrElse(0)
            Subset(red, green, blue)

    case class Subset(red: Int, green: Int, blue: Int):
        override def toString(): String = s"$red red, $green green, $blue blue"
    case class Game(number: Int, subsets: Set[Subset]):
        override def toString(): String = s"Game $number: ${subsets.mkString("; ")}"