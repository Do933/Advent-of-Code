object Day01:

    @main
    def day01(): Unit =
        println(AoCRunner.runFileSplit("2023/01/input", part1, part2))

    def part1(input: String): Int =
        input.linesIterator.map(l => (l.find(_.isDigit).get.toInt - 48) * 10 + l.findLast(_.isDigit).get.toInt - 48 )
            .sum

    def part2(input: String): Int =
        input.linesIterator
            .map(l => ((matches: List[Int]) => matches.head * 10 + matches.last)
                (l.indices.map(i => List("one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9").find(d => l.substring(i).startsWith(d)))
                    .flatMap(opt => opt.toList)
                    .map(c => Map("one" -> 1, "two" -> 2, "three" -> 3, "four" -> 4, "five" -> 5, "six" -> 6, "seven" -> 7, "eight" -> 8, "nine" -> 9).getOrElse(c, c.toInt))
                    .toList))
            .sum

    def findDigits(string: String): List[Int] =
        val digits = List("one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9")
        string.indices.map(i => digits.find(d => string.substring(i).startsWith(d)))
            .flatMap(opt => opt.toList)
            .map(toDigit)
            .toList

    def toDigit(s: String): Int = s match {
        case "0" | "zero" => 0
        case "1" | "one" => 1
        case "2" | "two" => 2
        case "3" | "three" => 3
        case "4" | "four" => 4
        case "5" | "five" => 5
        case "6" | "six" => 6
        case "7" | "seven" => 7
        case "8" | "eight" => 8
        case "9" | "nine" => 9
    }

