import java.util.stream.Collectors
import scala.:+
import scala.annotation.targetName
import scala.collection.mutable

object Y22_Day18:

    @main
    def y22_day18(): Unit =
        println(AoCRunner.runFileSplit("2022/18/input", part1, part2))

    def part1(input: String): Int =
        val droplets = input.linesIterator.map(_.split(",")).map(parts => Droplet(parts(0).toInt, parts(1).toInt, parts(2).toInt)).toSet

        droplets.toList
            .map(d => d.sides.count(d2 => !droplets.contains(d2)))
            .sum

    def part2(input: String): Int =
        val droplets = input.linesIterator.map(_.split(",")).map(parts => Droplet(parts(0).toInt, parts(1).toInt, parts(2).toInt)).toSet
        val min = (droplets.map(_.x).min, droplets.map(_.y).min, droplets.map(_.z).min)
        val max = (droplets.map(_.x).max, droplets.map(_.y).max, droplets.map(_.z).max)

        val external = mutable.Set[Droplet]()
        val internal = mutable.Set[Droplet]()
        def isExternal(droplet: Droplet): Boolean =
            val queue = mutable.Queue[Droplet](droplet)
            val visited = mutable.Set[Droplet]()

            while queue.nonEmpty do
                val current = queue.dequeue()

                visited.add(current)

                if current.x < min._1 || current.y < min._2 || current.z < min._3 ||
                    current.x > max._1 || current.y > max._2 || current.z > max._3 ||
                    external.contains(current) then
                    external.addAll(visited)
                    return true

                if internal.contains(current) then
                    internal.addAll(visited)
                    return false

                current.sides.filter(!droplets.contains(_)).filter(!visited.contains(_)).filter(!queue.contains(_)).foreach(queue.enqueue)

            internal.addAll(visited)
            false

        droplets.toList
            .flatMap(d => d.sides.filter(!droplets.contains(_)))
            .count(isExternal)

    case class Droplet(x: Int, y: Int, z: Int):
        def sides: List[Droplet] = List(Droplet(x + 1, y, z), Droplet(x - 1, y, z), Droplet(x, y + 1, z), Droplet(x, y - 1, z), Droplet(x, y, z + 1), Droplet(x, y, z - 1))