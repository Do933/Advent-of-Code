import Day07.HandType.*
import Day07.Card.*
import Day07.HandType

import scala.collection.mutable

object Day07:

    @main
    def day07(): Unit =
        println(AoCRunner.runFile("2023/07/input", solve))

    def solve(input: String): Result =
        val bids = input.linesIterator.map(l => Bid(Hand(l.split(" ")(0).map(c => read[Card](c.toString)).toList), l.split(" ")(1).toLong)).toList

        Result(bids.sortBy(_.hand).zipWithIndex.map { case (b, i) => b.value * (i+1) }.sum,
            bids.map(b => Bid(Hand(b.hand.cards.map(c => if c == Jack then Joker else c)), b.value)).sortBy(_.hand).zipWithIndex.map { case (b, i) => b.value * (i+1) }.sum)

    case class Bid(hand: Hand, value: Long)

    case class Hand(cards: List[Card]):
        val handType: HandType =
            val jokers = cards.count(_ == Joker)
            val noJokers = cards.filter(_ != Joker)
            handType(noJokers) match
                case FiveOfAKind => FiveOfAKind
                case FourOfAKind if jokers == 1 => FiveOfAKind
                case FourOfAKind => FourOfAKind
                case FullHouse => FullHouse
                case ThreeOfAKind if jokers == 2 => FiveOfAKind
                case ThreeOfAKind if jokers == 1 => FourOfAKind
                case ThreeOfAKind => ThreeOfAKind
                case TwoPairs if jokers == 1 => FullHouse
                case TwoPairs => TwoPairs
                case OnePair if jokers == 3 => FiveOfAKind
                case OnePair if jokers == 2 => FourOfAKind
                case OnePair if jokers == 1 => ThreeOfAKind
                case OnePair => OnePair
                case HighCard if jokers == 4 => FiveOfAKind
                case HighCard if jokers == 3 => FourOfAKind
                case HighCard if jokers == 2 => ThreeOfAKind
                case HighCard if jokers == 1 => OnePair
                case HighCard => HighCard
        def handType(cs: List[Card]): HandType =
            if cs.count(_ == cs.head) == cs.size then FiveOfAKind
            else if cs.exists(c => cs.count(_ == c) == 4) then FourOfAKind
            else if cs.exists(c => cs.count(_ == c) == 3) && cs.exists(c => cs.count(_ == c) == 2) then FullHouse
            else if cs.exists(c => cs.count(_ == c) == 3) then ThreeOfAKind
            else if cs.count(c => cs.count(_ == c) == 2) == 4 then TwoPairs
            else if cs.exists(c => cs.count(_ == c) == 2) then OnePair
            else HighCard
    given Ordering[Hand] = Ordering.by[Hand, HandType](_.handType).orElseBy[List[Card]](_.cards)
    given Ordering[List[Card]] with
        def compare(l: List[Card], r: List[Card]): Int =  (l, r) match
            case (Nil, Nil) => 0
            case (x :: xs, y :: ys) if x == y => compare(xs, ys)
            case (x :: _, y :: _) => summon[Ordering[Card]].compare(x, y)

    enum HandType:
        case HighCard, OnePair, TwoPairs, ThreeOfAKind, FullHouse, FourOfAKind, FiveOfAKind
    given Ordering[HandType] = Ordering.by(_.ordinal)

    enum Card:
        case Joker, Two, Three, Four, Five, Six, Seven, Eight, Nine, T, Jack, Queen, King, Ace
    given Ordering[Card] = Ordering.by(_.ordinal)
    given Read[Card] with
        def read(s: String): Card = s match
            case "2" => Two
            case "3" => Three
            case "4" => Four
            case "5" => Five
            case "6" => Six
            case "7" => Seven
            case "8" => Eight
            case "9" => Nine
            case "T" => T
            case "J" => Jack
            case "Q" => Queen
            case "K" => King
            case "A" => Ace
