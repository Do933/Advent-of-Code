import java.util.stream.Collectors
import scala.:+
import scala.annotation.{tailrec, targetName}
import scala.collection.mutable
import scala.math.{ceil, exp, max}

object Y22_Day21:

    @main
    def y22_day21(): Unit =
        println(AoCRunner.runFile("2022/21/input", solve))

    def solve(input: String): Result =
        val monkeys = input.linesIterator.map(read[Monkey]).toList

        val Add(l, r) = expand(monkeys.find(_.name == "root").get.job, monkeys.map(m => if m.name == "humn" then Monkey("humn", Var("x")) else m)) : @unchecked
        val value = if containsVar(l) then eval(r) else eval(l)
        val equation = if containsVar(l) then l else r

        Result(eval(Var("root"), monkeys, mutable.Map()), solve(equation, value))

    @tailrec
    def solve(equation: Job, value: Long): Long = equation match {
        case Var(_) => value
        case Yell(_) => throw IllegalArgumentException()
        case Add(l, r) => if containsVar(l) then solve(l, value - eval(r)) else solve(r, value - eval(l))
        case Sub(l, r) => if containsVar(l) then solve(l, value + eval(r)) else solve(r, eval(l) - value)
        case Mul(l, r) => if containsVar(l) then solve(l, value / eval(r)) else solve(r, value / eval(l))
        case Div(l, r) => if containsVar(l) then solve(l, value * eval(r)) else solve(r, eval(l) / value)
        case Eq(_, _) => throw IllegalArgumentException()
    }

    def expand(job: Job, monkeys: List[Monkey]): Job = job match {
        case Yell(_) => job
        case Var(name) => monkeys.find(_.name == name).map(_.job).map(expand(_, monkeys)).getOrElse(job)
        case Add(l, r) => Add(expand(l, monkeys), expand(r, monkeys))
        case Sub(l, r) => Sub(expand(l, monkeys), expand(r, monkeys))
        case Mul(l, r) => Mul(expand(l, monkeys), expand(r, monkeys))
        case Div(l, r) => Div(expand(l, monkeys), expand(r, monkeys))
        case Eq(l, r) => Eq(expand(l, monkeys), expand(r, monkeys))
    }

    def containsVar(job: Job): Boolean = job match {
        case Yell(_) => false
        case Var(_) => true
        case Add(l, r) => containsVar(l) || containsVar(r)
        case Sub(l, r) => containsVar(l) || containsVar(r)
        case Mul(l, r) => containsVar(l) || containsVar(r)
        case Div(l, r) => containsVar(l) || containsVar(r)
        case Eq(l, r) => containsVar(l) || containsVar(r)
    }

    def eval(job: Job): Long = eval(job, List(), mutable.Map())
    def eval(job: Job, monkeys: List[Monkey], env: mutable.Map[String, Long]): Long =
        job match {
            case Yell(num) => num
            case Add(l, r) => eval(l, monkeys, env) + eval(r, monkeys, env)
            case Sub(l, r) => eval(l, monkeys, env) - eval(r, monkeys, env)
            case Mul(l, r) => eval(l, monkeys, env) * eval(r, monkeys, env)
            case Div(l, r) => eval(l, monkeys, env) / eval(r, monkeys, env)
            case Var(name) => env.getOrElse(name, {
                val res = eval(monkeys.find(_.name == name).get.job, monkeys, env)
                env.put(name, res)
                res
            })
            case Eq(_, _) => throw IllegalArgumentException()
        }

    given Read[Monkey] with
        def read(s: String): Monkey =
            Monkey(s.split(": ")(0), parse(s.split(": ")(1)))

    given Read[Job] with
        def read(s: String): Job =
            if s.contains("+") then Add(Var(s.split(" \\+ ")(0)), Var(s.split(" \\+ ")(1)))
            else if s.contains("-") then Sub(Var(s.split(" - ")(0)), Var(s.split(" - ")(1)))
            else if s.contains("*") then Mul(Var(s.split(" \\* ")(0)), Var(s.split(" \\* ")(1)))
            else if s.contains("/") then Div(Var(s.split(" / ")(0)), Var(s.split(" / ")(1)))
            else Yell(s.toLong)

    case class Monkey(name: String, job: Job)

    sealed trait Job
    case class Yell(number: Long) extends Job
    case class Var(name: String) extends Job
    case class Add(left: Job, right: Job) extends Job
    case class Sub(left: Job, right: Job) extends Job
    case class Mul(left: Job, right: Job) extends Job
    case class Div(left: Job, right: Job) extends Job
    case class Eq(left: Job, right: Job) extends Job
