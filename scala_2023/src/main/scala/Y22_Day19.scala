import java.util.stream.Collectors
import scala.:+
import scala.annotation.targetName
import scala.collection.mutable
import math.{ceil, max}

object Y22_Day19:

    @main
    def y22_day19(): Unit =
        println(AoCRunner.runFileSplit("2022/19/input", part1, part2))

    def part1(input: String): Int =
        val blueprints = input.linesIterator
            .map(l => l.split(": ")(1).split("\\. "))
            .map(p => (p(0), p(1), p(2), p(3).init))
            .map { case (ore, clay, obs, geo) => Blueprint(
                """(\d+) ore""".r.findAllIn(ore).matchData.map(_.group(1).toInt).toList.head,
                """(\d+) ore""".r.findAllIn(clay).matchData.map(_.group(1).toInt).toList.head,
                """(\d+) ore and (\d+) clay""".r.findAllIn(obs).matchData.map(m => (m.group(1).toInt, m.group(2).toInt)).toList.head,
                """(\d+) ore and (\d+) obsidian""".r.findAllIn(geo).matchData.map(m => (m.group(1).toInt, m.group(2).toInt)).toList.head
            ) }.toList

        blueprints.indices.map(i => (i + 1) * maxGeodes(blueprints(i), 24)).sum

    def part2(input: String): Int =
        val blueprints = input.linesIterator
            .map(l => l.split(": ")(1).split("\\. "))
            .map(p => (p(0), p(1), p(2), p(3).init))
            .map { case (ore, clay, obs, geo) => Blueprint(
                """(\d+) ore""".r.findAllIn(ore).matchData.map(_.group(1).toInt).toList.head,
                """(\d+) ore""".r.findAllIn(clay).matchData.map(_.group(1).toInt).toList.head,
                """(\d+) ore and (\d+) clay""".r.findAllIn(obs).matchData.map(m => (m.group(1).toInt, m.group(2).toInt)).toList.head,
                """(\d+) ore and (\d+) obsidian""".r.findAllIn(geo).matchData.map(m => (m.group(1).toInt, m.group(2).toInt)).toList.head
            )
            }.toList

        blueprints.take(3).map(maxGeodes(_, 32)).product

    def maxGeodes(blueprint: Blueprint, maxTime: Int): Int =
        case class State(oreProduction: Int, clayProduction: Int, obsidianProduction: Int, geodeProduction: Int,
                         ore: Int, clay: Int, obsidian: Int, geode: Int,
                         time: Int):
            val potential: Int = geode + (geodeProduction + time) * time
            def isValid: Boolean = time >= 0
            def buyOre: State =
                val t = ceil(max(0, blueprint.oreRobot - ore) / oreProduction.toDouble).toInt + 1
                State(oreProduction + 1, clayProduction, obsidianProduction, geodeProduction,
                    ore - blueprint.oreRobot + oreProduction * t, clay + clayProduction * t, obsidian + obsidianProduction * t, geode + geodeProduction * t,
                    time - t)
            def buyClay: State =
                val t = ceil(max(0, blueprint.clayRobot - ore) / oreProduction.toDouble).toInt + 1
                State(oreProduction, clayProduction + 1, obsidianProduction, geodeProduction,
                    ore - blueprint.clayRobot + oreProduction * t, clay + clayProduction * t, obsidian + obsidianProduction * t, geode + geodeProduction * t,
                    time - t)
            def buyObsidian: State =
                val t1 = ceil(max(0, blueprint.obsidianRobot._1 - ore) / oreProduction.toDouble).toInt + 1
                val t2 = ceil(max(0, blueprint.obsidianRobot._2 - clay) / clayProduction.toDouble).toInt + 1
                val t = max(t1, t2)
                State(oreProduction, clayProduction, obsidianProduction + 1, geodeProduction,
                    ore - blueprint.obsidianRobot._1 + oreProduction * t, clay - blueprint.obsidianRobot._2 + clayProduction * t, obsidian + obsidianProduction * t, geode + geodeProduction * t,
                    time - t)
            def buyGeode: State =
                val t1 = ceil(max(0, blueprint.geodeRobot._1 - ore) / oreProduction.toDouble).toInt + 1
                val t2 = ceil(max(0, blueprint.geodeRobot._2 - obsidian) / obsidianProduction.toDouble).toInt + 1
                val t = max(t1, t2)
                State(oreProduction, clayProduction, obsidianProduction, geodeProduction + 1,
                    ore - blueprint.geodeRobot._1 + oreProduction * t, clay + clayProduction * t, obsidian - blueprint.geodeRobot._2 + obsidianProduction * t, geode + geodeProduction * t,
                    time - t)
            def buyNothing: State =
                State(oreProduction, clayProduction, obsidianProduction, geodeProduction,
                    ore + oreProduction * time, clay + clayProduction * time, obsidian + obsidianProduction * time, geode + geodeProduction * time,
                    0)

        val startingState = State(1, 0, 0, 0, 0, 0, 0, 0, maxTime)
        val queue = mutable.Stack[State](startingState)

        var maxState = startingState

        def enqueue(state: State): Unit =
            if state.isValid && state.potential > maxState.geode then queue.push(state)

        while queue.nonEmpty do
            val current = queue.pop()

            if current.potential > maxState.geode then
                if current.time == 0 then
                    if current.geode > maxState.geode then
                        maxState = current

                else if current.time > 0 then
                    enqueue(current.buyNothing)
                    enqueue(current.buyOre)
                    enqueue(current.buyClay)
                    if current.clayProduction > 0 then
                        enqueue(current.buyObsidian)
                    if current.obsidianProduction > 0 then
                        enqueue(current.buyGeode)


        maxState.geode

    case class Blueprint(oreRobot: Int, clayRobot: Int, obsidianRobot: (Int, Int), geodeRobot: (Int, Int))