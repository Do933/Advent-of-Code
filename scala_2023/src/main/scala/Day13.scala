
import Day12.Spring.*

import scala.Iterator.iterate
import scala.annotation.targetName
import scala.collection.mutable

object Day13:

    @main
    def day13(): Unit =
        println(AoCRunner.runFile("2023/13/input", solve))

    def solve(input: String): Result =
        val patterns = input.split("\n\n").map(p => Pattern(p.lineList)).toList

        val part1 = patterns.map(p => p.findReflection(lines => lines.forall { case (l, r) => l == r })).sumOf {
            case (Line.Row, i) => i * 100
            case (Line.Column, i) => i
        }

        val part2 = patterns.map(p => p.findReflection(lines =>
            lines.zipWithIndex.find { case ((l, r), _) => l.zip(r).count { case (lc, rc) => lc != rc } == 1 }.map(_._2).exists(i =>
                lines.indices.filter(_ != i).map(lines(_)).forall { case (l, r) => l == r }
            )
        )).sumOf {
            case (Line.Row, i) => i * 100
            case (Line.Column, i) => i
        }

        Result(part1, part2)

    class Pattern(val rows: List[String]):
        val columns: List[String] = rows.head.indices.map(x => rows.map(_(x)).mkString).toList

        def findReflection(compare: List[(String, String)] => Boolean): (Line, Int) =
            val column = columns.indices.drop(1).find(x =>
                val mirrorRows = rows.map(r => (r.take(x).takeRight(r.length - x), r.drop(x).take(x).reverse))
                compare(mirrorRows)
            )
            val row = rows.indices.drop(1).find(y =>
                val mirrorColumns = columns.map(c => (c.take(y).takeRight(c.length - y), c.drop(y).take(y).reverse))
                compare(mirrorColumns)
            )
            column.map((Line.Column, _)).getOrElse((Line.Row, row.get))

    enum Line:
        case Row, Column
