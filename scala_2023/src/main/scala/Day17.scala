
import scala.Iterator.iterate
import scala.annotation.targetName
import scala.collection.mutable

object Day17:

    @main
    def day17(): Unit =
        println(AoCRunner.runFile("2023/17/input", solve))

    def solve(input: String): Result =
        val heatLoss = input.linesIterator.zipWithIndex.flatMap { case (l, y) =>
            l.zipWithIndex.map { case (c, x) => Vec2(x, y) -> c.toString.toInt } }
            .toMap

        val start = Vec2(0, 0)
        val end = Vec2(input.lineList.head.length-1, input.lineList.size-1)

        Result(dijkstra(start, end, heatLoss, 3, 0), dijkstra(start, end, heatLoss, 10, 4))

    def dijkstra(start: Vec2i, end: Vec2i, heatLoss: Map[Vec2i, Int], maxStraight: Int, minTurn: Int): Int =
        val distance = mutable.Map[State, Int]()
        given Ordering[State] with
            override def compare(x: State, y: State): Int =
                -distance.getOrElse(x, Int.MaxValue).compare(distance.getOrElse(y, Int.MaxValue))

        val visited = mutable.Set[State]()
        val queue = mutable.PriorityQueue[State]()

        queue.enqueue(State(start + Vec2(0, 1), 1, Vec2(0, 1)))
        queue.enqueue(State(start + Vec2(1, 0), 1, Vec2(1, 0)))
        distance.put(State(start + Vec2(0, 1), 1, Vec2(0, 1)), heatLoss(start + Vec2(0, 1)))
        distance.put(State(start + Vec2(1, 0), 1, Vec2(1, 0)), heatLoss(start + Vec2(1, 0)))

        while queue.nonEmpty do
            val current = queue.dequeue()

            if visited.contains(current) then
                ()

            else if current.pos == end then
                queue.clear()
                ()

            else
                visited.add(current)
                val neighbours = List(State(current.pos + current.dir.left, 1, current.dir.left),
                    State(current.pos + current.dir.right, 1, current.dir.right),
                    State(current.pos + current.dir, current.straightCount + 1, current.dir))

                neighbours
                    .filter(n => n.straightCount <= maxStraight)
                    .filter(n => n.dir == current.dir || current.straightCount >= minTurn)
                    .filter(n => n.pos != end || n.straightCount >= minTurn)
                    .filter(n => heatLoss.contains(n.pos))
                    .filter(n => distance.getOrElse(n, Int.MaxValue) > distance(current) + heatLoss(n.pos))
                    .foreach(n =>
                        distance.put(n, distance(current) + heatLoss(n.pos))
                        queue.enqueue(n))

        val endState = distance.filter(e => e._1.pos == end).minBy(_._2)._1
        distance(endState)

    case class State(pos: Vec2i, straightCount: Int, dir: Vec2i)
    extension (v: Vec2i)
        def left: Vec2i = Vec2(v.y, -v.x)
        def right: Vec2i = Vec2(-v.y, v.x)