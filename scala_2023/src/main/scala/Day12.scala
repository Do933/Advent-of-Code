
import Day12.Spring.*

import scala.Iterator.iterate
import scala.annotation.targetName
import scala.collection.mutable

object Day12:

    @main
    def day12(): Unit =
        println(AoCRunner.runFile("2023/12/input", solve))

    def solve(input: String): Result =
        val rows = input.lineList.map(read[Row])

        val part1 = rows.sumOf(r => countArrangements(r.springs.dropWhile(_ == Working), r.amounts))

        val unfolded = rows.map(r => Row((1 to 5).toList.foldRight[List[Spring]](Nil)((_, acc) => r.springs ::: (Unknown :: acc)).dropRight(1), (1 to 5).flatMap(_ => r.amounts).toList))
        val part2 = unfolded.sumOf(r => countArrangements(r.springs.dropWhile(_ == Working), r.amounts))

        Result(part1, part2)

    val cache: mutable.Map[String, Long] = mutable.Map()
    def countArrangements(springs: List[Spring], targetAmounts: List[Int]): Long =
        val key = springs.map(_.symbol).mkString + " | " + targetAmounts.mkString(",")
        //        println
        //        println(s"Looking for ${targetAmounts.mkString(",")} in ${springs.map(_.symbol).mkString}")
        if cache.contains(key) then
        //            println(s"In cache: ${cache(key)}")
            cache(key)
        else if amounts(springs.map(s => if s == Unknown then Working else s)) == targetAmounts then
            //            println("Matches exactly")
            cache.put(key, 1)
            1
        else if targetAmounts.isEmpty then
        //            println("No more targets")
            if springs.contains(Damaged) then
                //                println("Still damaged left, no solutions")
                cache.put(key, 0)
                0
            else
                //                println("Only solution: all ? = .")
                cache.put(key, 1)
                1
        else if springs.isEmpty then
            //            println("No more springs, no solutions")
            cache.put(key, 0)
            0
        else
            val groups = findGroups(springs, targetAmounts.head)
            //            println(s"Groups: ${groups.map(g => s"${g.map(springs(_)).map(_.symbol).mkString} (${g.start}-${g.end})")}")
            val result = groups.sumOf(g => countArrangements(springs.drop(g.end+1), targetAmounts.tail))
            cache.put(key, result)
            result

    def findGroups(springs: List[Spring], size: Int): List[Range] =
        springs.indices
            .takeWhile(i => i == 0 || springs(i-1) != Damaged)
            .filter(i => i + size <= springs.size)
            .filter(i => i+size == springs.size || springs(i+size) != Damaged)
            .map(i => Range(i, i + size))
            .filter(r => r.forall(i => springs(i) != Working))
            .toList

    def amounts(springs: List[Spring]): List[Int] =
        springs.map(s => if s == Working then "." else "#").mkString.split("\\.+").map(_.length).filter(_ > 0).toList

    given Read[Row] with
        override def read(s: String): Row =
            val parts = s.split(" ")
            Row(parts(0).map(c => parse[Spring](c.toString)).toList, parts(1).ints(","))

    given Read[Spring] with
        override def read(s: String): Spring = s match
            case "#" => Damaged
            case "." => Working
            case "?" => Unknown

    case class Row(springs: List[Spring], amounts: List[Int]):
        override def toString: String = springs.map(_.symbol).mkString + " " + amounts.mkString(",")
    enum Spring:
        case Working, Damaged, Unknown
        def symbol: String = this match
            case Working => "."
            case Damaged => "#"
            case Unknown => "?"
