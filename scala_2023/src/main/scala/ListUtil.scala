import scala.annotation.targetName

extension [T](l: List[T])

    def sumOf[N: Numeric](f: T => N): N = l.map(f).sum

    def withNext: List[(T, T)] = l.zip(l.drop(1))
    def withNext[S](f: (T, T) => S): List[S] = l.zip(l.drop(1)).map(f.tupled)
    def withPrev: List[(T, T)] = l.drop(1).zip(l)
    def withPrev[S](f: (T, T) => S): List[S] = l.drop(1).zip(l).map(f.tupled)

    @targetName("fmap")
    def |>[S](f: T => S): List[S] = l.map(f)