import scala.io.Source

object AoCRunner:
    def runFile(file: String, f: (String) => Result): Result =
        (timed("Part 1 and 2", f))(Source.fromResource(file).getLines.mkString("\n"))
    def runFilePartial(file: String, f: (String) => Any): Result =
        Result((timed("Part 1", f))(Source.fromResource(file).getLines.mkString("\n")))
    def runFileSplit(file: String, f1: (String) => Any, f2: (String) => Any): Result =
        val text = Source.fromResource(file).getLines.mkString("\n")
        Result((timed("Part 1", f1))(text), (timed("Part 2", f2))(text))
    def runFileSeq[T](file: String, f1: (String) => T, f2: (T) => Any): Result =
        val part1 = (timed("Part 1" , f1))(Source.fromResource(file).getLines.mkString("\n"))
        Result(part1, (timed("Part 2", f2))(part1))

    private def timed[A, B](name: String, f: (A) => B): (A) => B =
        x =>
            val startTime = System.nanoTime()
            val result = f(x)
            val endTime = System.nanoTime()
            println(s"$name took: ${((endTime - startTime) / 1E6).toInt}ms (${((endTime - startTime) / 1E9).toInt} seconds)\n")
            result

case class Result(part1: Any, part2: Option[Any]):
    override def toString: String =
        s"""Part 1:
           |$part1
           |
           |Part 2:
           |${part2 match
                case None => ""
                case Some(x) => x.toString}
           |""".stripMargin

object Result:
    def apply(part1: Any): Result = Result(part1, None)
    def apply[T](part1: T, part2: T): Result = Result(part1, Some(part2))
