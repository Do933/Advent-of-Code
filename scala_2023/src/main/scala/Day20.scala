
import scala.Iterator.iterate
import scala.annotation.targetName
import scala.collection.mutable

object Day20:

    @main
    def day20(): Unit =
        println(AoCRunner.runFile("2023/20/input", solve))

    def solve(input: String): Result =
        val (circuit, state) = read[(Circuit, CircuitState)](input)

        var st = state
        var tl = 0L
        var th = 0L

        for _ <- 1 to 1000 do
            val (newSt, l, h) = pushButton(circuit, st, _ => ())
            st = newSt
            tl += l
            th += h

        var inputs = List("gl", "mr", "bb", "kk")
        var periods = List[Long]()
        var i = 0L
        st = state
        while
            i += 1
            st = pushButton(circuit, st, s =>
                if inputs.contains(s.from) && s.value then
                    inputs = inputs.filter(_ != s.from)
                    periods = i :: periods
            )._1
            inputs.nonEmpty
        do ()

        Result(tl * th, periods.product)

    def pushButton(circuit: Circuit, state: CircuitState, process: Signal => Unit): (CircuitState, Long, Long) =
        var st = state
        var signals = circuit.modules("broadcaster").outputs.map(Signal(low, "broadcaster", _))

        var totalLow = 1L
        var totalHigh = 0L

        while
            totalLow += signals.count(!_.value)
            totalHigh += signals.count(_.value)
            signals.foreach(process)

            val (newSt, newSigs) = processSignals(signals, circuit, st)
            st = newSt
            signals = newSigs

            signals.nonEmpty
        do ()
        totalLow += signals.count(!_.value)
        totalHigh += signals.count(_.value)

        (st, totalLow, totalHigh)

    def processSignals(signals: List[Signal], circuit: Circuit, state: CircuitState): (CircuitState, List[Signal]) =
        signals.foldLeft((state, List[Signal]())) { case ((cs, sigs), sig) =>
            val (newSt, newSigs) = processSignal(sig, circuit, cs)
            (newSt, sigs ::: newSigs)
        }

    def processSignal(signal: Signal, circuit: Circuit, state: CircuitState): (CircuitState, List[Signal]) =
        val outputs = circuit.modules(signal.to).outputs
        val s = state.states(signal.to)

        s match
            case NoState => (state, outputs.map(Signal(signal.value, signal.to, _)))
            case FFState(o) =>
                if signal.value then (state, Nil)
                else (CircuitState(state.states.map(e => e._1 -> (if e._1 == signal.to then FFState(!o) else e._2))), outputs.map(Signal(!o, signal.to, _)))
            case ConjState(inp) =>
                val newInp = inp.map(e => e._1 -> (if e._1 == signal.from then signal.value else e._2))
                (CircuitState(state.states.map(e => e._1 -> (if e._1 == signal.to then ConjState(newInp) else e._2))), outputs.map(Signal(newInp.values.exists(_ == low), signal.to, _)))

    val low: Boolean = false
    val high: Boolean = true

    case class Signal(value: Boolean, from: String, to: String):
        override def toString: String = s"$from -${if value then "h" else "l"}-> $to"

    given Read[(Circuit, CircuitState)] with
        override def read(s: String): (Circuit, CircuitState) =
            val lines = s.lineList
            val modules = mutable.Map[String, Module]()
            val states = mutable.Map[String, State]()

            for line <- lines do
                val id = line.split(" -> ")(0)
                val outputs = line.split(" -> ")(1).split(", ").toList
                if id == "broadcaster" then modules.put(id, Broadcaster(outputs))
                else if id.startsWith("%") then modules.put(id.drop(1), FlipFlop(outputs))
                else if id.startsWith("&") then modules.put(id.drop(1), Conjunction(outputs))

            modules.values.flatMap(_.outputs).filter(!modules.contains(_)).foreach(modules.put(_, Untyped))

            for (name, module) <- modules do
                module match
                    case FlipFlop(_) => states.put(name, FFState(low))
                    case Conjunction(_) => states.put(name, ConjState(modules.filter(_._2.outputs.contains(name)).map(_._1 -> low).toMap))
                    case _ => states.put(name, NoState)

            (Circuit(modules.toMap), CircuitState(states.toMap))

    sealed trait Module:
        def outputs: List[String]
    case class Broadcaster(outputs: List[String]) extends Module
    case class FlipFlop(outputs: List[String]) extends Module
    case class Conjunction(outputs: List[String]) extends Module
    case object Untyped extends Module:
        override def outputs: List[String] = Nil

    case class Circuit(modules: Map[String, Module])

    sealed trait State
    case object NoState extends State
    case class FFState(on: Boolean) extends State
    case class ConjState(inp: Map[String, Boolean]) extends State

    case class CircuitState(states: Map[String, State])
//        def initial: Boolean =
//            states.values.forall(s => s match
//                case NoState => true
//                case FFState(o) => !o
//                case ConjState(inp) => inp.values.forall(!_)
//            )