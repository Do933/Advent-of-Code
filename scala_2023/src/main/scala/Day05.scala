import scala.collection.mutable

object Day05:

    @main
    def day05(): Unit =
        println(AoCRunner.runFileSplit("2023/05/input", part1, part2))

    def solveOneLine(input: String): Result =
        ((seeds: List[Long], conversions: List[List[(Long, Long, Long)]]) =>
            Result(conversions.foldLeft(seeds)((ss, ms) => ss.map(s => ms.find(m => s >= m._2 && s < m._2 + m._3).map(m => s + m._1 - m._2).getOrElse(s))).min,
                ((map: ((Long, Long, Long), (Long, Long, Boolean)) => List[(Long, Long, Boolean)]) =>
                    conversions.foldLeft(seeds.indices.map(_ * 2).take(seeds.length / 2).map(i => (seeds(i), seeds(i) + seeds(i + 1), false)).toList)
                        ((rs, ms) => ms.foldLeft(rs)((rs, c) => rs.flatMap(map(c, _))).map(r => (r._1, r._2, false))).map(_._1).min)
                    ((m: (Long, Long, Long), r: (Long, Long, Boolean)) =>
                        if r._3 then List(r)
                        else if r._1 >= m._2 && r._1 < m._2 + m._3 && r._2-1 >= m._2 && r._2-1 < m._2 + m._3 then List((r._1 + m._1 - m._2, r._2 + m._1 - m._2, true))
                        else if r._1 >= m._2 && r._1 < m._2 + m._3 then List((r._1 + m._1 - m._2, m._1 + m._3, true), (m._2 + m._3, r._2, false))
                        else if r._2-1 >= m._2 && r._2-1 < m._2 + m._3 then List((r._1, m._2, false), (m._1, r._2 + m._1 - m._2, true))
                        else if r._1 < m._2 && r._2 >= m._2 + m._3 then List((r._1, m._2, false), (m._1, m._1 + m._3, true), (m._2 + m._3, r._2, false))
                        else List(r))))
            (input.takeWhile(_ != '\n').drop(7).split(" ").map(_.toLong).toList,
                input.split("\n\n").drop(1).map(p => p.linesIterator.drop(1).map(l => l.split(" ").map(_.toLong)).map(ps => (ps(0), ps(1), ps(2))).toList).toList)

    def part1(input: String): Long =
        val parts = input.split("\n\n")
        val seeds = parts(0).drop("seeds: ".length).split(" ").map(_.toLong).toList
        val conversions = parts.drop(1)
            .map(p => Conversion(p.linesIterator.drop(1).map(l => l.split(" ").map(_.toLong)).map(ps => Mapping(ps(0), ps(1), ps(2))).toList))
            .toList

        conversions.foldLeft(seeds)((s, c) => s.map(c(_))).min

    def part2(input: String): Long =
        val parts = input.split("\n\n")
        val seeds = parts(0).drop("seeds: ".length).split(" ").map(_.toLong).toList
        val conversions = parts.drop(1)
            .map(p => Conversion(p.linesIterator.drop(1).map(l => l.split(" ").map(_.toLong)).map(ps => Mapping(ps(0), ps(1), ps(2))).toList))
            .toList

        val seedRanges = seeds.indices.map(_ * 2).takeWhile(_ < seeds.length).map(i => LRange(seeds(i), seeds(i)+seeds(i+1), false)).toList

        var ranges = seedRanges
        for c <- conversions do
            ranges = c(ranges)

        ranges.map(_.start).min

    case class Conversion(mappings: List[Mapping]):
        def apply(x: Long): Long = mappings.find(_.contains(x)).map(_(x)).getOrElse(x)
        def apply(r: LRange): List[LRange] = mappings.foldLeft(List(r))((rs, c) => rs.flatMap(c(_)))
        def apply(ranges: List[LRange]): List[LRange] =
            mappings.foldLeft(ranges)((rs, c) => rs.flatMap(c(_))).map(r => LRange(r.start, r.end, false))

    case class Mapping(to: Long, from: Long, length: Long):
        def contains(x: Long): Boolean = x >= from && x < from + length
        def apply(x: Long): Long = x + (to - from)
        def apply(r: LRange): List[LRange] =
            if r.mapped then List(r)
            else if contains(r.start) && contains(r.end-1) then List(LRange(this(r.start), this(r.end), true))
            else if contains(r.start) then List(LRange(this(r.start), this(from + length), true), LRange(from + length, r.end, false))
            else if contains(r.end-1) then List(LRange(r.start, from, false), LRange(this(from), this(r.end), true))
            else if r.start < from && r.end >= from + length then List(LRange(r.start, from, false), LRange(to, to + length, true), LRange(from + length, r.end, false))
            else List(r)

    case class LRange(start: Long, end: Long, mapped: Boolean)