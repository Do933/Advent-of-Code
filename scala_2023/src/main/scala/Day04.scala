import scala.collection.mutable

object Day04:

    @main
    def day04(): Unit =
        println(AoCRunner.runFile("2023/04/input", solve))

    def solveOneLine(input: String): Result =
        ((cards: List[(Set[Int], Set[Int])]) => Result(
            cards.map(c => (c._1 intersect c._2).size).map(i => if i == 0 then 0 else 1 << (i - 1)).sum,
            ((amounts: Map[Int, Int]) => cards.indices.map(amounts(_)).sum)
                (cards.indices.foldLeft(cards.indices.map(_ -> 1).toMap)((amounts, i) => (1 to (cards(i)._1 intersect cards(i)._2).size).foldRight(amounts)((j, acc) => acc + ((i+j) -> (amounts(i+j) + amounts(i))))))))
            (input.linesIterator.map(l => l.split(": ")(1).split(" \\| "))
                .map(ps => (ps(0).trim.split(" +").map(_.toInt).toSet, ps(1).trim.split(" +").map(_.toInt).toSet))
                .toList)

    def solve(input: String): Result =
        val cards = input.linesIterator.map(l => l.split(": ")(1).split(" \\| "))
            .map(ps => Card(ps(0).trim.split(" +").map(_.toInt).toSet, ps(1).trim.split(" +").map(_.toInt).toSet))
            .toList

        val part1 = cards.map(c => (c.winning intersect c.numbers).size).map(i => if i == 0 then 0 else 1 << (i-1)).sum

        val amounts = cards.map(c => c -> 1).to(mutable.Map)
        for i <- cards.indices do
            val card = cards(i)
            val amtWon = (card.winning intersect card.numbers).size
            for c <- cards.drop(i+1).take(amtWon) do
                amounts.put(c, amounts(c) + amounts(card))
        val part2 = cards.map(amounts(_)).sum

        Result(part1, part2)

    case class Card(winning: Set[Int], numbers: Set[Int])