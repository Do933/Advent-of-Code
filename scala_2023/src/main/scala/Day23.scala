import scala.Iterator.iterate
import scala.annotation.targetName
import scala.collection.mutable

object Day23:

    @main
    def day23(): Unit =
        println(AoCRunner.runFile("2023/23/test", solve))

    def solve(input: String): Result =
        val map = input.lineList.zipWithIndex.flatMap { case (l, y) => l.zipWithIndex.map { case (c, x) => Vec2(x, y) -> c } }.toMap
        val size = input.linesIterator.size

        val start = map.keySet.find(v => v.y == 0 && map(v) == '.').get
        val end = map.keySet.find(v => v.y == size-1 && map(v) == '.').get

        val next = mutable.Map[Vec2i, Map[Vec2i, Int]]()
        case class State(pos: Vec2i, steps: Int, prev: Vec2i, orig: Vec2i, totalSteps: Int)

        val stack = mutable.Stack(State(start, 0, Vec2(-1, -1), start, 0))
        var part1 = 0
        while stack.nonEmpty do
            val current = stack.pop()

            val neighbours = (map(current.pos) match
                case '.' => Set(Vec2(0, 1), Vec2(1, 0), Vec2(-1, 0), Vec2(0, -1))
                case '>' => Set(Vec2(1, 0))
                case '<' => Set(Vec2(-1, 0))
                case 'v' => Set(Vec2(0, 1))
                case '^' => Set(Vec2(0, -1)))
                    .map(current.pos + _)
                    .filter(map.contains)
                    .filter(map(_) != '#')
                    .filter(_ != current.prev)

            if current.pos == end then
                part1 = math.max(part1, current.totalSteps)

            if neighbours.size == 1 then
                neighbours.foreach(n => stack.push(State(n, current.steps + 1, current.pos, current.orig, current.totalSteps + 1)))
            else
                next.put(current.orig, next.getOrElse(current.orig, Map()) + (current.pos -> current.steps))
                next.put(current.pos, next.getOrElse(current.pos, Map()) + (current.orig -> current.steps))
                neighbours.foreach(n => stack.push(State(n, 1, current.pos, current.pos, current.totalSteps + 1)))

        case class State2(pos: Vec2i, steps: Int, visited: Set[Vec2i])
        val queue = mutable.Queue(State2(start, 0, Set()))
        var part2 = 0
        while queue.nonEmpty do
            val current = queue.dequeue()

            val neighbours = next(current.pos).keySet.filter(!current.visited.contains(_))

            if current.pos == end then
                part2 = math.max(current.steps, part2)
            else
                neighbours.foreach(n => queue.enqueue(State2(n, current.steps + next(current.pos)(n), current.visited + current.pos)))

        Result(part1, part2)