import java.util.stream.Collectors
import scala.:+
import scala.collection.mutable

object Y22_Day16:

    @main
    def y22_day16(): Unit =
        println(AoCRunner.runFile("2022/16/input", solve2))

    def solve2(input: String): Result =
        val valves = parse(input).map(v => v.name -> v).toMap

        val graph = Graph[Valve]()
        valves.values.foreach(n => graph.add(n))
        val nameToNode = graph.nodes.map(n => n.value.name -> n).toMap
        graph.nodes.foreach(n => n.value.neighbours.foreach(v => graph.connect(n, nameToNode(v.name), 1)))

        val smallGraph = removeAllNodes(graph, v => v.flowRate == 0 && v.name != "AA")
        val ds = bellmanFord(smallGraph)

        val start = smallGraph.nodes.find(n => n.value.name == "AA").get

        val part1 = solve2(start, 0, 0, 30, smallGraph.nodes.filter(n => n.value.name != "AA").toSet, ds)

        var part2 = 0
        val nodes = smallGraph.nodes.filter(n => n.value.name != "AA").toList
        for i <- 0 until (1 << (nodes.size - 1)) do
            val you = nodes.indices.filter(j => ((1 << j) & i) != 0).map(j => nodes(j)).toSet
            val elephant = nodes.indices.filter(j => ((1 << j) & i) == 0).map(j => nodes(j)).toSet
            if you.size > 6 && elephant.size > 6 then
                val score = solve2(start, 0, 0, 26, you, ds) + solve2(start, 0, 0, 26, elephant, ds)
                if score > part2 then part2 = score

        Result(part1, part2)

    def solve2(current: Node[Valve], currentFlow: Int, currentValue: Int, timeLeft: Int, closedValves: Set[Node[Valve]], distances: Map[(Node[Valve], Node[Valve]), Int]): Int =
        val newFlow = currentFlow + current.value.flowRate

        if closedValves.isEmpty then return currentValue + newFlow * timeLeft
        if timeLeft == 0 then return currentValue

        val possibleDestinations = closedValves.filter(n => distances(current -> n) + 2 <= timeLeft)
        if possibleDestinations.isEmpty then return currentValue + newFlow * timeLeft

        val result = possibleDestinations.map(dest =>
            val time = distances(current -> dest) + 1
            solve2(dest, newFlow, currentValue + time * newFlow, timeLeft - time, closedValves - dest, distances)).max

        result

    def bellmanFord[T](graph: Graph[T]): Map[(Node[T], Node[T]), Int] =
        def distances(from: Node[T]): Map[Node[T], Int] =
            val distance = mutable.HashMap[Node[T], Int]()
            graph.nodes.foreach(v => distance.put(v, Integer.MAX_VALUE / 4))
            distance.put(from, 0)

            for _ <- 1 until graph.nodes.size do
                graph.edges.foreach { case (u, v, w) =>
                    if distance(u) + w < distance(v) then
                        distance.put(v, distance(u) + w)
                }

            distance.toMap

        graph.nodes.flatMap(from =>
            val distance = distances(from)
            graph.nodes.map(to => (from -> to) -> distance(to))
        ).toMap

    def removeAllNodes[T](graph: Graph[T], doRemove: (T) => Boolean): Graph[T] =
        var current = graph
        while current.nodes.exists(n => doRemove(n.value)) do
            val toRemove = current.nodes.find(n => doRemove(n.value)).get
            current = removeNode(current, toRemove)
        current

    def removeNode[T](graph: Graph[T], toRemove: Node[T]): Graph[T] =
        val result = Graph[T]()

        val oldToNew = mutable.HashMap[Node[T], Node[T]]()
        graph.nodes.filter(_ != toRemove).foreach(node => oldToNew.put(node, result.add(node.value)))

        graph.nodes.filter(_ != toRemove).foreach(oldFrom =>
            oldFrom.neighbours.filter(_ != toRemove).foreach(oldTo =>
                val newFrom = oldToNew(oldFrom)
                val newTo = oldToNew(oldTo)
                result.connect(newFrom, newTo, graph.cost(oldFrom, oldTo))
            ))

        toRemove.neighbours.foreach(oldFrom =>
            toRemove.neighbours.filter(_ != oldFrom).foreach(oldTo =>
                val newFrom = oldToNew(oldFrom)
                val newTo = oldToNew(oldTo)
                result.connect(newFrom, newTo, graph.cost(oldFrom, toRemove) + graph.cost(toRemove, oldTo))
            ))

        result

    def solve(input: String): Int =
        val valves = parse(input).map(v => v.name -> v).toMap
        solve(valves("AA"), mutable.HashSet(), 0, 30, valves("AA"), 0, valves.count { case (_, v) => v.flowRate > 0 })

    def solve(currentValve: Valve, openValves: mutable.HashSet[String], currentScore: Int, timeLeft: Int, comingFrom: Valve, totalFlow: Int, valvesWithFlow: Int): Int =
        if (timeLeft == 0) return currentScore
        if (valvesWithFlow == openValves.size) return currentScore + timeLeft * totalFlow

        val updatedScore = currentScore + totalFlow

        val couldOpenValve = !openValves.contains(currentValve.name) && currentValve.flowRate > 0
        val valueOfOpening = if (couldOpenValve)
            openValves.add(currentValve.name)
            val value = solve(currentValve, openValves, updatedScore, timeLeft-1, currentValve, totalFlow + currentValve.flowRate, valvesWithFlow)
            openValves.remove(currentValve.name)
            value else 0

        val valuesOfMoving = currentValve.neighbours.filter(_.name != comingFrom.name).map(n => solve(n, openValves, updatedScore, timeLeft-1, currentValve, totalFlow, valvesWithFlow))
        val valueOfMoving = if (valuesOfMoving.isEmpty) 0 else valuesOfMoving.max

        Math.max(valueOfOpening, valueOfMoving)

    def parse(input: String): Set[Valve] =
        case class MutableValve(name: String, flowRate: Int, neighboursMut: mutable.Set[MutableValve]) extends Valve {
            override def neighbours: Set[Valve] = neighboursMut.toSet
            override def toString: String = s"$name ($flowRate)"
        }

        val valves: Map[String, MutableValve] = input.linesIterator
            .map(line => line.split("; ")(0).drop(6).split(" has flow rate="))
            .map(parts => MutableValve(parts(0), parts(1).toInt, mutable.HashSet()))
            .map(valve => valve.name -> valve)
            .toMap

        input.linesIterator.map(line =>
            val name = line.split("; ")(0).drop(6).split(" has flow rate=")(0)
            val neighbours = line.split("; ")(1).drop("tunnels lead to valves".length).trim.split(", ")
            (name, neighbours)
            ).foreach { case (name, neighbours) =>
                neighbours.foreach(n => valves(name).neighboursMut.add(valves(n))) }

        valves.values.toSet

    class Graph[T]:
        private val nodesMut = mutable.ArrayBuffer[Node[T]]()
        def nodes: Iterable[Node[T]] = nodesMut

        private val costs = mutable.HashMap[(Int, Int), Int]()
        def edges: Iterable[(Node[T], Node[T], Int)] = costs.map { case ((from, to), value) => (nodesMut(from), nodesMut(to), value) }

        def add(value: T): Node[T] =
            val node = Node(nodesMut.size, value)
            nodesMut.addOne(node)
            node

        def connect(from: Node[T], to: Node[T], value: Int): Unit =
            costs.put((from.index, to.index), value)
            costs.put((to.index, from.index), value)
            from.addNeighbour(to)
            to.addNeighbour(from)

        def cost(from: Node[T], to: Node[T]): Int = costs((from.index, to.index))

        override def toString: String = nodes.flatMap(node =>
            node.neighbours.map(n => s"${node.value} --(${cost(node, n)})-> ${n.value}"))
            .mkString("\n")

    class Node[T](val index: Int, val value: T):
        private val neighboursMut = mutable.Set[Node[T]]()
        def neighbours: Iterable[Node[T]] = neighboursMut

        def addNeighbour(neighbour: Node[T]): Unit =
            neighboursMut.add(neighbour)

        override def toString: String = s"Node($value)"

    trait Valve:
        val name: String
        val flowRate: Int
        def neighbours: Set[Valve]