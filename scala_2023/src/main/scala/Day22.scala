
import scala.Iterator.iterate
import scala.annotation.targetName
import scala.collection.mutable

object Day22:

    @main
    def day22(): Unit =
        println(AoCRunner.runFile("2023/22/input", solve))

    def solveOneLine(input: String): Result =
        ((atLevel: (((Int, Int, Int), (Int, Int, Int)), Int) => ((Int, Int, Int), (Int, Int, Int)), intersect: (((Int, Int, Int), (Int, Int, Int)), ((Int, Int, Int), (Int, Int, Int))) => Boolean) =>
            ((fallen: Set[((Int, Int, Int), (Int, Int, Int))]) =>
                ((supportedBy: Map[((Int, Int, Int), (Int, Int, Int)), Set[((Int, Int, Int), (Int, Int, Int))]]) =>
                    ((cannotDisintegrate: Set[((Int, Int, Int), (Int, Int, Int))]) =>
                        Result(fallen.size - cannotDisintegrate.size, cannotDisintegrate.map(brick =>
                            iterate(Set(brick))(removed => removed union supportedBy.filter(e => (e._2 intersect removed) == e._2 && e._1._1._3 > 1).keySet)
                                .dropWhile(removed => supportedBy.exists(e => e._1._1._3 > 1 && !removed.contains(e._1) && (e._2 intersect removed) == e._2)).drop(1).next.size - 1)
                            .sum))
                        (supportedBy.values.filter(_.size == 1).flatten.toSet))
                    (fallen.map(b => b -> fallen.filter(f => f._2._3 == b._1._3 - 1).filter(f => intersect(f, atLevel(b, f._2._3)))).toMap))
                (input.lineList.map(l => l.split("~").map(p => p.split(",")).map(ps => (ps(0).toInt, ps(1).toInt, ps(2).toInt))).map(vs => (vs(0), vs(1)))
                    .sortBy(_._1._3).foldLeft(Set[((Int, Int, Int), (Int, Int, Int))]())((fallen, brick) =>
                        fallen + atLevel(brick, fallen.filter(f => intersect(f, atLevel(brick, f._2._3))).map(_._2._3 + 1).maxOption.getOrElse(1)))))
            ((c, l) => ((c._1._1, c._1._2, l), (c._2._1, c._2._2, l + (c._2._3 - c._1._3))), (l, r) =>
                !((l._1._1 > r._2._1 || r._1._1 > l._2._1) ||
                    (l._1._2 > r._2._2 || r._1._2 > l._2._2) ||
                    (l._1._3 > r._2._3 || r._1._3 > l._2._3)))

    def solve(input: String): Result =
        val bricks = input.lineList.map(read[Cuboid])

        val fallen = mutable.Set[Cuboid]()
        for brick <- bricks.sortBy(_.start.z) do
            val support = fallen.filter(f => f intersect brick.atLevel(f.end.z)).maxByOption(_.end.z)
            val newBrick = brick.atLevel(support.map(_.end.z + 1).getOrElse(1))
            fallen.add(newBrick)

        val supportedBy = fallen.map(b => b -> fallen.filter(f => f.end.z == b.start.z - 1).filter(f => f intersect b.atLevel(f.end.z))).toMap
        val supports = fallen.map(b => b -> fallen.filter(f => f.start.z == b.end.z + 1).filter(f => b intersect f.atLevel(b.end.z))).toMap

        val cannotDisintegrate = supportedBy.values.filter(_.size == 1).flatten.toSet

        var total = 0
        for brick <- cannotDisintegrate do
            val removed = mutable.Set(brick)
            while
                val toFall = supportedBy.filter(e => (e._2 intersect removed) == e._2 && e._1.start.z > 1).keySet
                removed.addAll(toFall)
                supportedBy.filter(e => e._1.start.z > 1).filter(e => !removed.contains(e._1)).exists(e => (e._2 intersect removed) == e._2)
            do ()
            total += removed.size - 1

        Result(fallen.size - cannotDisintegrate.size, total)

    case class Cuboid(start: Vec3i, end: Vec3i):
        require(start.x <= end.x)
        require(start.y <= end.y)
        require(start.z <= end.z)
        def corners: Set[Vec3i] = Set(
            Vec3(start.x, start.y, start.z),
            Vec3(start.x, start.y, end.z),
            Vec3(start.x, end.y, start.z),
            Vec3(start.x, end.y, end.z),
            Vec3(end.x, start.y, start.z),
            Vec3(end.x, start.y, end.z),
            Vec3(end.x, end.y, start.z),
            Vec3(end.x, end.y, end.z),
        )
        def contains(p: Vec3i): Boolean =
            start.x <= p.x && start.y <= p.y && start.z <= p.z
                && end.x >= p.x && end.y >= p.y && end.z >= p.z
        def intersect(other: Cuboid): Boolean =
            !((this.start.x > other.end.x || other.start.x > this.end.x) ||
                (this.start.y > other.end.y || other.start.y > this.end.y) ||
                (this.start.z > other.end.z || other.start.z > this.end.z))

        def atLevel(level: Int): Cuboid = Cuboid(Vec3(start.x, start.y, level), Vec3(end.x, end.y, level + (end.z - start.z)))

    given Read[Cuboid] with
        override def read(s: String): Cuboid = s.split("~").map(parse[Vec3i]).let(vs => Cuboid(vs(0), vs(1)))

    given Read[Vec3i] with
        override def read(s: String): Vec3i = s.ints(",").let(is => Vec3(is(0), is(1), is(2)))
