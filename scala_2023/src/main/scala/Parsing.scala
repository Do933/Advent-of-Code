trait Read[T]:
    def read(s: String): T

def read[T](s: String)(using r: Read[T]): T = r.read(s)
def parse[T](s: String)(using r: Read[T]): T = r.read(s)

extension (s: String)
    def as[T](using r: Read[T]): T = r.read(s)
    def words: List[String] = s.split("\\s+").toList
    def lineList: List[String] = s.linesIterator.toList
    def mapLines[T](f: String => T): List[T] = s.lineList.map(f)
    def readLines[T: Read]: List[T] = mapLines(read)
    def longs: List[Long] = s.words.map(_.toLong)
    def longs(regex: String): List[Long] = s.split(regex).map(_.toLong).toList
    def ints(regex: String): List[Int] = s.split(regex).map(_.toInt).toList
