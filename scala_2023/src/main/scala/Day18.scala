
import scala.Iterator.iterate
import scala.annotation.targetName
import scala.collection.mutable

object Day18:

    @main
    def day18(): Unit =
        println(AoCRunner.runFile("2023/18/input", solve))

    def solve(input: String): Result =
        type Dir = "U"|"D"|"L"|"R"
        def area(instrs: List[(Dir, Long)]): Long =
            var pos = Vec2(0L, 0L)
            var area = 0L
            var perimeter = 0L
            for (dir, amt) <- instrs do
                perimeter += amt
                dir match
                    case "U" =>
                        area -= amt * pos.x
                        pos = Vec2(pos.x, pos.y - amt)
                    case "D" =>
                        area += amt * pos.x
                        pos = Vec2(pos.x, pos.y + amt)
                    case "L" =>
                        pos = Vec2(pos.x - amt, pos.y)
                    case "R" =>
                        pos = Vec2(pos.x + amt, pos.y)
            area + perimeter / 2 + 1

        val part1 = area(input.lineList.map(l => l.split(" ")).map(l => (l(0).asInstanceOf[Dir], l(1).toInt)))
        val dirs = List[Dir]("R", "D", "L", "U")
        val part2 = area(input.lineList.map(l => l.split(" ")(2).drop(1).dropRight(1)).map(l => (dirs(l.takeRight(1).toInt), Integer.parseInt(l.drop(1).take(5), 16))))

        Result(part1, part2)