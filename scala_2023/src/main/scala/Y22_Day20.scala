import java.util.stream.Collectors
import scala.:+
import scala.annotation.{tailrec, targetName}
import scala.collection.mutable
import scala.math.{ceil, max}

object Y22_Day20:

    @main
    def y22_day20(): Unit =
        println(AoCRunner.runFile("2022/20/input", solve))

    def solve(input: String): Result =
        val numbers = input.linesIterator.map(s => Number(s.toLong)).toList

        val decrypted = numbers.map(n => Number(n.value * 811589153L))
        var part2 = decrypted
        for _ <- 1 to 10 do
            part2 = mix(part2, decrypted)

        Result(coords(mix(numbers, numbers)), coords(part2))

    def mix(numbers: List[Number], order: List[Number]): List[Number] =
        var result = numbers
        for num <- order do
            val index = result.indexOf(num)
            val newIndex = ((index + num.value) mod (numbers.size - 1)).toInt
            result = result.removed(index).inserted(num, newIndex)
        result

    def coords(numbers: List[Number]): Long =
        val zero = numbers.find(_.value == 0).get
        val zeroIndex = numbers.indexOf(zero)
        val n1000 = numbers((zeroIndex + 1000) % numbers.size)
        val n2000 = numbers((zeroIndex + 2000) % numbers.size)
        val n3000 = numbers((zeroIndex + 3000) % numbers.size)
        n1000.value + n2000.value + n3000.value

    class Number(val value: Long):
        override def toString: String = value.toString

    extension[T] (list: List[T])
        def inserted(elem: T, index: Int): List[T] = list.take(index) ::: elem :: list.drop(index)
        def removed(index: Int): List[T] = list.take(index) ::: list.drop(index + 1)