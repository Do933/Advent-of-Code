import scala.Iterator.iterate
import scala.annotation.targetName
import scala.collection.mutable

object Day25:

    @main
    def day25(): Unit =
        println(AoCRunner.runFilePartial("2023/25/input", solve))

    def solve(input: String): Int =
        val connections = mutable.Map[String, Set[String]]()

        for line <- input.linesIterator do
            val from = line.split(": ")(0)
            val to = line.split(": ")(1).split(" ").toSet
            for x <- to do
                connections.put(from, connections.getOrElse(from, Set()) + x)
                connections.put(x, connections.getOrElse(x, Set()) + from)

        val nodes = connections.keySet
        val capacities = connections.flatMap:
            case (from, to) => to.map(t => Edge(from, t) -> 1)
        .toMap

        var res = 0
        while
            val s = nodes.toList((math.random() * nodes.size).toInt)
            val t = nodes.toList((math.random() * nodes.size).toInt)
            val g = FlowGraph(s, t, capacities)
            val (a, b) = g.minCut
            res = a.size * b.size
            a.size == 1 || b.size == 1
        do ()

        res

    class FlowGraph(val s: String, val t: String, val capacities: Map[Edge, Int]):
        val edges: mutable.Set[REdge] = mutable.Set()
        val flows: mutable.Map[Edge, Int] = mutable.Map()

        capacities.foreach:
            case (Edge(a, b), _) =>
                edges.add(REdge(a, b, true))
                edges.add(REdge(b, a, false))
                flows.put(Edge(a, b), 0)

        def residual(e: REdge): Int =
            if e.forwards then capacity(Edge(e.from, e.to)) - flow(Edge(e.from, e.to))
            else flow(Edge(e.to, e.from))

        def flow(e: Edge): Int = flows(e)
        def capacity(e: Edge): Int = capacities(e)

        def minCut: (Set[String], Set[String]) =
            maximiseFlow()
            val visited = mutable.Set[String]()
            val queue = mutable.Queue(s)
            while queue.nonEmpty do
                val current = queue.dequeue()
                if !visited.contains(current) then
                    visited.add(current)
                    val neighbours = edges.filter(_.from == current).filter(residual(_) > 0).filter(e => !visited.contains(e.to))
                    neighbours.foreach(n => queue.enqueue(n.to))
            val nodes = capacities.keySet.flatMap(e => Set(e.from, e.to))
            (visited.toSet, nodes.diff(visited))

        def maximiseFlow(): Unit =
            while
                val p = path
                if p.isDefined then
                    augment(p.get)
                    true
                else false
            do ()

        def augment(path: List[REdge]): Unit =
            val b = path.map(residual).min
            for REdge(u, v, forwards) <- path do
                if forwards then flows.put(Edge(u, v), flow(Edge(u, v)) + b)
                else flows.put(Edge(v, u), flow(Edge(v, u)) - b)

        def path: Option[List[REdge]] =
            val stack = mutable.Stack(s)
            val visited = mutable.Set[String]()
            val prev = mutable.Map[String, REdge]()
            while stack.nonEmpty do
                val current = stack.pop()

                if current == t then
                    visited.add(t)
                    stack.clear()

                else if !visited.contains(current) then
                    visited.add(current)
                    val neighbours = edges.filter(_.from == current).filter(residual(_) > 0).filter(e => !visited.contains(e.to))
                    for e@REdge(_, n, _) <- neighbours do
                        prev.put(n, e)
                        stack.push(n)

            if visited.contains(t) then
                var path = List[REdge]()
                var current = t
                while prev.contains(current) do
                    path = prev(current) :: path
                    current = prev(current).from
                Some(path)
            else None

    case class Edge(from: String, to: String):
        override def toString: String = s"$from -> $to"
    case class REdge(from: String, to: String, forwards: Boolean):
        override def toString: String = if forwards then s"$from -> $to" else s"$to <- $from"