extension[A] (x: A)
    def let[B](f: A => B): B = f(x)