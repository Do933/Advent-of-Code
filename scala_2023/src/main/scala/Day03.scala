object Day03:

    @main
    def day03(): Unit =
        println(AoCRunner.runFile("2023/03/input", solve))

    def solveOneLine(input: String): Result =
        ((symbols: Map[(Int, Int), Char], numbers: Map[(Int, Int), Int]) =>
            ((adjacentSymbols: ((Int, Int), Int) => Set[((Int, Int), Char)]) =>
                Result(numbers.filter(num => adjacentSymbols(num._1, num._2).nonEmpty).values.sum,
                    numbers.toList.flatMap(n => adjacentSymbols(n._1, n._2).map(n -> _)).groupBy(_._2).map { case (s, nums) => s -> nums.map(_._1) }
                        .filter(e => e._1._2 == '*').filter(e => e._2.size == 2).map { case (_, List((_,a), (_,b))) => a * b }.sum))
                ((pos: (Int, Int), n: Int) => (0 until n.toString.length).map(i => (pos._1 + i, pos._2))
                    .flatMap(p => (-1 to 1).flatMap(dx => (-1 to 1).map(dy => (p._1 + dx, p._2 + dy))))
                    .filter(symbols.contains).map(p => p -> symbols(p)).toSet))
            (input.linesIterator.zipWithIndex
                .flatMap { case (line, y) => line.zipWithIndex.filter { case (c, _) => !c.isDigit && c != '.' }.map { case (c, x) => (x, y) -> c }}.toMap,
                input.linesIterator.zipWithIndex
                    .flatMap { case (line, y) => "\\d+".r.findAllIn(line).matchData.map(m => (m.start, y) -> m.group(0).toInt)}.toMap)

    def solve(input: String): Result =
        val symbols = input.linesIterator.zipWithIndex.flatMap { case (line, y) =>
            line.zipWithIndex.filter { case (c, _) => !c.isDigit && c != '.' }.map { case (c, x) => Pos(x, y) -> c } }
            .toMap

        val numbers = input.linesIterator.zipWithIndex.flatMap { case (line, y) =>
            """\d+""".r.findAllIn(line).matchData.map(m => Pos(m.start, y) -> m.group(0).toInt) }
            .toMap

        val adjacentSymbols = (pos: Pos, n: Int) =>
            (0 until n.toString.length).map(i => Pos(pos.x + i, pos.y)).flatMap(p => List(Pos(p.x + 1, p.y), Pos(p.x + 1, p.y + 1),
                Pos(p.x, pos.y + 1), Pos(p.x - 1, p.y + 1), Pos(p.x - 1, p.y), Pos(p.x - 1, p.y - 1),
                Pos(p.x, p.y - 1), Pos(p.x + 1, p.y - 1)))
                .filter(symbols.contains)
                .map(p => p -> symbols(p))
                .toSet

        val adjacentNumbers = numbers.toList.flatMap(n => adjacentSymbols(n._1, n._2).map(n -> _)).groupBy(_._2).map { case (s, nums) => s -> nums.map(_._1) }

        val partNumbers = numbers.filter(num => adjacentSymbols(num._1, num._2).nonEmpty).values

        val gearRatios = adjacentNumbers.filter(e => e._1._2 == '*').filter(e => e._2.size == 2).map { case (_, List((_,a), (_,b))) => a * b }

        Result(partNumbers.sum, gearRatios.sum)

    case class Pos(x: Int, y: Int)