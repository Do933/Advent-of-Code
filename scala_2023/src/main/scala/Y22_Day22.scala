import Y22_Day22.Tile.*
import Y22_Day22.Direction.*
import Y22_Day22.Turn.*

import java.util.stream.Collectors
import scala.:+
import scala.annotation.{tailrec, targetName}
import scala.collection.mutable
import scala.math.{ceil, exp, max}

object Y22_Day22:

    @main
    def y22_day22(): Unit =
        println(AoCRunner.runFile("2022/22/input", solve))

    def solve(input: String): Result =
        val List(b, is) = input.split("\n\n").toList : @unchecked
        val board = b.as[Board]
        val instructions = is.as[List[Instruction]]

        var pos = board.start
        var dir = Right
        for instruction <- instructions do
            instruction match
                case Rotate(t) => dir = dir.turn(t)
                case Move(amt) => for _ <- 1 to amt do pos = board.move(pos, dir)

        val dirScore = dir match
            case Right => 0
            case Down => 1
            case Left => 2
            case Up => 3

        Result(pos.y * 1000 + pos.x * 4 + dirScore, 0)

    given Read[List[Instruction]] with
        def read(string: String): List[Instruction] =
            val res = mutable.Buffer[Instruction]()
            var s = string
            while s.nonEmpty do
                if s.startsWith("L") || s.startsWith("R") then
                    res.append(Rotate(s.take(1).as[Turn]))
                    s = s.drop(1)
                else
                    res.append(Move(s.takeWhile(_.isDigit).toInt))
                    s = s.dropWhile(_.isDigit)
            res.toList
    given Read[Board] with
        def read(s: String): Board =
            val tiles = s.linesIterator.zipWithIndex.flatMap { case (line, y) =>
                line.zipWithIndex.map { case (c, x) => Pos(x+1, y+1) -> c.toString.as[Tile] } }
                .toMap
            Board(tiles.filter(_._2 == Wall).keySet, tiles.filter(_._2 == Floor).keySet)
    given Read[Tile] with
        def read(s: String): Tile = if s == "." then Floor else if s == "#" then Wall else Empty
    given Read[Turn] with
        def read(s: String): Turn = if s == "L" then AntiClockwise else Clockwise

    enum Side:
        case T, U, L, R, F, B
    sealed trait Instruction
    case class Rotate(turn: Turn) extends Instruction
    case class Move(amount: Int) extends Instruction
    enum Turn:
        case Clockwise, AntiClockwise
    enum Direction(val dx: Int, val dy: Int):
        case Left extends Direction(-1, 0)
        case Up extends Direction(0, -1)
        case Right extends Direction(1, 0)
        case Down extends Direction(0, 1)
        def turn(t: Turn): Direction = (this, t) match
            case (Left, Clockwise) => Up
            case (Up, Clockwise) => Right
            case (Right, Clockwise) => Down
            case (Down, Clockwise) => Left
            case (Left, AntiClockwise) => Down
            case (Up, AntiClockwise) => Left
            case (Right, AntiClockwise) => Up
            case (Down, AntiClockwise) => Right
    enum Tile:
        case Wall, Floor, Empty
    case class Board(walls: Set[Pos], floor: Set[Pos]):
        def start: Pos = floor.filter(_.y == 1).minBy(_.x)
        def side(pos: Pos): Side = ((pos.x - 1) / 50, (pos.y - 1) / 50) match
            case (1, 0) => Side.B
            case (2, 0) => Side.R
            case (1, 1) => Side.T
            case (0, 2) => Side.L
            case (1, 2) => Side.F
            case (0, 3) => Side.U
            case _ => throw IllegalArgumentException()
//        def wrapFace(pos: Pos, dir: Direction): (Pos, Direction) = (side(pos), dir) match
//            case (Side.F, Down) => (Side.U, Left)
//            case (Side.U, Right) => (Side.F, )
        def tile(pos: Pos): Tile = if walls.contains(pos) then Wall else if floor.contains(pos) then Floor else Empty
        def wrap(pos: Pos, dir: Direction): Pos = dir match
            case Left => (walls union floor).filter(_.y == pos.y).maxBy(_.x)
            case Up => (walls union floor).filter(_.x == pos.x).maxBy(_.y)
            case Right => (walls union floor).filter(_.y == pos.y).minBy(_.x)
            case Down => (walls union floor).filter(_.x == pos.x).minBy(_.y)
        def move(pos: Pos, dir: Direction): Pos =
            var newPos = pos.move(dir)
            if tile(newPos) == Empty then newPos = wrap(pos, dir)
            if tile(newPos) == Wall then pos else newPos
    case class Pos(x: Int, y: Int):
        def move(dir: Direction): Pos = Pos(x + dir.dx, y + dir.dy)