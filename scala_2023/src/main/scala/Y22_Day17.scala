import java.util.stream.Collectors
import scala.:+
import scala.annotation.targetName
import scala.collection.mutable

object Y22_Day17:

    @main
    def y22_day17(): Unit =
        println(AoCRunner.runFileSplit("2022/17/input", solve(_, 2022), solve(_, 1000000000000L)))

    def solve(input: String, goal: Long): Long =
        val directions = input.map(c => if c == '<' then Direction.Left else Direction.Right).toList

        val space = Space((0 until 7).map(x => Pos(x, -1)).toSet.to(mutable.Set))
        val shapes = List(
            Set(Pos(0, 0), Pos(1, 0), Pos(2, 0), Pos(3, 0)), // ---
            Set(Pos(1, 0), Pos(0, 1), Pos(1, 1), Pos(2, 1), Pos(1, 2)), // +
            Set(Pos(0, 0), Pos(1, 0), Pos(2, 0), Pos(2, 1), Pos(2, 2)), // _|
            Set(Pos(0, 0), Pos(0, 1), Pos(0, 2), Pos(0, 3)), // |
            Set(Pos(0, 0), Pos(1, 0), Pos(0, 1), Pos(1, 1)), // []
        )

        val seen = mutable.HashMap[State, Int]()
        val heights = mutable.HashMap[State, Int]()

        var i = 0
        var j = 0
        var start = 0
        var startHeight = 0
        var step = 0
        var increment = 0
        while
            val state = State(i % 5, j % directions.size, space.topRows)
            if seen.contains(state) then
                start = seen(state)
                startHeight = heights(state)
                step = i - start
                increment = space.topY + 1 - heights(state)
                false
            else
                seen.put(state, i)
                heights.put(state, space.topY + 1)

                val (_, newJ) = simulate(1, shapes, directions, space, i, j)
                j = newJ

                i += 1
                true
        do ()

        println(s"At $start, $startHeight high")
        println(s"For every $step rocks, $increment higher")

        val jumpTo = ((goal - start) / step) * step + start
        val rocksLeft = (goal - jumpTo).toInt

        val atJump = startHeight + ((goal - start) / step) * increment

        println(s"Can calculate $jumpTo")
        println(s"At $jumpTo, $atJump high")
        println(s"$rocksLeft rocks left to simulate")

        atJump + simulate(rocksLeft, shapes, directions, space, i, j)._1 - startHeight - increment

    def simulate(rocks: Int, shapes: List[Set[Pos]], directions: List[Direction], space: Space, rockStart: Int, dirStart: Int): (Int, Int) =
        var j = dirStart
        for i <- 0 until rocks do
            if space.topY > 200 then
                space.removeBottom()

            var rock = Rock(space.nextPos, shapes((rockStart + i) % shapes.size))

            while
                val dir = directions(j % directions.size)
                j += 1
                dir match
                    case Direction.Left => if rock.min.x > 0 && space.fits(rock.move(Pos(-1, 0)).occupies) then rock = rock.move(Pos(-1, 0))
                    case Direction.Right => if rock.max.x < 6 && space.fits(rock.move(Pos(1, 0)).occupies) then rock = rock.move(Pos(1, 0))
                if space.fits(rock.move(Pos(0, -1)).occupies) then
                    rock = rock.move(Pos(0, -1))
                    true
                else
                    space.add(rock.occupies)
                    false
            do ()

        (space.topY + 1, j)

    def visualise(space: Space, rock: Rock): String =
        var result = "+-------+"
        for y <- 0 to rock.max.y do
            val line = "|" + (0 until 7).map(x => if space.occupied.contains(Pos(x, y)) then "#" else if rock.occupies.contains(Pos(x, y)) then "@" else ".").mkString + "|"
            result = line + "\n" + result
        result

    case class State(rockIndex: Int, dirIndex: Int, topRows: Set[Pos])

    case class Rock(pos: Pos, shape: Set[Pos]):
        def occupies: Set[Pos] = shape.map(pos + _)
        def min: Pos = Pos(occupies.map(_.x).min, occupies.map(_.y).min)
        def max: Pos = Pos(occupies.map(_.x).max, occupies.map(_.y).max)
        def move(amount: Pos): Rock = Rock(pos + amount, shape)

    class Space(val occupied: mutable.Set[Pos]):
        def topY: Int = occupied.map(_.y).maxOption.getOrElse(-1)
        def topRows: Set[Pos] =
            val top = topY
            occupied.map(pos => Pos(pos.x, top - pos.y)).filter(pos => pos.y < 40).toSet
        def nextPos: Pos = Pos(2, topY + 4)
        def fits(shape: Set[Pos]): Boolean = occupied.intersect(shape).isEmpty
        def add(shape: Set[Pos]): Unit = occupied.addAll(shape)
        def removeBottom(): Unit =
            val top = topY
            val toRemove = occupied.filter(pos => pos.y < top - 100).toSet
            toRemove.foreach(occupied.remove)

    case class Pos(x: Int, y: Int):
        @targetName("plus")
        def +(other: Pos): Pos = Pos(x + other.x, y + other.y)

    enum Direction:
        case Left, Right