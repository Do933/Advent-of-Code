import scala.Iterator.iterate
import scala.annotation.targetName
import scala.collection.mutable

object Day24:

    @main
    def day24(): Unit =
        println(AoCRunner.runFile("2023/24/input", solve))

    def solve(input: String): Result =
        val hailstones = input.linesIterator.map(_.as[Hailstone]).toList

        val (min, max) = (BigDecimal(200000000000000.0), BigDecimal(400000000000000.0))

        val inBoundary = hailstones.indices.flatMap(i => hailstones.drop(i+1).map(h => hailstones(i).zLine intersect h.zLine))
            .filter(_.isDefined)
            .map(_.get)
            .filter(p => p.x >= min && p.x < max && p.y >= min && p.y < max)

        val answers = mutable.Map[Long, Int]()
        for _ <- 0 until 1000 do
            var equations = List[Array[Double]]()

            var indices = List[Int]()
            for _ <- 0 until 4 do
                while
                    val index = (math.random() * hailstones.size).toInt
                    if indices.contains(index) then
                        true
                    else
                        indices = index :: indices
                        false
                do ()

            val h1 = hailstones(indices(0))
            val h2 = hailstones(indices(1))
            val h3 = hailstones(indices(2))
            val h4 = hailstones(indices(3))

            val dp1 = h1.orig - h2.orig
            val dv1 = h2.dir - h1.dir
            val dp2 = h3.orig - h4.orig
            val dv2 = h4.dir - h3.dir

            val r1 = (h1.orig cross h1.dir) - (h2.orig cross h2.dir)
            val r2 = (h3.orig cross h3.dir) - (h4.orig cross h4.dir)

            equations = equations :+ Array(0, -dp1.z, dp1.y, 0, -dv1.z, dv1.y, r1.x)
            equations = equations :+ Array(dp1.z, 0, -dp1.x, dv1.z, 0, -dv1.x, r1.y)
            equations = equations :+ Array(-dp1.y, dp1.x, 0, -dv1.y, dv1.x, 0, r1.z)
            equations = equations :+ Array(0, -dp2.z, dp2.y, 0, -dv2.z, dv2.y, r2.x)
            equations = equations :+ Array(dp2.z, 0, -dp2.x, dv2.z, 0, -dv2.x, r2.y)
            equations = equations :+ Array(-dp2.y, dp2.x, 0, -dv2.y, dv2.x, 0, r2.z)

            val solutions = System(equations).solve.map(math.round)

            val sum = solutions(3) + solutions(4) + solutions(5)
            answers.put(sum, answers.getOrElse(sum, 0) + 1)

        Result(inBoundary.size, answers.maxBy(_._2)._1)

    case class System(equations: List[Array[Double]]):
        require(equations.nonEmpty)
        require(equations.forall(e => e.nonEmpty && e.length == equations.head.length))
        val vars: Int = equations.head.length - 1

        override def toString: String =
            equations.map(e => e.dropRight(1).zipWithIndex.map { case (c, i) => s"$c * x$i" }.mkString(" + ") + s" = ${e.last}").mkString("\n")

        def solve: Array[Double] =
            val diagonalised = this.diagonalise.equations
            var results = List[Double]()
            for i <- 0 until vars do
                val eq = diagonalised(vars - i - 1)
                val res = (eq.last - eq.dropRight(1).takeRight(i).zip(results).map { case (c, x) => c * x }.sum) / eq(vars - i - 1)
                results = res :: results
            results.toArray

        def diagonalise: System = System(diagonalise(equations, 0))

        private def diagonalise(eqs: List[Array[Double]], x: Int): List[Array[Double]] =
            if eqs.isEmpty then eqs
            else if x+2 >= eqs.head.length then eqs
            else if eqs.forall(e => e(x) == 0.0) then diagonalise(eqs, x+1)
            else if eqs.head(x) == 0.0 then
                val nonZero = eqs.indexWhere(_(x) != 0.0)
                diagonalise(eqs.zipWithIndex.map { case (e, i) => if i == 0 then eqs(nonZero) else if i == nonZero then eqs.head else e }, x)
            else
                eqs.head :: diagonalise(eqs.drop(1).map(e =>
                    val factor = e(x) / eqs.head(x)
                    e.zip(eqs.head).map { case (v, t) => v - t * factor }
                ), x)

    case class Hailstone(orig: Vec3l, dir: Vec3l):
        def xLine: Line2d = Line2d(orig.yz, dir.yz)
        def yLine: Line2d = Line2d(orig.xz, dir.xz)
        def zLine: Line2d = Line2d(orig.xy, dir.xy)
        def line: Line3d = Line3d(orig, dir)

    case class Line3d(orig: Vec3l, dir: Vec3l):
        def intersect(other: Line3d): Option[Vec3d] =
            val Line3d(p, v) = this
            val Line3d(q, w) = other

            val s = (w.y * (q.x - p.x) - w.x * (q.y - p.y)).toDouble / (v.x * w.y - v.y * w.x).toDouble
            val t = (v.y * (p.x - q.x) - v.x * (p.y - q.y)).toDouble / (v.y * w.x - v.x * w.y).toDouble

            if p.z + s * v.z != q.z + t * w.z then return None

            Some(p.toVec3d + s * v.toVec3d)

    case class Line2d(orig: Vec2l, dir: Vec2l):
        def intersect(other: Line2d): Option[Vec2bd] =
            val Line2d(p@Vec2(x1, y1), v) = this
            val Line2d(q@Vec2(x3, y3), w) = other

            if v.x*w.y == w.x*v.y then return None

            val Vec2(x2, y2) = v + p
            val Vec2(x4, y4) = q + w

            val den = BigDecimal((x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4))
            val t = BigDecimal((x1 - x3) * (y3 - y4) - (y1 - y3) * (x3 - x4)) / den
            val u = BigDecimal((x1 - x3) * (y1 - y2) - (y1 - y3) * (x1 - x2)) / den

            if t.compareTo(BigDecimal(0.0)) == -1 || u.compareTo(BigDecimal(0.0)) == -1 then return None

            Some(Vec2(BigDecimal(x1), BigDecimal(y1)) + Vec2((t * BigDecimal(v.x)), (t * BigDecimal(v.y))))

    given Read[Hailstone] with
        override def read(s: String): Hailstone =
            val ps = s.split(" @ ")
            Hailstone(ps(0).as[Vec3l], ps(1).as[Vec3l])
    given Read[Vec3l] with
        override def read(s: String): Vec3l =
            val ps = s.split(", ").map(_.trim)
            Vec3(ps(0).toLong, ps(1).toLong, ps(2).toLong)