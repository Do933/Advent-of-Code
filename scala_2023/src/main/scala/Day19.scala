
import scala.Iterator.iterate
import scala.annotation.targetName
import scala.collection.mutable

object Day19:

    @main
    def day19(): Unit =
        println(AoCRunner.runFile("2023/19/input", solve))

    def solve(input: String): Result =
        val workflows = input.split("\n\n")(0)
            .mapLines(l => l.split("\\{")(0) -> l.split("\\{")(1).dropRight(1).split(","))
            .map { case (n, rs) => n -> Workflow(rs.dropRight(1).map(read[Rule]).toList, rs.last) }
            .toMap

        val parts = input.split("\n\n")(1)
            .mapLines(l => l.drop(1).dropRight(1).split(","))
            .map(ps => Part(ps(0).drop(2).toLong, ps(1).drop(2).toLong, ps(2).drop(2).toLong, ps(3).drop(2).toLong))

        extension (w: Workflow)
            def apply(p: Part): "A"|"R" =
                val next = w.rules.find(_(p)).map(_.dest).getOrElse(w.default)
                next match
                    case "A"|"R" => next.asInstanceOf["A"|"R"]
                    case _ => workflows(next)(p)
            def combinations(p: IPart, i: Int): Long =
                val rule = w.rules.lift(i)
                val (applies, next) = rule.map(_(p)).getOrElse((Some(p), None))
                val dest = rule.map(_.dest).getOrElse(w.default)
                val c = dest match
                    case "A" => applies.map(_.combinations).getOrElse(0L)
                    case "R" => 0
                    case _ => applies.map(workflows(dest).combinations(_, 0)).getOrElse(0L)
                c + next.map(w.combinations(_, i+1)).getOrElse(0L)

        Result(parts.filter(p => workflows("in")(p) == "A").sumOf(_.score), workflows("in").combinations(IPart(), 0))

    case class Interval(min: Long, max: Long):
        require(max >= min)
        val size: Long = max - min + 1
        def split(value: Long, op: Op): (Option[Interval], Option[Interval]) = op match
            case Op.LT =>
                if value < min then (None, Some(this))
                else if value >= min && value <= max then (Some(Interval(min, value-1)), Some(Interval(value, max)))
                else /*value > max*/ (Some(this), None)
            case Op.GT =>
                if value < min then (Some(this), None)
                else if value >= min && value <= max then (Some(Interval(value+1, max)), Some(Interval(min, value)))
                else /*value > max*/ (None, Some(this))
        override def toString: String = s"{$min...$max}"

    case class Part(x: Long, m: Long, a: Long, s: Long):
        def apply(n: String): Long = n match
            case "x" => x
            case "m" => m
            case "a" => a
            case "s" => s
        def score: Long = x + m + a + s
    case class IPart(x: Interval, m: Interval, a: Interval, s: Interval):
        def split(n: String, value: Long, op: Op): (Option[IPart], Option[IPart]) = n match
            case "x" =>
                val (t, f) = x.split(value, op)
                (t.map(IPart(_, m, a, s)), f.map(IPart(_, m, a, s)))
            case "m" =>
                val (t, f) = m.split(value, op)
                (t.map(IPart(x, _, a, s)), f.map(IPart(x, _, a, s)))
            case "a" =>
                val (t, f) = a.split(value, op)
                (t.map(IPart(x, m, _, s)), f.map(IPart(x, m, _, s)))
            case "s" =>
                val (t, f) = s.split(value, op)
                (t.map(IPart(x, m, a, _)), f.map(IPart(x, m, a, _)))
        def combinations: Long = x.size * m.size * a.size * s.size
    object IPart:
        def apply(): IPart = IPart(Interval(1, 4000), Interval(1, 4000), Interval(1, 4000), Interval(1, 4000))

    case class Workflow(rules: List[Rule], default: String):
        override def toString: String = s"${rules.mkString("\n")}\n_ => $default"

    given Read[Rule] with
        override def read(s: String): Rule =
            val field = s.split("[<>]")(0)
            val value = s.drop(field.length+1).split(":")(0).toLong
            val dest = s.drop(field.length+1).split(":")(1)
            if s.contains(">") then Rule(field, value, dest, Op.GT)
            else Rule(field, value, dest, Op.LT)

    case class Rule(field: String, value: Long, dest: String, op: Op):
        def apply(p: Part): Boolean = op match
            case Op.LT => p(field) < value
            case Op.GT => p(field) > value
        def apply(p: IPart): (Option[IPart], Option[IPart]) = p.split(field, value, op)
        override def toString: String = s"$field $op $value => $dest"

    enum Op:
        case LT, GT
        override def toString: String = if this == LT then "<" else ">"