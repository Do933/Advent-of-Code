import Day07.Card.*
import Day07.HandType
import Day07.HandType.*

import scala.collection.mutable
import scala.Iterator.iterate

object Day09:

    @main
    def day09(): Unit =
        println(AoCRunner.runFile("2023/09/input", solve))

    def solveOneLine(input: String): Result =
        ((X: (List[Long], List[Long] => Long, (Long, Long) => Long) => Long) =>
            ((seqs: List[List[Long]]) => Result(seqs.map(X(_, _.last, _+_)).sum, seqs.map(X(_, _.head, _-_)).sum))
                (input.linesIterator.map(_.split(" ").map(_.toLong).toList).toList))
            ((seq, f, c) => iterate(seq)(s => s.drop(1).zip(s).map(_-_)).takeWhile(_.exists(_!=0)).map(f).reduceRight(c))

    def solve(input: String): Result =
        val sequences = input.mapLines(_.longs)
        Result(sequences.sumOf(extrapolate(_, _.last, _+_)), sequences.sumOf(extrapolate(_, _.head, _-_)))

    def extrapolate(sequence: List[Long], select: List[Long] => Long, combine: (Long, Long) => Long): Long =
        val diffs = sequence.withPrev(_-_)
        if diffs.exists(_ != 0L) then combine(select(sequence), extrapolate(diffs, select, combine))
        else select(sequence)