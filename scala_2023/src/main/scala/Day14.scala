
import Day12.Spring.*

import scala.Iterator.iterate
import scala.annotation.targetName
import scala.collection.mutable

object Day14:

    @main
    def day14(): Unit =
        println(AoCRunner.runFile("2023/14/input", solve))

    def solve(input: String): Result =
        val square = input.linesIterator.zipWithIndex.flatMap { case (l, y) => l.zipWithIndex.filter(_._1 == '#').map { case (_, x) => Vec2(x, y) } }.toSet
        val round = input.linesIterator.zipWithIndex.flatMap { case (l, y) => l.zipWithIndex.filter(_._1 == 'O').map { case (_, x) => Vec2(x, y) } }.toSet

        val seen = mutable.Map[Set[Vec2i], Int]()
        var i = 0
        var current = round
        while !seen.contains(current) do
            seen.put(current, i)
            current = cycle(current, square)
            i += 1

        val init = seen(current)
        val step = i - init

        val target = 1000000000
        val closest = ((target - init) / step) * step + init

        for _ <- 0 until (target - closest) do
            current = cycle(current, square)

        Result(moveRocks(round, square, north).toList.map(r => input.linesIterator.size - r.y).sum, current.toList.map(r => input.linesIterator.size - r.y).sum)

    val north: Vec2i = Vec2(0, -1)
    val west: Vec2i = Vec2(-1, 0)
    val south: Vec2i = Vec2(0, 1)
    val east: Vec2i = Vec2(1, 0)

    def cycle(round: Set[Vec2i], square: Set[Vec2i]): Set[Vec2i] =
        val n = moveRocks(round, square, north)
        val w = moveRocks(n, square, west)
        val s = moveRocks(w, square, south)
        moveRocks(s, square, east)

    def moveRocks(round: Set[Vec2i], square: Set[Vec2i], dir: Vec2i): Set[Vec2i] =
        val toMove = round.toList.sortBy(r => if dir.x == 0 then r.y * -dir.y else r.x * -dir.x)
        val result = mutable.Set[Vec2i]()

        val min = Vec2(0, 0)
        val max = Vec2((round union square).map(_.x).max, (round union square).map(_.y).max)

        for rock <- toMove do
            val collide = (result union square)
                .filter(r => if dir.x == 0 then rock.x == r.x && (if dir.y == -1 then rock.y > r.y else rock.y < r.y) else rock.y == r.y && (if dir.x == -1 then rock.x > r.x else rock.x < r.x))
                .minByOption(r => if dir.x == 0 then r.y * dir.y else r.x * dir.x)

            collide match
                case None => result.add(Vec2(if dir.y == 0 then if dir.x == -1 then min.x else max.x else rock.x, if dir.x == 0 then if dir.y == -1 then min.y else max.y else rock.y))
                case Some(c) => result.add(c - dir)

        result.toSet