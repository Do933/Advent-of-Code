extension (n: Int)
    def mod(m: Int): Int =
        ((n % m) + m) % m

extension (n: Long)
    def mod(m: Long): Long =
        ((n % m) + m) % m
