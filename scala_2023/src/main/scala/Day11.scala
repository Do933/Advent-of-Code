import scala.Iterator.iterate
import scala.annotation.targetName
import scala.collection.mutable

object Day11:

    @main
    def day11(): Unit =
        println(AoCRunner.runFile("2023/11/input", solve))

    def solve(input: String): Result =
        val universe = Universe(input.linesIterator.zipWithIndex.flatMap { case (l, y) =>
            l.zipWithIndex.filter { case (c, _) => c == '#' }.map { case (_, x) => Pos(x.toLong, y.toLong) } }.toSet)

        val galaxies = universe.expand(1).galaxies.toList
        val part1 = galaxies.indices.flatMap(i => ((i+1) until galaxies.size).map(j => galaxies(i) dist galaxies(j))).sum

        val galaxiesBig = universe.expand(999999).galaxies.toList
        val part2 = galaxiesBig.indices.flatMap(i => ((i + 1) until galaxiesBig.size).map(j => galaxiesBig(i) dist galaxiesBig(j))).sum

        Result(part1, part2)

    case class Pos(x: Long, y: Long):
        @targetName("plus")
        def +(other: Pos): Pos = Pos(x + other.x, y + other.y)
        def dist(other: Pos): Long = math.abs(x - other.x) + math.abs(y - other.y)

    case class Universe(galaxies: Set[Pos]):
        def expandRight(x: Long, amt: Long): Universe = Universe(galaxies.map(g => if g.x > x then g + Pos(amt, 0) else g))
        def expandDown(y: Long, amt: Long): Universe = Universe(galaxies.map(g => if g.y > y then g + Pos(0, amt) else g))
        def expand(amt: Long): Universe =
            val rows = (galaxies.map(_.y).min to galaxies.map(_.y).max).filter(y => !galaxies.exists(_.y == y)).toList
            val columns = (galaxies.map(_.x).min to galaxies.map(_.x).max).filter(x => !galaxies.exists(_.x == x)).toList
            columns.foldRight(rows.foldRight(this)((y, u) => u.expandDown(y, amt)))((x, u) => u.expandRight(x, amt))
