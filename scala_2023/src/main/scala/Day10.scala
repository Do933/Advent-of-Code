import Day10.Shape.*

import scala.Iterator.iterate
import scala.annotation.targetName
import scala.collection.mutable

object Day10:

    @main
    def day10(): Unit =
        println(AoCRunner.runFile("2023/10/input", solve))

    def solve(input: String): Result =
        val sketch = input.linesIterator.zipWithIndex.flatMap { case (l, y) => l.zipWithIndex.map { case (c, x) => Pos(x, y) -> c.toString.as[Shape] } }.toMap
        val start = sketch.find(_._2 == Start).get._1

        val loop = bfs(start, sketch)
        def neighboursInside(pos: Pos): Set[Pos] =
            val top = if loop.contains(pos + Pos(-1, -1)) && sketch(pos + Pos(-1, -1)).connections.contains(Pos(1, 0)) then Set() else Set(pos + Pos(0, -1))
            val left = if loop.contains(pos + Pos(-1, -1)) && sketch(pos + Pos(-1, -1)).connections.contains(Pos(0, 1)) then Set() else Set(pos + Pos(-1, 0))
            val bottom = if loop.contains(pos + Pos(-1, 0)) && sketch(pos + Pos(-1, 0)).connections.contains(Pos(1, 0)) then Set() else Set(pos + Pos(0, 1))
            val right = if loop.contains(pos + Pos(0, -1)) && sketch(pos + Pos(0, -1)).connections.contains(Pos(0, 1)) then Set() else Set(pos + Pos(1, 0))
            top union left union bottom union right
        def neighboursGrid(pos: Pos): Set[Pos] =
            Set(Pos(0, 1), Pos(0, -1), Pos(1, 0), Pos(-1, 0)).map(_ + pos).filter(!loop.contains(_))
        def inBounds(pos: Pos): Boolean =
            val min = Pos(loop.keySet.map(_.x).min-1, loop.keySet.map(_.y).min-1)
            val max = Pos(loop.keySet.map(_.x).max+1, loop.keySet.map(_.y).max+1)
            pos.x >= min.x && pos.x <= max.x && pos.y >= min.y && pos.y <= max.y

        val outside = mutable.Set[Pos]()
        val inside = mutable.Set[Pos]()
        def insideFrom(pos: Pos): Set[Pos] = {
            val visited = dfs(pos, neighboursInside, inBounds, outside.toSet, inside.toSet)
            if visited.forall(inBounds) then
                inside.addAll(visited)
                dfs(pos, neighboursGrid, inBounds, Set(), Set())
            else
                outside.addAll(visited)
                Set()
        }

        val toCheck = sketch.keySet.filter(!loop.contains(_))
        val insides = mutable.Set[Pos]()
        var i = 0
        for pos <- toCheck do
            if !insides.contains(pos) then
                insides.addAll(insideFrom(pos))
            i += 1

        Result(loop.values.max, insides.size)

    def dfs(start: Pos, neighbours: Pos => Set[Pos], inBounds: Pos => Boolean, outside: Set[Pos], inside: Set[Pos]): Set[Pos] =
        val visited = mutable.Set[Pos]()

        val stack = mutable.Stack[Pos]()
        stack.push(start)

        while stack.nonEmpty do
            val current = stack.pop()

            if outside.contains(current) then
                return visited.toSet union outside
            if inside.contains(current) then
                return visited.toSet union inside

            if !inBounds(current) then
                visited.add(current)
                stack.clear()

            if visited.contains(current) then
                ()

            else
                visited.add(current)
                neighbours(current).filter(!visited.contains(_)).foreach(stack.push)

        visited.toSet

    def bfs(start: Pos, sketch: Map[Pos, Shape]): Map[Pos, Int] =
        val distance = mutable.Map[Pos, Int]()
        distance.put(start, 0)

        val queue = mutable.Queue[Pos]()
        queue.enqueue(start)

        val visited = mutable.Set[Pos]()

        while queue.nonEmpty do
            val current = queue.dequeue()

            if visited.contains(current) || !sketch.contains(current) || sketch(current) == Ground then
                ()

            else
                visited.add(current)

                for n <- sketch(current).connections.map(dir => dir + current) do
                    val couldGetBack = sketch.contains(n) && sketch(n).connections.exists(dir => dir + n == current)
                    if couldGetBack && distance.getOrElse(n, Int.MaxValue) > distance(current) + 1 then
                        distance.put(n, distance(current) + 1)
                        queue.enqueue(n)

        distance.toMap

    case class Pos(x: Int, y: Int):
        @targetName("plus")
        def +(other: Pos): Pos = Pos(x + other.x, y + other.y)

    given Read[Shape] with
        override def read(s: String): Shape = s match
            case "|" => Vertical
            case "-" => Horizontal
            case "L" => BottomLeft
            case "F" => TopLeft
            case "7" => TopRight
            case "J" => BottomRight
            case "S" => Start
            case "." => Ground

    enum Shape:
        case Horizontal, Vertical, BottomLeft, TopLeft, TopRight, BottomRight, Start, Ground
        def connections: Set[Pos] = this match
            case Horizontal => Set(Pos(-1, 0), Pos(1, 0))
            case Vertical => Set(Pos(0, -1), Pos(0, 1))
            case BottomLeft => Set(Pos(0, -1), Pos(1, 0))
            case TopLeft => Set(Pos(0, 1), Pos(1, 0))
            case TopRight => Set(Pos(0, 1), Pos(-1, 0))
            case BottomRight => Set(Pos(0, -1), Pos(-1, 0))
            case Start => Set(Pos(0, 1), Pos(0, -1), Pos(1, 0), Pos(-1, 0))
            case Ground => Set()
