
import scala.Iterator.iterate
import scala.annotation.targetName
import scala.collection.mutable

object Day16:

    @main
    def day16(): Unit =
        println(AoCRunner.runFile("2023/16/input", solve))

    def solve(input: String): Result = {
        val layout = input.linesIterator.zipWithIndex.flatMap { case (l, y) =>
                l.zipWithIndex.map { case (c, x) => Vec2(x, y) -> c } }
            .toMap

        val minX = layout.keys.map(_.x).min
        val maxX = layout.keys.map(_.x).max
        val minY = layout.keys.map(_.y).min
        val maxY = layout.keys.map(_.y).max

        val possibleStarts =
            layout.keys.map(_.x).toSet.map(x => Beam(Vec2(x, minY), Vec2(0, 1))) union
                layout.keys.map(_.x).toSet.map(x => Beam(Vec2(x, maxY), Vec2(0, -1))) union
                layout.keys.map(_.y).toSet.map(y => Beam(Vec2(minX, y), Vec2(1, 0))) union
                layout.keys.map(_.y).toSet.map(y => Beam(Vec2(maxX, y), Vec2(-1, 0)))

        Result(score(layout, Beam(Vec2(0, 0), Vec2(1, 0))), possibleStarts.map(score(layout, _)).max)
    }

    def score(layout: Map[Vec2[Int], Char], start: Beam): Int =
        val prevBeams = mutable.Set[Beam]()
        val currBeams = mutable.Set[Beam](start)

        while currBeams.nonEmpty do
            val beams = currBeams.toSet
            currBeams.clear()

            for beam <- beams.filter(!prevBeams.contains(_)).filter(b => layout.contains(b.pos)) do
                val c = layout(beam.pos)
                if c == '.' then
                    currBeams.add(beam.next)
                else if c == '-' then
                    if beam.dir.x == 0 then
                        currBeams.add(Beam(beam.pos, Vec2(-1, 0)))
                        currBeams.add(Beam(beam.pos, Vec2(1, 0)))
                    else
                        currBeams.add(beam.next)
                else if c == '|' then
                    if beam.dir.y == 0 then
                        currBeams.add(Beam(beam.pos, Vec2(0, -1)))
                        currBeams.add(Beam(beam.pos, Vec2(0, 1)))
                    else
                        currBeams.add(beam.next)
                else if c == '/' then
                    currBeams.add(Beam(beam.pos, Vec2(-beam.dir.y, -beam.dir.x)).next)
                else if c == '\\' then
                    currBeams.add(Beam(beam.pos, Vec2(beam.dir.y, beam.dir.x)).next)

            prevBeams.addAll(beams.filter(b => layout.contains(b.pos)))
        prevBeams.map(_.pos).size

    case class Beam(pos: Vec2i, dir: Vec2i):
        def next: Beam = Beam(pos + dir, dir)

